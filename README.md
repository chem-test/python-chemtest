Chem-test
=========

Chem-test is a usefull python package for benchmarking deep-learning model for chemistry

# Documentation

Documentation available at [Chem-test doc](https://chem-test.gitlab.io/documentation/index.html)

# Installation

## Requierment

Chem-test support python 3.7 and require:
* torch>=1.8
* schnetpack>=0.3
* tqdm>=4.59

## Download

The source code is available on gitlab.
```bash
git clone https://gitlab.com/chem-test/python-chemtest.git
```

## System wide

Install chem-test with pip
```bash
pip install git+https://gitlab.com/chem-test/python-chemtest.git
```

## Virtual env

Chem-test can be installed in a virtual env
```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install git+https://gitlab.com/chem-test/python-chemtest.git
```

## Check installed version

```bash
chem-test -V
```

## Unit testing

Once chem-test is installed, unit testing can be performed.
```bash
chem-test test
```
