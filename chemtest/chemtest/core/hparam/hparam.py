from __future__ import annotations
import json
from typing import Any, Union, Type, Dict
import copy

from .node import HParamsNode


class DefaultHParams:
    _hparams_default: dict = {}

    def __init__(self, *args, global_hparams: HParams = None, **kwargs):
        # default hyperparameters
        if global_hparams is None:
            global_hparams = HParams()

        self.global_hparams = global_hparams

        super(DefaultHParams, self).__init__(*args, **kwargs)

    @classmethod
    def get_name(cls) -> str:
        return str(cls.__name__)

    @property
    def scope_name(self) -> str:
        return ".".join([self._hparams_namespace, self.get_name().lower()])

    @property
    def hparams(self) -> HParamsNode:
        return self.global_hparams.get_node(self.scope_name)


class HParams:
    """
    The HParams object contains hyperparameters. Default parameters can be
    register by the **HParams.register_default** decorator and can be
    initialize by default when a partial definition of hyperparameters are
    provided at the object initialization.

    .. code-block:: python

        from chemtest.core.hparam import HParams

        hparams = HParams({"a": 1, "b": 2}, default=False)
        hparams.expand(True)
        hparams.a = 3
        hparams.c.d = 4
        print(hparams.a)


    :param hparams: The hyperparameters
    :param default: Initialize defaults parameters
    """

    __default = HParamsNode(__expand__=True)

    def __init__(
        self,
        hparams: Union[HParams, Dict[str, Any]] = None,
        default: bool = True,
    ):
        assert (
            isinstance(hparams, HParams) or isinstance(hparams, dict) or hparams is None
        )

        if default:
            self.__hparams__ = copy.deepcopy(self.__default)
            self.expand(False)
        else:
            self.__hparams__ = HParamsNode(__expand__=False)

        if hparams is not None:
            if isinstance(hparams, HParams):
                self.__hparams__.merge(hparams)
            elif isinstance(hparams, dict):
                target = HParamsNode.__from_dict__(hparams)
                self.__hparams__.merge(target)

    @staticmethod
    def register_default(to_register):
        """
        Create a new configuration fo hyperparameters and setup them as
        default

        :param inputs: A list of numpy array of a given input format
        """

        assert type(to_register._hparams_namespace) == str
        assert type(to_register._hparams_default) == dict

        namespace_name = to_register._hparams_namespace
        default = to_register._hparams_default
        module_name = to_register.__name__.lower()

        new_node = HParamsNode.__from_dict__(default)

        namespace = HParams.__default.__getattr__(namespace_name)
        namespace.__setattr__(module_name, new_node)
        HParams.__default.__setattr__(namespace_name, namespace)

        return to_register

    @staticmethod
    def get_default() -> HParams:
        """
        Get the default hyperparameters.
        """

        return HParams()

    @staticmethod
    def reset_default():
        """
        Reset the default hyperparameters.
        """

        HParams.__default = HParamsNode(__expand__=True)

    @property
    def params(self):
        raise DeprecationWarning
        return self.__hparams__.params

    def expand(self, exp: bool):
        """
        Automatically expand unknown parameters when setting true if needed.

        :param exp: should it be automatically expand
        """

        self.__hparams__.expand(exp)

    def scope(self, s: str, expand: bool = False):
        """
        Enter into a scope with a **with** statement.

        .. code-block:: python

            from chemtest.core.hparam import HParams

            hparams = HParams({"a": 2, "b": {"c": 3, "d": {"e": 1}}}, default=False)

            with hparams.scope("b.d", expand=True) as params:
                print(params.e)
                params.f = 4

            print(str(hparams))

        Output:

        .. code-block:: bash

            1
            {
                "a": 2,
                "b": {
                    "c": 3,
                    "d": {
                        "e": 1,
                        "f": 4
                    }
                }
            }

        :param s: the name of the scope
        :param expand: automatically inside of the **with** statement
        """

        assert type(s) == str

        return self.__hparams__.scope(s, expand)

    def has_scope(self, s: str):
        """
        Test the existance of a scope or parameter.

        :param s: the name of the scope
        """

        return self.__hparams__.has_scope(s)

    def __getattr__(self, attr: str) -> Any:
        assert type(attr) == str

        return self.__hparams__.__getattr__(attr)

    def __setattr__(self, attr: str, value: Any):
        assert type(attr) == str

        if attr[0] == "_":
            super(HParams, self).__setattr__(attr, value)
        else:
            self.__hparams__.__setattr__(attr, value)

    def get_node(self, s) -> object:
        return self.__hparams__.get_node(s)

    def __str__(self) -> str:
        return json.dumps(self.__hparams__.__to_dict__(), indent=4, sort_keys=True)

    def to_dict(self) -> Dict[str, Any]:
        """
        Convert the object into a dictionnary.
        """

        return self.__hparams__.__to_dict__()

    def from_dict(self, dct: Dict[str, Any]):
        """
        Load hyperparameters from a dictionnary.

        :param dct: Input hyperparameters
        """

        self.__hparams__ = HParamsNode.__from_dict__(dct)

    @staticmethod
    def __flatten_dict__(dct: Dict[str, Any], root: str = "") -> Dict[str, Any]:
        result = {}
        for k, v in dct.items():
            new_k = root + "/" + k if len(root) != 0 else k
            if type(v) == dict:
                tmp = HParams.__flatten_dict__(v, new_k)
                for k2, v2 in tmp.items():
                    result[k2] = v2
            else:
                result[new_k] = copy.deepcopy(v)
        return result

    def to_tensorboard_hparams(self) -> Dict[str, Any]:
        import torch

        hparams = self.to_dict()
        hparams = self.__flatten_dict__(hparams)

        for k, v in hparams.items():
            if type(v) == list:  # only way to display it (sadly)
                hparams[k] = str(v)
            elif type(v) == torch.Tensor:  # cast torch Tensor
                if torch.prod(torch.tensor(v.shape)).item() == 1:
                    hparams[k] = v.item()
                else:
                    hparams[k] = str(v.tolist())
            elif not (type(v) in [bool, int, float, str]):  # try what you can!
                hparams[k] = str(v)

        return hparams

    def save(self, file_name: str, pretty: bool = True):
        """
        Save hyperparameters into a file.

        :param file_name: The file name
        :param pretty: Pretty print (make editing easyer)
        """

        assert type(file_name) == str
        assert type(pretty) == bool

        with open(file_name, "w") as fp:
            dct = self.__hparams__.__to_dict__()

            if pretty:
                json.dump(dct, fp, indent=4, sort_keys=True)
            else:
                json.dump(dct, fp)

    def load(self, file_name: str, fill_default: bool = True):
        """
        Load hyperparameters from a file.

        :param file_name: The file name
        :param fill_default: Set unexisting parameters with default values
        """

        assert type(file_name) == str
        assert type(fill_default) == bool

        with open(file_name, "r") as fp:
            dct = json.load(fp)
            hparams = HParamsNode.__from_dict__(dct)

        if fill_default:
            self.__hparams__ = copy.deepcopy(self.get_default().__hparams__)
            self.__hparams__.merge(hparams)
        else:
            self.__hparams__ = hparams

    def __eq__(self, hparams: HParams) -> bool:
        return self.__hparams__.__eq__(hparams.__hparams__)

    def __ne__(self, hparams: HParams) -> bool:
        return not self.__eq__(hparams)
