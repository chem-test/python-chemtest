from __future__ import annotations
import json
from typing import ItemsView, Dict, Any
import copy


class HParamsNode(object):
    """
    Create a new node from a dict of nodes and values. By default,
    a node is expanded automaticaly when an unknown attribute is get.

    :param **kwargs: The dict of nodes and values
    """

    def __init__(self, __expand__: bool = False, **kwargs):

        self.__params__ = kwargs
        self.__expand__ = __expand__

    def expand(self, exp: bool):
        """
        Set node auto expansion to unknown attributes if queried.

        :param exp: True to enable the expansion , False to turn it off
        """

        self.__expand__ = exp

        for key, value in self.__params__.items():
            if isinstance(value, self.__class__):
                self.__params__[key].expand(exp)

    @property
    def params(self) -> dict:
        """Get the node of the subtree."""

        return self.__params__

    def check_expand(self, attr: str):
        """
        Check if an attribute existe. raise an error if it donesn't existe
        and cannot be created.

        :param attr: The name of the attribute
        """

        if (not (attr in self.__params__)) and (not self.__expand__):
            raise KeyError(
                f'attribute "{attr}" doesn\'t exist (expand: {self.__expand__})'
            )

    def __getattr__(self, attr: str) -> Any:
        """
        Get a given attribute.

        :param attr: The name of the attribute
        """

        assert type(attr) == str

        if not (attr in self.__params__):
            node = HParamsNode()
            node.__expand__ = True
            self.__setattr__(attr, node)

        self.check_expand(attr)
        return self.__params__[attr]

    def __setattr__(self, attr: str, value: Any):
        """
        Set a given attribute. The attribute is created if it doesn't existe
        and of the node can be expanded.

        :param attr: The name of the attribute
        :param value: The value of the attribute
        """
        assert type(attr) == str

        if attr[0] == "_":
            super(HParamsNode, self).__setattr__(attr, value)
        else:
            self.check_expand(attr)
            self.__params__[attr] = value

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            result.__dict__[k] = copy.deepcopy(v, memo)

        return result

    def __merge_dict__(self, src: HParamsNode, dst: HParamsNode) -> HParamsNode:
        assert isinstance(src, HParamsNode)
        assert isinstance(dst, HParamsNode)

        if src.__params__ == {}:
            return dst

        for key, value in src.__params__.items():
            exist = key in dst.__params__

            if exist:
                current = dst.__params__[key]
                # cast type if different
                if exist and (type(current) != type(value)):
                    if type(current) == tuple and (type(value) in [int, float, str]):
                        # create tuple from scalar
                        value = tuple([value for _ in current])
                    else:
                        # cast value
                        value = type(current)(value)

                # cast inside tuple
                if type(current) == tuple:
                    # check length and types
                    if len(current) != len(value):
                        raise Exception("tuple should have the same size")

                    value = tuple([type(t)(v) for t, v in zip(current, value)])

            if exist and isinstance(value, HParamsNode):
                dst.__params__[key] = self.__merge_dict__(value, dst.__params__[key])
            else:
                dst.__params__[key] = copy.deepcopy(value)

        return dst

    def merge(self, hparams: HParamsNode):
        """
        Merge the current node with another node

        :param hparams: The node to merge
        """

        assert isinstance(hparams, HParamsNode)

        self = self.__merge_dict__(hparams, self)

    def __to_dict__(self) -> Dict[str, Any]:
        result = copy.deepcopy(self.__params__)
        for key, value in result.items():
            if type(value) == HParamsNode:
                result[key] = value.__to_dict__()
            else:
                result[key] = copy.deepcopy(value)
        return result

    @staticmethod
    def __from_dict__(dct: Dict[str, Any]) -> HParamsNode:
        assert isinstance(dct, dict)

        result = HParamsNode()
        for key, value in dct.items():
            if type(value) == dict:
                result.__params__[key] = HParamsNode.__from_dict__(value)
            else:
                result.__params__[key] = copy.deepcopy(value)
        return result

    def __str__(self) -> str:
        return json.dumps(self.__to_dict__(), indent=4, sort_keys=True)

    def items(self, reverse: bool = False) -> ItemsView:
        """
        Provied an iterator over the attributes of the node.

        :param reverse: True to reverse the order
        """

        assert type(reverse) == bool

        if not reverse:
            return self.__params__.items()
        else:
            result = {}
            for k in reversed(list(self.__params__.keys())):
                result[k] = self.__params__[k]
            return result.items()

    def has_scope(self, s: str) -> bool:
        """
        Test if the current node has a specific scope.

        :param s: the name of the scope
        """

        assert type(s) == str

        scope = s.split(".")[::-1]

        def get(hparams, scope):
            if not isinstance(hparams, HParamsNode):
                return False

            key = scope.pop()
            if not (key in hparams.__params__):
                return False
            if len(scope) > 0:
                return get(hparams.__getattr__(key), scope)
            else:
                return True

        return get(self, scope)

    def get_node(self, s: str) -> HParamsNode:
        scope = s.split(".")[::-1]

        def get(hparams, scope):
            key = scope.pop()
            if len(scope) > 0:
                return get(hparams.__getattr__(key), scope)
            else:
                return hparams.__getattr__(key)

        return get(self, scope)

    def scope(self, s: str, expand: bool = True) -> object:
        """
        Return a context manager of the node

        :param s: the path (e.g. "node1.node2.param1")
        :param expand: expand the node
        """

        assert type(s) == str

        class Scope:
            def __init__(self, hparams: HParamsNode, s: str):
                assert type(hparams) == HParamsNode
                assert type(s) == str

                self.hparams = hparams
                self.__scope__ = s

            def __enter__(self):
                self.prev_expand_settings = self.hparams.__expand__
                if self.prev_expand_settings != expand:
                    self.hparams.expand(expand)

                return self.hparams.get_node(self.__scope__)

            def __exit__(self, type, value, traceback):
                if self.prev_expand_settings != expand:
                    self.hparams.expand(self.prev_expand_settings)

        return Scope(self, s)

    def __eq__(self, params: HParamsNode) -> bool:
        if not isinstance(params, HParamsNode):
            return False

        keys = set(self.__params__.keys())
        param_keys = set(params.__params__.keys())

        if keys != param_keys:
            return False

        for key in keys:
            if type(self.__params__[key]) != type(params.__params__[key]):
                return False

            if isinstance(self.__params__[key], HParamsNode):
                if not self.__params__[key].__eq__(params.__params__[key]):
                    return False

            else:
                if self.__params__[key] != params.__params__[key]:
                    return False

        return True

    def __ne__(self, params: HParamsNode) -> bool:
        return not self.__eq__(params)
