from torch.utils.tensorboard import SummaryWriter
import torch

# import tensorflow as tf

from .metric import Metric
from ..data import DataType, Set, Batch

import json
import copy
from typing import List, Union, Dict


class Metrics:
    """
    The Metrics object is a Metric manager. It can be used calculate all the
    registered metrics, gather them, save and load them and export them to
    tensorboard. Here is a minimal example code:

    .. code-block:: python

        writer = tensorboard.writer.SummaryWriter()

        metrics = Metrics()

        # configure metrics
        metrics.add_metric(Loss(), [Set.TRAINING, Set.VALIDATION, Set.TESTING])
        metrics.add_metric(CustomMetric(CustomType), [Set.VALIDATION, Set.TESTING])

        for epoch in range(10):

            # training
            for batch in training_set:
                pred = model.forward(batch)
                loss = loss_fn(batch, pred)

                metrics.handle_batch(
                    Set.TRAINING,
                    batch,
                    pred,
                    loss=loss,
                )

            metrics.gather(Set.TRAINING)
            metrics.to_tensorboard(Set.TRAINING, writer, step=epoch)

            # validation
            for batch in training_set:
                pred = model.forward(batch)
                loss = loss_fn(batch, pred)

                metrics.handle_batch(
                    Set.TRAINING,
                    batch,
                    pred,
                    loss=loss,
                )

            metrics.gather(Set.VALIDATION)
            metrics.to_tensorboard(Set.VALIDATION, writer, step=epoch)

        # testing

        for batch in testing_set:
            pred = model.forward(batch)
            loss = loss_fn(batch, pred)

            metrics.handle_batch(
                Set.TESTING,
                batch,
                pred,
                loss=loss,
            )

        metrics.gather(Set.TESTING)
        metrics.to_tensorboard(Set.TESTING, writer)

        writer.flush()
        writer.close()

    """

    _metrics_prefix = {
        Set.TRAINING: "train",
        Set.VALIDATION: "valid",
        Set.TESTING: "test",
    }

    def __init__(self):
        self.metrics: Dict[Set, Dict[str, List[Metric]]] = {
            Set.TRAINING: {},
            Set.VALIDATION: {},
            Set.TESTING: {},
        }

    def add_metric(self, metric: Metric, on_sets: List[Set]):
        """
        A function to add a metric on a set. A metric can be added multiple
        time is the target of the metric is diffrent.

        :param metric: The metric to add
        :param on_sets: A list of set, e.g. [Set.TRAINING, Set.VALIDATION, Set.TESTING] to add the metric calculation on all sets
        """

        for s in on_sets:
            metrics = self.metrics[s].get(metric.name, [])

            metrics.append(copy.deepcopy(metric))

            self.metrics[s][metric.name] = metrics

    def handle_batch(
        self,
        set: Set,
        batch: Batch,
        pred: Dict[type, DataType],
        loss: Union[torch.Tensor, Dict[str, torch.Tensor]] = None,
    ):
        """
        Calcul all the metrics of a given set.

        :param set: The set of the data
        :param batch: The data from the data set
        :param pred: The predicted data
        :param loss: the value of the loss function
        """

        if (loss is not None) and (not isinstance(loss, dict)):
            loss = {"loss": loss}

        for metrics in self.metrics[set].values():
            for metric in metrics:
                if metric.loss:
                    assert loss is not None
                    metric.handle_loss(loss[metric.name])

                else:
                    metric.handle_batch(batch, pred)

    def gather(self, set: Set):
        """
        Gather all the data collected on a given set.

        :param set: The set of the data
        """

        for metrics in self.metrics[set].values():
            for metric in metrics:
                metric.gather()

    def preview(self, set: Set) -> Dict[str, float]:
        """
        Generate a preview of the metrics.

        :param set: The set of the data
        """

        preview = {}
        for metrics in self.metrics[set].values():
            for metric in metrics:
                preview.update(metric.preview())

        return preview

    def load(self, file_name: str):
        """
        Load a checkpoint. The metrics of the checkpoint must be the same as
        the current object (i.e. same set, same name and same target).

        :param file_name: The name of the file (json format)
        """

        with open(file_name, "r") as fp:
            data = json.load(fp)

        def find_metric(set_name, metric_name, target_name=None):
            metrics_type = data[set_name]

            for m_type in metrics_type:
                for m_target in m_type:

                    if m_target["name"] == metric_name:
                        if (target_name is None) or (m_target["target"] == target_name):
                            assert (target_name is not None) or len(m_type) == 1, ""

                            return m_target

        for set_name in self.metrics.keys():
            for metric_name, metrics in self.metrics[set_name].items():
                for j, metric in enumerate(metrics):

                    target = None
                    if metric.target is not None:
                        target = metric.target.get_name()

                    m = find_metric(set_name.name, metric_name, target_name=target)
                    self.metrics[set_name][metric_name][j].unserialize(m)

    def save(self, file_name: str):
        """
        Save a checkpoint.

        :param file_name: The name of the file (json format)
        """

        data = {}

        for set, metrics in self.metrics.items():
            data[set.name] = [
                [m.serialize() for m in set_metrics] for set_metrics in metrics.values()
            ]

        with open(file_name, "w") as fp:
            json.dump(data, fp)

    def to_tensorboard(self, set: Set, writer: SummaryWriter, step: int = None):
        """
        Export the metrics to tensorboard. This function only export the last
        gathered data.

        :param set: The exported set
        :param writer: The tensorboard
        :param step: The current step of the training process
        """

        prefix = self._metrics_prefix[set]
        for metrics in self.metrics[set].values():
            for metric in metrics:
                metric.to_tensorboard(writer, step=step, prefix=prefix)

    def export(self, set: Set) -> Dict[str, float]:
        """
        Export the last gathered metrics.

        :param set: The exported set
        """

        metrics = {}
        for metric_lst in self.metrics[set].values():
            for metric in metric_lst:
                if metric.target is not None:
                    tag = f"{metric.name}({metric.target.get_name()})"
                    metrics[tag] = metric.get_last_gathered()
                else:
                    metrics[metric.name] = metric.get_last_gathered()

        return metrics
