from __future__ import annotations
from enum import Enum
from typing import Any, Union
import time
from abc import ABCMeta, abstractmethod
import json


class TimeBase(Enum):
    BATCH = 1
    EPOCH = 2
    TIME = 3  # time in sec


class EventSchelude(metaclass=ABCMeta):
    def __init__(self, period: Union[float, int], reset_flag: bool = True):
        self.__event = False
        self.period = period
        self.reset_flag = reset_flag
        self.previous_state = None

    @property
    def event(self) -> bool:
        event = self.__event
        if self.reset_flag:
            self.__event = False
        return event

    def rise(self) -> None:
        self.__event = True

    @abstractmethod
    def update(self, epoch: int, batch: int, time: float) -> None:
        pass

    def to_dict(self) -> dict:
        return {
            "event_schelude": self.__class__.__name__,
            "period": self.period,
            "reset_flag": self.reset_flag,
            "previous_state": self.previous_state,
        }

    @staticmethod
    def from_dict(data: dict) -> EventSchelude:
        event_schelude = globals()[data["event_schelude"]](
            data["period"], reset_flag=data["reset_flag"]
        )
        event_schelude.previous_state = data["previous_state"]
        return event_schelude


class EventScheludeBatch(EventSchelude):
    def __init__(self, period: int, reset_flag: bool = True):
        super(EventScheludeBatch, self).__init__(period, reset_flag=reset_flag)

    def update(self, epoch: int, batch: int, time: float) -> None:
        if batch != self.previous_state and (self.previous_state is not None):
            if (batch % self.period) == 0:
                self.rise()
        self.previous_state = batch


class EventScheludeEpoch(EventSchelude):
    def __init__(self, period: int, reset_flag: bool = True):
        super(EventScheludeEpoch, self).__init__(period, reset_flag=reset_flag)

    def update(self, epoch: int, batch: int, time: float) -> None:
        if epoch != self.previous_state and (self.previous_state is not None):
            if (epoch % self.period) == 0:
                self.rise()
        self.previous_state = epoch


class EventScheludeTime(EventSchelude):
    def __init__(self, period: int, reset_flag: bool = True):
        super(EventScheludeTime, self).__init__(period, reset_flag=reset_flag)

    def update(self, epoch: int, batch: int, time: float) -> None:
        if self.previous_state is not None:
            duration = time - self.previous_state

            if duration > self.period:
                self.rise()
                self.previous_state = time
        else:
            self.previous_state = time


class TrainingConfig:
    def __init__(
        self,
        stop: EventSchelude = None,
        valid_freq: EventSchelude = None,
        checkpoint_freq: EventSchelude = None,
        valid_on_batch: int = 0,
    ):
        if valid_freq is None:
            valid_freq = EventScheludeEpoch(1)
        if checkpoint_freq is None:
            checkpoint_freq = EventScheludeTime(600)

        assert isinstance(stop, (type(None), EventScheludeBatch, EventScheludeEpoch))
        assert isinstance(valid_freq, (EventScheludeBatch, EventScheludeEpoch))
        assert isinstance(checkpoint_freq, EventSchelude)
        assert isinstance(valid_on_batch, int)

        self.stop = stop
        self.valid_freq = valid_freq
        self.checkpoint_freq = checkpoint_freq

        self.valid_on_batch = valid_on_batch

    def to_dict(self) -> dict:
        return {
            "stop": self.stop.to_dict(),
            "valid_freq": self.valid_freq.to_dict(),
            "checkpoint_freq": self.checkpoint_freq.to_dict(),
            "valid_on_batch": self.valid_on_batch,
        }

    @staticmethod
    def from_dict(data: dict) -> None:
        return TrainingConfig(
            stop=EventSchelude.from_dict(data["stop"]),
            valid_freq=EventSchelude.from_dict(data["valid_freq"]),
            checkpoint_freq=EventSchelude.from_dict(data["checkpoint_freq"]),
            valid_on_batch=data["valid_on_batch"],
        )


class Schelude:
    @property
    def elapsed_time(self) -> float:
        return self.__prev_elasped_time + (self.time - self.__current_t0)

    @property
    def time(self) -> float:
        return time.time()

    def __init__(self, config: TrainingConfig, current_t0: float = None):
        assert isinstance(config, TrainingConfig)
        if current_t0 is None:
            current_t0 = self.time
        assert isinstance(current_t0, float) and current_t0 > 0

        self.config = config

        self.epoch = 0
        self.batch = 0

        self.__current_t0 = current_t0
        self.__prev_elasped_time = 0.0

    def update(self, epoch: int, batch: int) -> None:
        handlers = [
            self.config.stop,
            self.config.valid_freq,
            self.config.checkpoint_freq,
        ]

        for handler in handlers:
            if handler is not None:
                handler.update(epoch, batch, self.time)

        self.epoch = epoch
        self.batch = batch

    @property
    def flag_stop(self) -> bool:
        if self.config.stop is not None:
            return self.config.stop.event
        return False

    @property
    def flag_valid(self) -> bool:
        return self.config.valid_freq.event

    @property
    def flag_checkpoint(self) -> bool:
        return self.config.checkpoint_freq.event

    def to_dict(self) -> dict:
        return {
            "config": self.config.to_dict(),
            "epoch": self.epoch,
            "batch": self.batch,
            "elapsed_time": self.elapsed_time,
        }

    @staticmethod
    def from_dict(data: dict) -> Any:
        schelude = Schelude(config=TrainingConfig.from_dict(data["config"]))

        schelude.epoch = data["epoch"]
        schelude.batch = data["batch"]

        # update time
        schelude.__current_t0 = schelude.time
        schelude.__prev_elasped_time = data["elapsed_time"]

        return schelude

    def save(self, file_name: str) -> None:
        with open(file_name, "w") as fp:
            json.dump(self.to_dict(), fp)

    @staticmethod
    def load(file_name: str) -> None:
        with open(file_name, "r") as fp:
            return Schelude.from_dict(json.load(fp))
