from chemtest.core.metrics.metrics import Metrics
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim.lr_scheduler import _LRScheduler
from torch.utils.tensorboard import SummaryWriter
from torch.nn.utils import clip_grad_norm_

# import tensorflow as tf
import tqdm

from typing import Union
import glob
import json
import os
import time
import uuid
from typing import Union, List, Dict, Any, Tuple
from abc import ABCMeta, abstractmethod

from .model import Model
from .hparam import HParams, DefaultHParams
from .data import Framework, DataLoader, DataLoaderKeepInMemory, Dataset, Batch, Set
from .hook import *
from .tools import mkdir


class Training(DefaultHParams, metaclass=ABCMeta):
    hparams_file_name = "hparams.json"
    schelude_file_name = "schelude.json"
    split_file_name = "split.json"
    model_file_name = "model_%s"
    optimizer_file_name = "optimizer.pt"
    metrics_file_name = "metrics.json"

    _hparams_namespace: str = "training"

    """
    @classmethod
    def get_name(cls) -> str:
        return str(cls.__name__)

    @property
    def scope_name(self) -> str:
        return ".".join([self._hparams_namespace, self.get_name().lower()])

    @property
    def hparams(self) -> HParamsNode:
        return self.global_hparams.get_node(self.scope_name)
    """

    @property
    def framework(self) -> Framework:
        for model in self._get_models().values():
            break
        if model is not None:
            return model.framework

        raise Exception("no model defined, the framework is unkown")

    def __init__(
        self,
        dataset_name: str,
        dataset_path: str,
        split_config: Dataset.Split,
        checkpoint_path: str,
        duration: Step,
        global_hparams: HParams = None,
        num_worker: int = 8,
        device: str = None,
        tensorboard_dir: str = "runs",
        start_at: Step = None,
        default_hooks: bool = True,
        keep_in_memory: bool = False,
    ):
        super(Training, self).__init__(global_hparams=global_hparams)

        if device is None:
            if torch.cuda.is_available():
                device = "cuda"
            else:
                device = "cpu"

        self.device = device

        """
        # default hyperparameters
        if global_hparams is None:
            global_hparams = HParams()

        self.global_hparams = global_hparams
        """

        # init step
        self.duration = duration
        self.current_step = Step(0, 0)
        self.hooks = Hooks()

        self.checkpoint_path = checkpoint_path

        self.__validation_it = None

        self.metrics = Metrics()
        self.padding = None
        self.ema = None
        self.gradient_clipping = None

        if tensorboard_dir is None:
            self.tensorboard = None
        else:
            self.tensorboard = SummaryWriter(
                log_dir=os.path.join(
                    tensorboard_dir, f"{self.get_name()}_{str(uuid.uuid4())}"
                ),
                flush_secs=10,
                filename_suffix=self.get_name(),
            )

        self.init()

        self._check()

        dataset_class = Dataset.get_dataset(dataset_name)
        self.dataset = dataset_class(dataset_path, split_config=split_config)
        self.train_set, self.valid_set, self.test_set = self.dataset.split()

        for model in self._get_models().values():
            targets = model.inputs_type + model.outputs_type

        if start_at is not None:
            start_at = start_at.batch
        else:
            start_at = 0

        if keep_in_memory:
            self.dataloader_train = DataLoaderKeepInMemory(
                self.dataset,
                targets,
                self.framework,
                self.hparams.batch_size,
                subset=self.train_set,
                num_workers=0,
                start_at=start_at,
                device=self.device,
            )

            self.dataloader_valid = DataLoaderKeepInMemory(
                self.dataset,
                targets,
                self.framework,
                self.hparams.batch_size,
                subset=self.valid_set,
                num_workers=0,
                device=self.device,
            )

            self.dataloader_test = DataLoaderKeepInMemory(
                self.dataset,
                targets,
                self.framework,
                self.hparams.batch_size,
                subset=self.test_set,
                num_workers=0,
                device=self.device,
            )
        else:
            self.dataloader_train = DataLoader(
                self.dataset,
                targets,
                self.framework,
                self.hparams.batch_size,
                subset=self.train_set,
                num_workers=num_worker,
                start_at=start_at,
            )

            self.dataloader_valid = DataLoader(
                self.dataset,
                targets,
                self.framework,
                self.hparams.batch_size,
                subset=self.valid_set,
                num_workers=num_worker,
            )

            self.dataloader_test = DataLoader(
                self.dataset,
                targets,
                self.framework,
                self.hparams.batch_size,
                subset=self.test_set,
                num_workers=num_worker,
            )

        if default_hooks:
            self.setup_default_hooks()

    def _get_optimizers(self) -> Dict[str, optim.Optimizer]:
        optimimzer = {}
        for name, obj in self.__dict__.items():
            if isinstance(obj, optim.Optimizer):
                optimimzer[name] = obj
        return optimimzer

    def _get_models(self) -> Dict[str, Model]:
        models = {}
        for name, obj in self.__dict__.items():
            if isinstance(obj, Model):
                models[name] = obj
        return models

    def _get_schedulers(self) -> Dict[str, _LRScheduler]:
        schedulers = {}
        for name, obj in self.__dict__.items():
            if isinstance(obj, _LRScheduler):
                schedulers[name] = obj
        return schedulers

    def setup_default_hooks(self):
        self.hooks.add_hook(HookCheckpoint(Hook.Event.TIME, every_n=600))
        self.hooks.add_hook(HookCheckpoint(Hook.Event.END_TRAINING))
        self.hooks.add_hook(HookCheckpoint(Hook.Event.END))

        self.hooks.add_hook(HookValidation(Hook.Event.AFTER_TRAINING_EPOCH))

        self.hooks.add_hook(
            HookTensorboardScalar(Hook.Event.AFTER_TRAINING_BATCH, Set.TRAINING)
        )
        self.hooks.add_hook(
            HookTensorboardScalar(Hook.Event.END_VALIDATION, Set.VALIDATION)
        )
        self.hooks.add_hook(HookTensorboardScalar(Hook.Event.END_TESTING, Set.TESTING))

        self.hooks.add_hook(HookTensorboardHParam(Hook.Event.END))

    def _check(self) -> None:
        assert len(self._get_models()) > 0
        assert len(self._get_optimizers()) > 0

    @abstractmethod
    def init(self):
        pass

    def loss_batch(
        self,
        outputs: List[torch.Tensor],
        targets: List[torch.Tensor],
    ) -> torch.Tensor:
        pass

    @staticmethod
    def unit_test(self) -> bool:
        return True

    def step(self, batch: Batch, set: Set) -> float:
        optimizers = self._get_optimizers()
        models = self._get_models()
        assert (
            len(optimizers) == 1
        ), "if the is more than one optimizer, the step function should be reimplemented"
        assert (
            len(models) == 1
        ), "if the is more than one model, the evaluation function should be reimplemented"

        for optimizer in optimizers.keys():
            break
        for model in models.values():
            break

        self.__dict__[optimizer].zero_grad()

        y_pred = model.forward(batch)
        loss = self.loss_batch(y_pred, batch)

        loss.backward()

        if self.gradient_clipping is not None:
            clip_grad_norm_(
                model.model.parameters(),
                max_norm=self.gradient_clipping,
                norm_type=2,
            )

        self.__dict__[optimizer].step()

        self.metrics.handle_batch(set, batch, y_pred, loss=loss)

        return loss.item()

    def eval(self, batch: Batch, set: Set) -> float:
        models = self._get_models()
        assert (
            len(models) == 1
        ), "if the is more than one model, the evaluation function should be reimplemented"

        for model in models.values():
            break

        with torch.no_grad():
            pred = model.forward(batch)
            loss = self.loss_batch(pred, batch)

        self.metrics.handle_batch(set, batch, pred, loss=loss)

        return loss.item()

    @classmethod
    def load_checkpoint(
        cls,
        path: str,
        dataset_name: str,
        dataset_path: str,
        hparams: HParams,
        num_worker: int = 8,
    ):
        # all checkpoints match a pattern
        paths = os.path.join(path, f"{cls.get_name()}_*")

        # get the most recent path if the checkpoint must be located
        paths = glob.glob(paths)
        if len(paths) != 0:
            max_batch = 0
            max_path = ""
            for p in paths:

                current_hparams = HParams()
                current_hparams.load(
                    os.path.join(p, cls.hparams_file_name), fill_default=False
                )

                if current_hparams == hparams:

                    with open(os.path.join(path, cls.schelude_file_name), "r") as fp:
                        data = json.load(fp)
                        batch = int(data["batch"])

                    if batch > max_batch:
                        max_batch = batch
                        max_path = p

            if max_path == "":
                raise Exception("not checkpoint found")
            path = max_path

        checkpoint_path, _ = os.path.split(path)

        hparams = HParams()
        hparams.load(os.path.join(path, cls.hparams_file_name), fill_default=False)

        split_config = Dataset.Split.load(os.path.join(path, cls.split_file_name))

        checkpoint = cls(
            dataset_name,
            dataset_path,
            split_config,
            checkpoint_path,
            global_hparams=hparams,
            num_worker=num_worker,
        )

        with open(os.path.join(path, cls.schelude_file_name), "r") as fp:
            data = json.load(fp)
            checkpoint.step.epoch = int(data["epoch"])
            checkpoint.step.batch = int(data["batch"])
            checkpoint.hooks.from_dict(data["hooks"])

        models = checkpoint._get_models()
        optimizers = checkpoint._get_optimizers()
        schedulers = checkpoint._get_schedulers()

        for name in models.keys():
            checkpoint.__dict__[name].load(
                os.path.join(path, checkpoint.model_file_name.format(name))
            )

        # checkpoint.model.load(os.path.join(path, checkpoint.model_file_name))

        checkpoint.metrics.load(os.path.join(path, checkpoint.metrics_file_name))

        if checkpoint.framework == Framework.PYTORCH:
            optimizer = torch.load(os.path.join(path, checkpoint.optimizer_file_name))

            for name in optimizers.keys():
                checkpoint.__dict__[name].load_state_dict(optimizer["optimizer"][name])

            for name in schedulers.keys():
                checkpoint.__dict__[name].load_state_dict(optimizer["scheduler"][name])
        else:
            raise NotImplementedError("chekpoint only implemented for pytorch")

        return checkpoint

    def save_checkpoint(self, path: str = None):
        if path is None:
            path = self.checkpoint_path

        dir_name = f"{self.get_name()}_{str(uuid.uuid4())}"
        path = os.path.join(path, dir_name)

        mkdir(path)

        self.global_hparams.save(os.path.join(path, self.hparams_file_name))

        with open(os.path.join(path, self.schelude_file_name), "w") as fp:
            json.dump(
                {
                    "epoch": self.current_step.epoch,
                    "batch": self.current_step.batch,
                    "hooks": self.hooks.to_dict(),
                },
                fp,
            )

        self.dataset.split_config.save(os.path.join(path, self.split_file_name))

        models = self._get_models()
        optimizers = self._get_optimizers()
        schedulers = self._get_schedulers()

        for model in models.values():
            break

        for name in models.keys():
            self.__dict__[name].save(
                os.path.join(path, self.model_file_name.format(name))
            )

        if self.ema is not None:
            assert len(models) == 1
            self.ema.assign(model)

            model.save(os.path.join(path, "ema_" + self.model_file_name))

            self.ema.resume(model)

        self.metrics.save(os.path.join(path, self.metrics_file_name))

        if self.framework == Framework.PYTORCH:

            optimizers = {
                name: self.__dict__[name].state_dict() for name in optimizers.keys()
            }
            schedulers = {
                name: self.__dict__[name].state_dict() for name in schedulers.keys()
            }

            data = {"optimizer": optimizers, "scheduler": schedulers}

            torch.save(data, os.path.join(path, self.optimizer_file_name))
        else:
            raise NotImplementedError("chekpoint only implemented for pytorch")

    @staticmethod
    def optimizer_to(optim, device):
        for param in optim.state.values():
            # Not sure there are any global tensors in the state dict
            if isinstance(param, torch.Tensor):
                param.data = param.data.to(device)

                if param._grad is not None:
                    param._grad.data = param._grad.data.to(device)

            elif isinstance(param, dict):
                for subparam in param.values():

                    if isinstance(subparam, torch.Tensor):
                        subparam.data = subparam.data.to(device)

                        if subparam._grad is not None:
                            subparam._grad.data = subparam._grad.data.to(device)

    def to(self, device: str) -> None:
        # model
        models = self._get_models()
        for name in models.keys():
            self.__dict__[name].to(device)

        # ema
        if self.ema is not None:
            if self.framework == Framework.PYTORCH:
                self.ema.to(device)
            elif self.framework == Framework.TENSORFLOW:
                raise NotImplemented

        # optimizer
        optimizers = self._get_optimizers()
        if self.framework == Framework.PYTORCH:
            for name in optimizers.keys():
                self.optimizer_to(self.__dict__[name], device)

        elif self.framework == Framework.TENSORFLOW:
            raise NotImplemented

    def _metrics_preview(self, set: Set) -> str:
        values_dict = self.metrics.preview(set)

        def to_str(s, precision=3):
            if s is not None:
                return str(round(s, precision))
            else:
                return "-"

        return " ".join(
            [f"{name}:{to_str(value)}" for name, value in values_dict.items()]
        )

    def make_validation(self):
        models = self._get_models()
        for name in models.keys():
            self.__dict__[name].eval()

        if len(models) == 1:
            for model in models.values():
                break

        if self.ema is not None:
            assert len(models) == 1
            self.ema.assign(model)

        self.hooks.handle(self, Hook.Event.BEGIN_VALIDATION, self.current_step)

        for valid_batch in self.dataloader_valid:
            valid_batch.to(self.device)

            self.hooks.handle(
                self, Hook.Event.BEFORE_VALIDATION_BATCH, self.current_step
            )

            self.eval(valid_batch, Set.VALIDATION)

            self.hooks.handle(
                self, Hook.Event.AFTER_VALIDATION_BATCH, self.current_step
            )

        if self.ema is not None:
            assert len(models) == 1
            self.ema.resume(model)

        self.validation = False

        self.hooks.handle(self, Hook.Event.END_VALIDATION, self.current_step)

    def train(self):
        if self.duration.epoch is not None:
            total = self.duration.epoch * len(self.dataloader_train)
        elif self.duration.batch is not None:
            total = self.duration.batch
        else:
            total = None

        models = self._get_models()
        for name in models.keys():
            self.__dict__[name].train()
        for model in models.values():
            break
        self.to(self.device)

        self.stop = False
        self.validation = False

        print("training")
        with tqdm.tqdm(
            total=total, unit="batch", initial=self.current_step.batch
        ) as pbar:

            if self.current_step.batch == 0:
                self.hooks.handle(self, Hook.Event.BEGIN_TRAINING, self.current_step)

            while not self.stop:

                self.hooks.handle(
                    self, Hook.Event.BEFORE_TRAINING_EPOCH, self.current_step
                )

                for train_batch in self.dataloader_train:
                    train_batch.to(self.device)

                    self.hooks.handle(
                        self, Hook.Event.BEFORE_TRAINING_BATCH, self.current_step
                    )

                    self.step(train_batch, Set.TRAINING)

                    self.hooks.handle(
                        self, Hook.Event.AFTER_TRAINING_BATCH, self.current_step
                    )

                    pbar.postfix = (
                        f"epoch: {self.current_step.epoch} "
                        + self._metrics_preview(Set.TRAINING)
                    )
                    pbar.update(1)

                    self.current_step.update_time()
                    self.hooks.handle(self, Hook.Event.TIME, self.current_step)

                    if self.validation:
                        self.make_validation()
                        for name in models.keys():
                            self.__dict__[name].train()

                    self.current_step.batch += 1

                    if self.stop or (
                        total is not None and total <= self.current_step.batch
                    ):
                        self.stop = True
                        break

                self.hooks.handle(
                    self, Hook.Event.AFTER_TRAINING_EPOCH, self.current_step
                )

                if self.validation:
                    self.make_validation()
                    for name in models.keys():
                        self.__dict__[name].train()

                self.current_step.epoch += 1

        self.hooks.handle(self, Hook.Event.END_TRAINING, self.current_step)

        print("testing")
        with tqdm.tqdm(total=len(self.dataloader_test), unit="batch") as pbar:

            self.hooks.handle(self, Hook.Event.BEGIN_TESTING, self.current_step)

            # inference on the testing set
            for name in models.keys():
                self.__dict__[name].eval()

            if self.ema is not None:
                assert len(models) == 1
                self.ema.assign(model)

            for test_batch in self.dataloader_test:
                test_batch.to(self.device)

                self.hooks.handle(
                    self, Hook.Event.BEFORE_TESTING_BATCH, self.current_step
                )

                self.eval(test_batch, Set.TESTING)

                self.hooks.handle(
                    self, Hook.Event.AFTER_TESTING_BATCH, self.current_step
                )

                pbar.postfix = self._metrics_preview(Set.TESTING)
                pbar.update(1)

            if self.ema is not None:
                assert len(models) == 1
                self.ema.resume(model)

            self.hooks.handle(self, Hook.Event.END_TESTING, self.current_step)

        self.hooks.handle(self, Hook.Event.END, self.current_step)

        self.tensorboard.flush()
