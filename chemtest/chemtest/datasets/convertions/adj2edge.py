from chemtest.core.hparam import HParams
from chemtest.core.data import DataConversion, Dependency

import numpy as np

from typing import List

from ..types import Adjacency, EdgeList, EdgesCount


@Dependency.register_dataconversion
class Adjacency2EdgeList(DataConversion):
    inputs = [Adjacency]
    outputs = [EdgeList, EdgesCount]

    def __init__(self, *args, **kwargs):
        super(Adjacency2EdgeList, self).__init__()

    def convert(self, inputs: List[np.ndarray]) -> List[np.ndarray]:
        """Convert an adjacency matrix to a list of edge

        Args:
            inputs (List[np.ndarray]): an adjacency matrix

        Returns:
            List[np.ndarray]: a list of edge (indexed nodes)
        """

        assert self._check_type(inputs)

        adj = inputs[0]

        i, j = np.where(np.tril(adj) != 0)

        edge_list = np.transpose([i, j])
        edge_count = np.array([len(edge_list)])

        return [edge_list, edge_count]
