from chemtest.core.data import DataType, Dataset, elements
from chemtest.datasets.types import *

import numpy as np

import json
import os
from typing import Dict, List


@Dataset.register
class MaterialProject(Dataset):
    _dir_name = "mp_old"
    _data_file = "mp.json"

    properties = {
        PropertyEnergy: "energy",
        PropertyEnergyPerAtom: "energy_per_atom",
        PropertyVolume: "volume",
        PropertyDensity: "density",
        PropertyFormationEnergyPerAtom: "formation_energy_per_atom",
        PropertyEAboveHull: "e_above_hull",
        PropertyBandGap: "band_gap",
        PropertyTotalMagnetization: "total_magnetization",
    }

    data_types = [
        Index,
        PointCloud,
        AtomicNumber,
        AtomicIndex,
        MatrixCell,
        *list(properties.keys()),
    ]

    def __init__(
        self,
        path: str,
        api_key: str = None,
        split_config: Dataset.Split = Dataset.Split(0, 1.0),
    ):
        super(MaterialProject, self).__init__(path, split_config)

        self.api_key = api_key

        self.samples = []

        if not os.path.exists(os.path.join(self.path, self._data_file)):
            self.download()

        self.load(os.path.join(self.path, self._data_file))

    def load(
        self,
        file_name: str,
        max_formation_energy: float = -0.1,
        atoms_blacklist: List[str] = ["He", "Ne", "Ar", "Kr", "Xe", "Rn"],
    ) -> None:
        with open(file_name, "r") as fp:
            data = json.load(fp)

        norm_params = data["normalize"]
        for data_type, label in self.properties.items():
            mean = norm_params[label]["mean"]
            std = norm_params[label]["std"]
            data_type.set_normalization_param(mean, std)

        atoms_blacklist = [elements[e] for e in atoms_blacklist]

        samples = data["samples"]
        self.samples = []
        for i, sample in enumerate(samples):
            print(f"filtering {i+1}/{len(samples)}", end="\r")
            if sample["formation_energy_per_atom"] < max_formation_energy:
                for a in atoms_blacklist:
                    if a in sample["structure"]["z"]:
                        break
                else:
                    self.samples.append(sample)
        print(f"{len(self.samples)} samples filtered")

        self._elements = [z for z in data["species"]]

        self.elements_index = np.zeros((128,), dtype=np.int)
        for idx, e in enumerate(self.elements):
            self.elements_index[e] = idx

    def __len__(self) -> int:
        return len(self.samples)

    @property
    def elements(self) -> List[int]:
        """List of elements contained in the dataset."""

        return self._elements

    def download(self) -> None:
        from pymatgen.ext.matproj import MPRester
        from pymatgen.core.structure import Structure

        class MatEncoder(json.JSONEncoder):
            def default(self, obj):
                if isinstance(obj, np.ndarray):
                    return obj.tolist()
                if isinstance(obj, Structure):
                    return {
                        "lattice": obj.lattice.matrix,
                        "r": [atom.coords for atom in obj.sites],
                        "z": [atom.specie.number for atom in obj.sites],
                    }
                return json.JSONEncoder.default(self, obj)

        normalize = list(self.properties.values())

        with MPRester(self.api_key) as m:

            criteria = {"nelements": {"$gt": 0}}
            # criteria = "Fe3O2"
            properties = [
                "material_id",
                "pretty_formula",
                "unit_cell_formula",
                "energy",
                "energy_per_atom",
                "volume",
                "density",
                "nsites",
                "elements",
                "nelements",
                "spacegroup",
                "structure",
                "formation_energy_per_atom",
                "e_above_hull",
                "elasticity",
                "piezo",
                "diel",
                "is_hubbard",
                "hubbards",
                "band_gap",
                "total_magnetization",
            ]

            samples = m.query(criteria=criteria, properties=properties)

        to_normalize = {k: [] for k in normalize}
        atoms = set()

        for sample in samples:
            for key in normalize:
                if sample[key] is not None:
                    to_normalize[key].append(sample[key])

            for atom in sample["structure"].sites:
                atoms.add(atom.specie.number)

        normalize_params = {}
        for key in normalize:
            series = np.array(to_normalize[key])
            normalize_params[key] = {
                "mean": series.mean(),
                "std": series.std(),
            }

        species = list(atoms)

        with open(os.path.join(self.path, self._data_file), "w") as fp:
            json.dump(
                {
                    "samples": samples,
                    "normalize": normalize_params,
                    "species": species,
                },
                fp,
                cls=MatEncoder,
            )

    def get_sample_size(self, idx: int) -> int:
        return len(self.samples[idx]["structure"]["z"])

    def __getitem__(self, idx: int) -> Dict[type, DataType]:

        if isinstance(idx, (list, np.ndarray)):
            return [self.__getitem__(int(i)) for i in idx]

        if idx >= len(self):
            raise StopIteration

        data = self.samples[idx]

        sample = {}

        for data_type, index in self.properties.items():
            prop = np.array([data[index]], dtype=np.float32)
            sample[data_type] = data_type(prop)

        index = np.array([idx], dtype=np.long)
        matrix_cell = np.array(data["structure"]["lattice"], dtype=np.float32)
        points_clouds = np.array(data["structure"]["r"], dtype=np.float32)
        atomic_number = np.array(data["structure"]["z"], dtype=np.long)
        atomic_index = self.elements_index[atomic_number]

        sample[Index] = Index(index)
        sample[MatrixCell] = MatrixCell(matrix_cell)
        sample[PointCloud] = PointCloud(points_clouds, cell=matrix_cell)
        sample[AtomicNumber] = AtomicNumber(atomic_number)
        sample[AtomicIndex] = AtomicIndex(atomic_index)

        return sample
