from chemtest.core.data import DataType, Dataset, elements
from chemtest.datasets.types import *

from torch_geometric.data import download_url, extract_zip
import numpy as np
from ase.io.cif import read_cif

import json
import os
import io
from typing import Dict, List


def process_sample(args):
    idx, sample = args
    f = io.StringIO(sample["structure"])
    cry = next(read_cif(f, slice(None)))

    return idx, {
        "cell": cry.get_cell().tolist(),
        "z": cry.get_atomic_numbers().tolist(),
        "pos": cry.get_positions().tolist(),
        "formation_energy_per_atom": sample["formation_energy_per_atom"],
        "band_gap": sample.get("band_gap", 0.0),
    }


@Dataset.register
class MaterialProjectV2(Dataset):
    _dir_name = "mp"
    _data_file = "mp.2019.04.01.json"
    _data_file_prepro = "mp.2019.04.01_prepro.json"

    properties = {
        PropertyFormationEnergyPerAtom: "formation_energy_per_atom",
        PropertyBandGap: "band_gap",
    }

    data_types = [
        Index,
        PointCloud,
        AtomicNumber,
        AtomicIndex,
        MatrixCell,
        *list(properties.keys()),
    ]

    def __init__(
        self,
        path: str,
        max_size: int = 32,
        max_energy: float = -0.1,
        split_config: Dataset.Split = Dataset.Split(0, 1.0),
    ):
        super(MaterialProjectV2, self).__init__(path, split_config)

        self.samples = []

        self.load(
            os.path.join(self.path, self._data_file),
            os.path.join(self.path, self._data_file_prepro),
            max_size=max_size,
            max_energy=max_energy,
        )

    def load(
        self,
        file_name: str,
        file_name_prepro: str = "",
        max_size: int = 32,
        max_energy: float = -0.1,
        atoms_blacklist: List[str] = ["He", "Ne", "Ar", "Kr", "Xe", "Rn"],
    ):
        if os.path.exists(file_name_prepro):
            print("loading Material Project")
            with open(file_name_prepro, "r") as fp:
                samples = json.load(fp)

            self.samples = []
            for sample in samples:
                if (
                    len(sample["z"]) <= max_size
                    and sample["formation_energy_per_atom"] <= max_energy
                ):
                    self.samples.append(sample)

            return

        if not os.path.exists(file_name):
            print("download Material Project")
            dir, _ = os.path.split(file_name)
            file_path = download_url(
                "https://figshare.com/ndownloader/articles/8097992/versions/2",
                dir,
            )
            extract_zip(file_path, dir)
            extract_zip(os.path.join(dir, "mp.2019.04.01.json.zip"), dir)
            os.unlink(file_path)
            os.unlink(os.path.join(dir, "mp_elastic.2019.04.01.json.zip"))
            os.unlink(os.path.join(dir, "mp.2019.04.01.json.zip"))

        with open(file_name, "r") as fp:
            data = json.load(fp)

        atoms_blacklist = [elements[e] for e in atoms_blacklist]

        from multiprocessing import Pool
        from tqdm import tqdm

        out = [None for _ in data]

        with Pool(processes=8) as p:
            for i, value in tqdm(
                p.imap_unordered(process_sample, enumerate(data)),
                total=len(data),
                postfix="preprocessing Material Project",
            ):
                out[i] = value

        self.samples = []
        for sample in out:
            for a in atoms_blacklist:
                if a in sample["z"]:
                    break
            else:
                self.samples.append(sample)

        with open(file_name_prepro, "w") as fp:
            json.dump(self.samples, fp)

    def __len__(self) -> int:
        return len(self.samples)

    @property
    def elements(self) -> List[int]:
        """List of elements contained in the dataset."""

        return list(range(1, 85))

    def get_sample_size(self, idx: int) -> int:
        return len(self.samples[idx]["z"])

    def __getitem__(self, idx: int) -> Dict[type, DataType]:

        if isinstance(idx, (list, np.ndarray)):
            return [self.__getitem__(int(i)) for i in idx]

        if idx >= len(self):
            raise StopIteration

        data = self.samples[idx]

        sample = {}

        for data_type, index in self.properties.items():
            prop = np.array([data[index]], dtype=np.float32)
            sample[data_type] = data_type(prop)

        index = np.array([idx], dtype=np.long)
        matrix_cell = np.array(data["cell"], dtype=np.float32)
        points_clouds = np.array(data["pos"], dtype=np.float32)
        atomic_number = np.array(data["z"], dtype=np.long)

        sample[Index] = Index(index)
        sample[MatrixCell] = MatrixCell(matrix_cell)
        sample[PointCloud] = PointCloud(points_clouds, cell=matrix_cell)
        sample[AtomicNumber] = AtomicNumber(atomic_number)

        return sample
