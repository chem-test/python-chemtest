from chemtest.core.data import DataType, Dependency


@Dependency.register_datatype
class Adjacency(DataType):
    def __init__(self):
        super(Adjacency, self).__init__((-1, -1), dtype=float)
