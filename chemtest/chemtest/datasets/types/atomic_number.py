from chemtest.core.data import DataType, Dependency, Framework


@Dependency.register_datatype
class AtomicNumber(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(AtomicNumber, self).__init__(data, framework=framework, indexing=None)


@Dependency.register_datatype
class AtomicIndex(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(AtomicIndex, self).__init__(data, framework=framework, indexing=None)
