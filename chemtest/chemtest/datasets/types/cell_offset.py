from chemtest.core.data import DataType, Dependency, Framework


@Dependency.register_datatype
class CellOffset(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(CellOffset, self).__init__(data, framework=framework, indexing=None)
