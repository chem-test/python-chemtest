from chemtest.core.data import DataType, Dependency


@Dependency.register_datatype
class EdgesCount(DataType):
    def __init__(self):
        super(EdgesCount, self).__init__((1,), dtype=int)
