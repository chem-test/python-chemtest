from chemtest.core.data import DataType, Dependency, Framework


@Dependency.register_datatype
class MatrixCell(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(MatrixCell, self).__init__(data, framework=framework, indexing=None)
