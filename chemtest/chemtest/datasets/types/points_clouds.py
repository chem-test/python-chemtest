from chemtest.core.data import DataType, Dependency, Framework

import numpy as np


@Dependency.register_datatype
class PointCloud(DataType):
    def __init__(self, data, framework=Framework.NUMPY, cell=None, **kwargs):

        if cell is not None:
            norm_points = np.matmul(
                data.reshape(-1, 1, 3), np.linalg.inv(cell)
            ).reshape(-1, 3)
            super(PointCloud, self).__init__(
                norm_points,
                framework=framework,
                normalised=True,
                indexing=None,
            )
        else:
            super(PointCloud, self).__init__(data, framework=framework, indexing=None)
