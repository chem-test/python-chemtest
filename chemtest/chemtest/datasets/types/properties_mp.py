from chemtest.core.data import Dependency, DataType, Framework, Normal
from .properties_qm9 import Property


@Dependency.register_datatype
class PropertyEnergy(Property):
    pass


@Dependency.register_datatype
class PropertyEnergyPerAtom(Property):
    pass


@Dependency.register_datatype
class PropertyVolume(Property):
    pass


@Dependency.register_datatype
class PropertyDensity(Property):
    pass


@Dependency.register_datatype
class PropertyFormationEnergyPerAtom(Property):
    pass


@Dependency.register_datatype
class PropertyEAboveHull(Property):
    pass


@Dependency.register_datatype
class PropertyPiezo(Property):
    pass


@Dependency.register_datatype
class PropertyDiel(Property):
    pass


@Dependency.register_datatype
class PropertyBandGap(Property):
    pass


@Dependency.register_datatype
class PropertyTotalMagnetization(Property):
    pass
