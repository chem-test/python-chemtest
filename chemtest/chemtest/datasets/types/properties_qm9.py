from chemtest.core.data import Dependency, DataType, Framework, Normal


class Property(DataType):
    normalize = None

    @classmethod
    def set_normalization_param(cls, mean: float, std: float):
        cls.normalize = Normal(mean=mean, std=std)

    def __init__(
        self, data, framework=Framework.NUMPY, normalised: bool = False, **kwargs
    ):
        super(Property, self).__init__(
            data, framework=framework, indexing=None, normalised=normalised
        )


@Dependency.register_datatype
class PropertyA(Property):
    pass


@Dependency.register_datatype
class PropertyB(Property):
    pass


@Dependency.register_datatype
class PropertyC(Property):
    pass


@Dependency.register_datatype
class PropertyMu(Property):
    pass


@Dependency.register_datatype
class PropertyAlpha(Property):
    pass


@Dependency.register_datatype
class PropertyHomo(Property):
    pass


@Dependency.register_datatype
class PropertyLumo(Property):
    pass


@Dependency.register_datatype
class PropertyGap(Property):
    pass


@Dependency.register_datatype
class PropertyR2(Property):
    pass


@Dependency.register_datatype
class PropertyZPVE(Property):
    pass


@Dependency.register_datatype
class PropertyU0(Property):
    pass


@Dependency.register_datatype
class PropertyU(Property):
    pass


@Dependency.register_datatype
class PropertyH(Property):
    pass


@Dependency.register_datatype
class PropertyG(Property):
    pass


@Dependency.register_datatype
class PropertyCv(Property):
    pass


@Dependency.register_datatype
class PropertyDeltaEpsilon(Property):
    pass


@Dependency.register_datatype
class PropertyU0_atom(Property):
    pass


@Dependency.register_datatype
class PropertyU_atom(Property):
    pass


@Dependency.register_datatype
class PropertyH_atom(Property):
    pass


@Dependency.register_datatype
class PropertyG_atom(Property):
    pass
