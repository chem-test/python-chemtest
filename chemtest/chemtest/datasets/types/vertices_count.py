from chemtest.core.data import DataType, Dependency


@Dependency.register_datatype
class VerticesCount(DataType):
    def __init__(self):
        super(VerticesCount, self).__init__((1,), dtype=int)
