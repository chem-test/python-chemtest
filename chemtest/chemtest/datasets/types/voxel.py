from chemtest.core.data import DataType, Dependency, Framework

from typing import List


@Dependency.register_datatype
class VoxelCell(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(VoxelCell, self).__init__(data, framework=framework)


@Dependency.register_datatype
class VoxelElements(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(VoxelElements, self).__init__(data, framework=framework)
