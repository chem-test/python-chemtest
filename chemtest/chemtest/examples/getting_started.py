from chemtest.core import Module, Training
from chemtest.core.hparam import HParams
from chemtest.core.data import Framework, Padding, Set
from chemtest.core.metrics import Loss
from chemtest.datasets.types import *
from chemtest.metrics import *

import schnetpack
from schnetpack import Properties

import torch.nn.functional as F
from torch.optim import Adam


@HParams.register_default
class SchNet(Module):
    _hparams_default = {
        "schnet": {"n_atom_basis": 64, "n_filters": 128, "n_interactions": 6},
        "atomwise": {"n_layers": 2},
    }
    framework = Framework.PYTORCH
    inputs_type = [
        (PointCloud, Padding.PADDING_WITHOUT_MASK),
        (AtomicNumber, Padding.PADDING_WITH_MASK),
        (MatrixCell, Padding.PADDING_WITHOUT_MASK),
        (CellOffset, Padding.PADDING_WITHOUT_MASK),
        (SchnetNeighbors, Padding.PADDING_WITHOUT_MASK),
    ]
    outputs_type = [(QM9Normalized, Padding.PADDING_WITHOUT_MASK)]

    def build(self, hparams) -> None:
        # create model
        reps = schnetpack.representation.SchNet(
            n_atom_basis=hparams.schnet.n_atom_basis,
            n_filters=hparams.schnet.n_filters,
            n_interactions=hparams.schnet.n_interactions,
        )
        output = schnetpack.atomistic.Atomwise(
            n_in=hparams.schnet.n_atom_basis,
            n_out=len(QM9Properties.properties),
            n_layers=hparams.atomwise.n_layers,
        )
        self.model = schnetpack.atomistic.AtomisticModel(reps, output)

    def forward(self, inputs):
        schnet_inputs = {
            Properties.R: inputs[0],
            Properties.Z: inputs[1][0],
            Properties.atom_mask: inputs[1][1],
            Properties.cell: inputs[2],
            Properties.cell_offset: inputs[3],
            Properties.neighbors: inputs[4],
            Properties.neighbor_mask: None,
        }
        out = self.model(schnet_inputs)
        properties = out["y"]
        return [properties]


@HParams.register_default
class TrainSchNet(Training):
    _hparams_default = {
        "optimizer": {"lr": 1e-4},
        "batch_size": 32,
    }

    def init(self):
        self.model = SchNet(self.global_hparams)

        self.optimizer = Adam(
            self.model.model.parameters(), lr=self.hparams.optimizer.lr
        )

        self.metrics.add_metric(Loss(), on_sets=[Set.TRAINING, Set.VALIDATION])

        self.metrics.add_metric(
            MAEMetric(
                input_types=[(QM9Normalization, Padding.NONE)],
                prediction_types=[QM9Normalized],
            ),
            on_sets=[Set.VALIDATION, Set.TESTING],
        )

    def loss_batch(self, outputs, targets):
        return F.mse_loss(outputs[0], targets[0], reduction="mean")
