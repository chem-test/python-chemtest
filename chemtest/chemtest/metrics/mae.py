import torch
import torch.nn.functional as F

from chemtest.core.data import DataType

from .regression import RegressionMetric


class MAEMetric(RegressionMetric):
    """
    The mean absolut error (mae) defined as the l1 distance between the
    predicted data and the target.

    :param target: The targeted data
    :param target_name: The displayed name of the target
    :param normalized: must be calculated on normalized (**True**) or on raw data (**False**)
    :param has_preview: display a preview a the metric's value during the training pocess in the progress bar
    """

    def __init__(
        self,
        target: DataType,
        target_name: str = None,
        normalized: bool = False,
        has_preview: bool = True,
    ):
        if target_name is None:
            target_name = target.get_name()

        super(MAEMetric, self).__init__(
            target=target,
            target_name=target_name,
            normalized=normalized,
            has_preview=has_preview,
        )

    @property
    def name(self) -> str:
        """
        Return the name of the metric and the name of the target i.e. "mae(target_name)"
        """

        if self.target_name == "":
            return f"mae"
        else:
            return f"mae({self.target_name})"

    def function_torch(self, pred: torch.Tensor, target: torch.Tensor):
        return F.l1_loss(
            pred.view(-1, 1),
            target.view(-1, 1),
            reduction="none",
        )

    def function_tensorflow(self, pred: torch.Tensor, target: torch.Tensor):
        raise NotImplemented
