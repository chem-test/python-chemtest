import torch
import torch.nn.functional as F

from chemtest.core.data import DataType

from .regression import RegressionMetric


class MSEMetric(RegressionMetric):
    """
    The mean squared error (mse) defined as the l2 distance between the
    predicted data and the target.

    :param target: The targeted data
    :param target_name: The displayed name of the target
    :param normalized: must be calculated on normalized (**True**) or on raw data (**False**)
    :param has_preview: display a preview a the metric's value during the training pocess in the progress bar
    """

    def __init__(
        self,
        target: DataType,
        target_name: str = "",
        normalized: bool = False,
        has_preview: bool = True,
    ):
        super(MSEMetric, self).__init__(
            target=target,
            target_name=target_name,
            normalized=normalized,
            has_preview=has_preview,
        )

    @property
    def name(self) -> str:
        """
        Return the name of the metric and the name of the target i.e. "mse(target_name)"
        """

        if self.target_name == "":
            return f"mse"
        else:
            return f"mse({self.target_name})"

    def function_torch(self, pred: torch.Tensor, target: torch.Tensor):
        return F.mse_loss(
            pred.view(-1, 1),
            target.view(-1, 1),
            reduction="none",
        )

    def function_tensorflow(self, pred: torch.Tensor, target: torch.Tensor):
        raise NotImplemented
