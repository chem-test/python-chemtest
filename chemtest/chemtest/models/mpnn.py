import torch
import torch_geometric.data as data
from torch_geometric.nn import models

from typing import List, Tuple, Dict

from ..core.data import Framework, Padding, Batch, DataType
from ..core.hparam import HParams, HParamsNode
from ..core import Model
from ..datasets.types import *


@HParams.register_default
class MPNN(Model):
    _hparams_default = {}
    framework = Framework.PYTORCH
    inputs_type = [
        (PointCloud, Padding.CONCATENATE),
        (AtomicNumber, Padding.CONCATENATE),
        (Bounds, Padding.CONCATENATE),
    ]

    def build(self, outputs_type: List[Tuple[type, Padding]], hparams: HParamsNode):
        pass

    def forward(self, batch: Batch) -> Dict[type, DataType]:
        return {}
