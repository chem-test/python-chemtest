from .src import mxmnet_crystal
from chemtest.dep.mxmnet import model as mxmnet

import torch
import torch_geometric.data as data

from typing import List, Tuple, Dict

from ..core.data import Framework, Padding, Batch, DataType
from ..core.hparam import HParams, HParamsNode
from ..core import Model
from ..datasets.types import *


@HParams.register_default
class MXMNet(Model):
    _hparams_default = {
        "dim": 128,
        "n_layer": 6,
        "cutoff": 5.0,
        "num_spherical": 7,
        "num_radial": 6,
        "envelope_exponent": 5,
    }
    framework = Framework.PYTORCH
    inputs_type = [
        (PointCloud, Padding.CONCATENATE),
        (AtomicNumber, Padding.CONCATENATE),
        (Bounds, Padding.CONCATENATE),
    ]

    def build(self, outputs_type: List[Tuple[type, Padding]], hparams: HParamsNode):

        config = mxmnet.Config(hparams.dim, hparams.n_layer, hparams.cutoff)
        self.model = mxmnet.MXMNet(
            config,
            num_spherical=hparams.num_spherical,
            num_radial=hparams.num_radial,
            envelope_exponent=hparams.envelope_exponent,
        )

    def forward(self, batch: Batch) -> Dict[type, DataType]:
        index_z = torch.tensor(
            [[-1.0], [0.0], [-1.0], [-1.0], [-1.0], [-1.0], [0.0], [3.0], [2.0], [4.0]],
            device=batch.atomic_number.data.device,
        )

        a = index_z[batch.atomic_number.data]

        x = torch.cat((batch.point_cloud.data, a), dim=-1)
        edge_index = batch.bounds.data.t()
        batch_idx = batch.atomic_number.sample_index

        y = self.model(x, edge_index, batch_idx)

        assert len(self.outputs_type) == 1

        properties = {}
        data_type, _ = self.outputs_type[0]

        properties[data_type] = data_type(
            torch.unsqueeze(y, 1), framework=self.framework
        )

        return properties
