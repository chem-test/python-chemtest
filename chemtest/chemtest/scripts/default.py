from .command import Command

from argparse import ArgumentParser, Namespace


class Default(Command):
    @staticmethod
    def name() -> str:
        return "default"

    @staticmethod
    def init_subparser(subparser: ArgumentParser) -> None:
        subparser.add_argument("modules", nargs="*", help="all the desiered modules")
        subparser.add_argument(
            "--output",
            "-o",
            default="hparams.json",
            help="name of the output file (containing the defaults hyper-parameters",
        )

    @staticmethod
    def run(args: Namespace) -> int:
        from chemtest.core.hparam import HParams

        import os, sys
        import importlib

        for module in args.modules:
            base_dir, file_name = os.path.split(module)
            module_name, _ = os.path.splitext(file_name)

            sys.path.insert(0, os.path.abspath(base_dir))

            importlib.import_module(module_name)

        default = HParams.get_default()
        default.save(args.output)

        return 0
