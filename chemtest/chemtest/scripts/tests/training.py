import unittest

import numpy as np
import torch
import torch.nn as nn

from chemtest.core import Model
from chemtest.core.hparam import HParams, HParamsNode
from chemtest.core.data import (
    Framework,
    Padding,
    Dependency,
    DataType,
    Batch,
    Dataset,
    DataLoader,
)

from typing import List, Dict


@Dependency.register_datatype
class Index(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Index, self).__init__(data, framework=framework, indexing=None)


@Dependency.register_datatype
class Type1(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Type1, self).__init__(data, framework=framework, indexing=None)


@Dependency.register_datatype
class Type2(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Type2, self).__init__(data, framework=framework, indexing=None)


@Dependency.register_datatype
class Type3(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Type3, self).__init__(data, framework=framework, indexing=None)


@HParams.register_default
class CustomModelPytorch(Model):
    _hparams_default = {
        "layers": 3,
        "z_dim": 128,
        "subparameters": {"arg1": 1, "arg2": 2, "arg3": 3},
    }
    framework = Framework.PYTORCH
    inputs_type = [(Type1, Padding.PADDING_WITH_MASK)]

    def forward(self, batch: Batch) -> Dict[type, DataType]:
        y = self.model(batch.type1.data)

        assert y.shape[1] == len(self.outputs_type)

        results = {}
        for i, data_type in enumerate(self.outputs_type):
            results[data_type] = data_type(y[:, i : i + 1], framework=self.framework)

        return results

    def build(self, outputs_type: List[type], hparams: HParamsNode) -> None:

        layers = []
        prev = 1

        for _ in range(hparams.layers):
            layers.append(nn.Linear(prev, hparams.z_dim))
            layers.append(nn.ReLU())
            prev = hparams.z_dim

        dim_out = len(outputs_type)
        layers.append(nn.Linear(prev, dim_out))

        self.model = nn.Sequential(*layers)


class TestingDataset(Dataset):
    _dir_name = "testing_dataset"
    data_types = [Index, Type1]

    def __len__(self) -> int:
        return 100

    def __getitem__(self, idx: int):

        if isinstance(idx, (list, np.ndarray)):
            data = [self.__getitem__(int(i)) for i in idx]
            return data

        return {
            Index: Index(np.array([idx], dtype=np.long)),
            Type1: Type1(np.ones(1, dtype=np.float32)),
        }

    @property
    def elements(self):
        return []


class TestModel(unittest.TestCase):
    def test_model_init_pytorch(self):
        import tempfile
        import os
        from chemtest.core.tools import test_timeout

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            model = CustomModelPytorch([Type2])

            dataloader = DataLoader(
                TestingDataset(os.path.join(dir_name, "data"), Dataset.Split(1, 1.0)),
                targets=model.inputs_type,
                framework=model.framework,
                batch_size=4,
                num_workers=1,
            )

            for data in dataloader:
                res = model.forward(data)

                self.assertEqual(data.type1.shape, (4, 1))
                self.assertEqual(res[Type2].shape, (4, 1))

    def test_model_train_eval_pytorch(self):
        model = CustomModelPytorch([Type2])

        model.train()
        self.assertTrue(model.model.training)

        model.eval()
        self.assertFalse(model.model.training)

    def test_model_checkpoint_pytorch(self):
        import tempfile
        import os
        from chemtest.core.tools import test_timeout

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            model_name = os.path.join(dir_name, "model")

            model = CustomModelPytorch([Type2])
            model.save(model_name)

            model2 = CustomModelPytorch([Type2])
            model2.load(model_name)

            for p1, p2 in zip(model.model.parameters(), model2.model.parameters()):
                self.assertEqual(p1.data.ne(p2.data).sum(), 0)

            self.assertEqual(
                model.global_hparams.to_dict(), model2.global_hparams.to_dict()
            )

    def test_model_hparam_pytorch(self):
        import tempfile
        import os
        from chemtest.core.tools import test_timeout

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            custom = HParams(
                {
                    "model": {
                        "custommodelpytorch": {
                            "layers": 2,
                            "subparameters": {"arg2": 3},
                        }
                    }
                }
            )

            model_name = os.path.join(dir_name, "model")

            model = CustomModelPytorch([Type2], global_hparams=custom)
            model.save(model_name)

            self.assertDictEqual(
                model.global_hparams.to_dict(),
                {
                    "model": {
                        "custommodelpytorch": {
                            "layers": 2,
                            "z_dim": 128,
                            "subparameters": {"arg1": 1, "arg2": 3, "arg3": 3},
                        }
                    }
                },
            )
            self.assertDictEqual(
                model.hparams.__to_dict__(),
                {
                    "layers": 2,
                    "z_dim": 128,
                    "subparameters": {"arg1": 1, "arg2": 3, "arg3": 3},
                },
            )

            model2 = CustomModelPytorch([Type2])
            model2.load(model_name)

            for p1, p2 in zip(model.model.parameters(), model2.model.parameters()):
                self.assertEqual(p1.data.ne(p2.data).sum(), 0)

            self.assertEqual(
                model.global_hparams.to_dict(), model2.global_hparams.to_dict()
            )
