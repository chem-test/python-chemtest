from .command import Command

from argparse import ArgumentParser, Namespace


class Train(Command):
    @staticmethod
    def name() -> str:
        return "train"

    @staticmethod
    def init_subparser(subparser: ArgumentParser) -> None:
        subparser.add_argument("training_class", help="The targeted training class")
        subparser.add_argument("dataset", help="The name of the dataset")
        subparser.add_argument(
            "--dataset-path", "-dp", default="data", help="path of the datasets"
        )
        subparser.add_argument(
            "--dataset-split",
            "-ds",
            nargs=3,
            type=str,
            default=["0.8", "0.1", "0.1"],
            help=f"split the dataset into training, validation and testing. E.g. to split the dataset into 80/100 10/100 10/100 training validation and testing set the input must be 0.8 0.1 0.1",
        )
        subparser.add_argument(
            "--shuffle-seed",
            "-ss",
            type=int,
            default=0,
            help="initialise the random generator with the given seed to shuffle the dataset",
        )
        subparser.add_argument(
            "--checkpoints", "-c", default="checkpoints", help="checkpoints directory"
        )
        subparser.add_argument(
            "--device", "-d", default=None, help="the targeted device (cpu or cuda)"
        )
        subparser.add_argument(
            "--hparams",
            "-H",
            default=None,
            help="hyperparameters (file name in json format)",
        )
        subparser.add_argument(
            "--workers", "-w", default=0, type=int, help="workers for data loading"
        )
        subparser.add_argument(
            "--tensorboard", "-t", default="runs", help="tensorboard directory"
        )
        subparser.add_argument(
            "--duration", "-D", default="inf", help="duration of the training process"
        )
        subparser.add_argument(
            "--keep-in-memory", "-k", default=False, type=bool, help="keep the dataset in device memory to speed up the training process"
        )

    @staticmethod
    def run(args: Namespace) -> int:
        from chemtest.core.hparam import HParams
        from chemtest.core import Training, TrainingConfig, EventScheludeEpoch, Step
        from chemtest.core.data import Dataset

        import os, sys
        import re
        import importlib

        base_dir, module_class = os.path.split(args.training_class)

        module_name, class_name = module_class.split(".")

        sys.path.insert(0, os.path.abspath(base_dir))

        module = importlib.import_module(module_name)

        _class = getattr(module, class_name)

        assert issubclass(_class, Training)

        config = TrainingConfig(stop=EventScheludeEpoch(1))

        dataset_split = []
        for arg in args.dataset_split:
            if re.match(r"\A[+]?(\d+\.\d*)([eE][+-]?\d+)?\Z", arg):
                dataset_split.append(float(arg))
            elif re.match(r"\A[+]?(\d+)\Z", arg):
                dataset_split.append(int(arg))
            else:
                raise Exception(
                    "the --dataset-split take float or int as input (e.g. 0.8, 0.2 or 1-e2 for float and 123 for int value)"
                )

        split_config = Dataset.Split(
            args.shuffle_seed,
            training=dataset_split[0],
            validation=dataset_split[1],
            testing=dataset_split[2],
        )

        if args.hparams is not None:
            hparams = HParams()
            hparams.load(os.path.abspath(args.hparams), fill_default=False)
        else:
            hparams = None

        duration = Step.from_str(args.duration)

        training = _class(
            args.dataset,
            args.dataset_path,
            split_config,
            os.path.abspath(args.checkpoints),
            duration,
            global_hparams=hparams,
            num_worker=args.workers,
            device=args.device,
            tensorboard_dir=os.path.abspath(args.tensorboard),
            keep_in_memory=args.keep_in_memory,
        )

        training.train()

        return 0
