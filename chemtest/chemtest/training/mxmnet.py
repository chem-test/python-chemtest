from chemtest.core.data.dataloader import Padding

# import tensorflow as tf
import torch
import torch.nn.functional as F
from torch.optim import Adam
from torch.optim.lr_scheduler import ExponentialLR

from typing import Union, Dict
import math

from chemtest.core import Training, Hook, EMA, HookLrScheduler, HookEmaAppend
from chemtest.core.hparam import HParams
from chemtest.models import MXMNet
from chemtest.core.data import Set, Batch, DataType
from chemtest.core.metrics import Loss
from chemtest.datasets.types import *
from chemtest.datasets.convertions import *
from chemtest.metrics import MAEMetric

from warmup_scheduler import GradualWarmupScheduler


@HParams.register_default
class TrainMXMNet(Training):
    _hparams_default = {
        "optimizer": {
            "lr": 1e-4,
            "lr_decay": {"ratio": 0.1, "step": 2000000},
            "warmup_period": 4000,
            "gradient_clipping": 10.0,
        },
        "ema_decay": 0.999,
        "batch_size": 32,
    }

    def init(self):
        self.property = PropertyU0_atom

        self.model = MXMNet([(self.property, Padding.NONE)], self.global_hparams)
        self.optimizer = Adam(
            self.model.model.parameters(), lr=self.hparams.optimizer.lr
        )

        gamma = math.exp(
            math.log(self.hparams.optimizer.lr_decay.ratio)
            / self.hparams.optimizer.lr_decay.step
        )
        scheduler = ExponentialLR(self.optimizer, gamma=gamma)
        self.scheduler = GradualWarmupScheduler(
            self.optimizer,
            multiplier=1,
            total_epoch=self.hparams.optimizer.warmup_period,
            after_scheduler=scheduler,
        )

        self.ema = EMA(self.model, decay=self.hparams.ema_decay)
        self.gradient_clipping = self.hparams.optimizer.gradient_clipping

        self.hooks.add_hook(
            HookLrScheduler(Hook.Event.AFTER_TRAINING_BATCH, Set.TRAINING)
        )
        self.hooks.add_hook(
            HookEmaAppend(Hook.Event.AFTER_TRAINING_BATCH, Set.TRAINING)
        )

        self.metrics.add_metric(Loss(), on_sets=[Set.TRAINING, Set.VALIDATION])
        self.metrics.add_metric(
            MAEMetric(self.property, target_name="U_0"),
            on_sets=[Set.VALIDATION, Set.TESTING],
        )

    def loss_batch(
        self,
        pred: Dict[type, DataType],
        batch: Batch,
    ) -> torch.Tensor:
        return F.l1_loss(
            pred[self.property].raw_data,
            batch.get(self.property).raw_data.view((-1, 1)),
            reduction="mean",
        )
