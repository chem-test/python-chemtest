from chemtest.core.data.dataloader import Padding
#import tensorflow as tf
import torch
import torch.nn.functional as F
from torch.optim import Adam
from torch.optim.lr_scheduler import ExponentialLR

from typing import Union, Dict
import math

from chemtest.core import Training, Hook, EMA, HookLrScheduler, HookEmaAppend
from chemtest.core.hparam import HParams
from chemtest.core.data import Set, Batch, DataType
from chemtest.core.metrics import Loss
from chemtest.models import SchNet
from chemtest.datasets.types import *
from chemtest.datasets.convertions import *
from chemtest.metrics import MAEMetric


@HParams.register_default
class TrainSchNet(Training):
    _hparams_default = {
        "optimizer": {
            "lr": 1e-3,
            "decay": 0.99,
            "lr_decay": {"ratio": 0.96, "step": 100000},
        },
        "ema_decay": 0.999,
        "batch_size": 32,
    }

    def init(self):
        self.model = SchNet([(PropertyU0_atom, Padding.NONE)], self.global_hparams)

        self.optimizer = Adam(
            self.model.model.parameters(),
            lr=self.hparams.optimizer.lr,
            weight_decay=1.0 - self.hparams.optimizer.decay,
        )

        gamma = math.exp(
            math.log(self.hparams.optimizer.lr_decay.ratio)
            / self.hparams.optimizer.lr_decay.step
        )
        self.scheduler = ExponentialLR(self.optimizer, gamma=gamma)

        self.ema = EMA(self.model, decay=self.hparams.ema_decay)

        self.hooks.add_hook(
            HookEmaAppend(Hook.Event.AFTER_TRAINING_BATCH, Set.TRAINING)
        )

        self.metrics.add_metric(Loss(), on_sets=[Set.TRAINING, Set.VALIDATION])
        self.metrics.add_metric(
            MAEMetric(PropertyU0_atom, target_name="U_0"),
            on_sets=[Set.VALIDATION, Set.TESTING],
        )

        self.hooks.add_hook(
            HookLrScheduler(Hook.Event.AFTER_TRAINING_BATCH, Set.TRAINING)
        )

        # target 0.0134 eV for U0

    def loss_batch(
        self,
        pred: Dict[type, DataType],
        batch: Batch,
    ) -> torch.Tensor:
        return F.l1_loss(
            pred[PropertyU0_atom].raw_data,
            batch.property_u0_atom.raw_data.view((-1, 1)),
            reduction="mean",
        )
