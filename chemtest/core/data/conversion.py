from typing import List, Dict, Set
from abc import ABCMeta, abstractmethod

import numpy as np

from ..hparam import HParams, HParamsNode
from .type import DataType


class DataConversion(metaclass=ABCMeta):
    """
    Create a converter with given hyperparameters

    :param global_hparams: global hyperparameters
    """

    _hparams_namespace: str = "preprocessing"
    _hparams_default: dict = {}

    inputs: Set[DataType] = {}
    outputs: Set[DataType] = {}

    @classmethod
    def get_name(cls) -> str:
        """Get the name of the class"""
        return str(cls.__name__)

    @property
    def scope_name(self) -> str:
        """Get the scope name of the hyperparameters used in the convertion function."""

        return ".".join([self._hparams_namespace, self.get_name().lower()])

    @property
    def hparams(self) -> HParamsNode:
        return self.global_hparams.get_node(self.scope_name)

    def __init__(self, global_hparams: HParams = None):
        self.global_hparams = HParams()
        if global_hparams is not None:
            self.global_hparams = global_hparams

    @abstractmethod
    def convert(self, inputs: Dict[type, DataType]) -> Dict[type, DataType]:
        """
        Convert the inputs data to the output format

        :param inputs: A list of numpy array of a given input format
        """

        return {}

    def check_input_type(self, inputs: Dict[type, DataType]) -> bool:
        """
        Check the type of the input args

        :param inputs: input arguments
        """

        if (not isinstance(inputs, dict)) or (not isinstance(self.inputs, set)):
            return False

        for data_type in self.inputs:
            if data_type not in inputs:
                return False
        return True

    def check_output_type(self, outputs: Dict[type, DataType]) -> bool:
        """
        Check the type of the output

        :param outputs: output arguments
        """

        if (not isinstance(outputs, dict)) or (not isinstance(self.outputs, set)):
            return False

        for data_type in self.outputs:
            if data_type not in outputs:
                return False
        return True
