from chemtest.core.hparam.hparam import HParams
from typing_extensions import final
import torch.utils.data as data
import numpy as np

from typing import List, Tuple, Any, Dict
from typing import Iterator

from .type import DataType, Framework, Padding
from .dataset import Dataset
from .dependency import Preprocessing


class Sampler(data.Sampler):
    """
    The Sampler object can be used to sample a dataset, shuffle it and beeing
    able to restart at a specific point.

    :param data_source: The dataset or a subset
    :param seed: the seed to initialize the random generator
    :param start_at: an index to start
    :param batch_size: batch size
    :param shuffle: must shuffle
    """

    def __init__(
        self,
        data_source: data.Subset,
        seed: int,
        start_at: int = 0,
        batch_size: int = 16,
        shuffle: bool = True,
        drop_last: bool = True,
    ):
        self.batch_size = batch_size
        self.drop_last = drop_last

        if shuffle:
            np.random.seed(seed)
            np.random.shuffle(data_source.indices)

        self.data_source = data_source

        self._build_batch()

        self.offset = start_at % len(self.batch_idx)
        self.it = iter(self.batch_idx[self.offset :])

    def _build_batch(self):
        self.batch_idx = []
        begin, count = 0, 0

        for i, idx in enumerate(self.data_source.indices):

            s = self.data_source.dataset.get_sample_size(idx)

            if (count + s) <= self.batch_size:
                count += s
            else:
                self.batch_idx.append(slice(begin, i))
                begin, count = i, s

        if not self.drop_last:
            self.batch_idx.append(slice(begin, len(self.data_source)))

    def __len__(self) -> int:
        return len(self.batch_idx)

    def __iter__(self) -> Iterator:
        return self

    def __next__(self):
        try:
            return next(self.it)
        except StopIteration:
            self.it = iter(self.batch_idx)

        raise StopIteration


class Batch:
    """
    The Batch class gather samples and process the data to obtain the desired
    DataType and Padding.

    :param raw_data: List of samples from a dataset
    :param preprocessing: The recipe to generate the targeted DataType
    :param framework: The targeted framework
    :param targets: The tergeted DataType and Padding

    The data of a Batch object can be accessed by their name. For exemple:

    .. code-block:: python

        dataloader = DataLoader(
            dataset,
            targets=[
                (Index, Padding.NONE),
                (Type1, Padding.NONE),
                (Type2, Padding.NONE),
                (Type3, Padding.NONE),
            ],
            framework=Framework.NUMPY,
            batch_size=4,
        )

        for batch in dataloader:
            print("access data of the batch:")
            print(isinstance(batch.index, DataType))
            print(type(batch.index))
            print(type(batch.index.data))
            print(batch.index.data)

            print(batch.type1.data)
            print(batch.type2.data)
            print(batch.type3.data)
            break

    .. code-block:: bash

        access data of the batch:
        True
        <class 'Index'>
        <class 'numpy.ndarray'>
        [80 84 33 81]
        [1 1 1 1]
        [2 2 2 2]
        [3 3 3 3]

    """

    def __init__(
        self,
        raw_data: List[Dict[type, DataType]],
        preprocessing: Preprocessing,
        framework: Framework,
        targets: List[Tuple[DataType, Padding]],
        dataset: Dataset = None,
    ):
        self.dataset = dataset
        self.framework = framework

        # proprocess data
        samples = []
        for data in raw_data:
            sample = preprocessing.preprocess(data)
            for data_type in sample.values():
                data_type.framework = framework
            samples.append(sample)

        # reorder data by DataType and pad them
        __data: Dict[type, DataType] = {}
        for (data_type, padding) in targets:
            data = []
            for sample in samples:
                data.append(sample[data_type])
            gathered = data_type.gather(data, padding=padding)
            __data[data_type] = gathered

        # reindex data
        for data_type, data in __data.items():
            if data.indexing is not None:
                __data[data_type].reindex(__data[data.indexing])

        # send to framework
        for data_type in __data.keys():
            __data[data_type].to_framework()

        # update keys with DataType's name
        self.__data = {data_type.get_name(): data_type for data_type in __data.values()}

    def __getattr__(self, name: str) -> DataType:
        """
        Get data stored on the batch by name.

        :param name: the name of the DataType
        """

        if name[:1] == "_":
            return super(Batch, self).__getattr__(name)

        return self.__data[name]

    def get(self, data_type: type) -> DataType:
        """
        Get data stored on the batch by name.

        :param data_type: the name of the DataType
        """

        return self.__data[data_type.get_name()]

    def to(self, device: str):
        """
        Transfert the data to the configured device.

        :param device: the device's name
        """

        for data_type in self.__data.values():
            data_type.to(device)

        return self

    def pin_memory(self):
        for data in self.__data.values():
            if data.framework == Framework.PYTORCH:

                data.data.pin_memory()

                if (id(data.data) != id(data.raw_data)) and (data.raw_data is not None):
                    data.raw_data.pin_memory()

                if data.mask is not None:
                    data.mask.pin_memory()

                if data.sample_index is not None:
                    data.sample_index.pin_memory()

                if data.sample_size is not None:
                    data.sample_size.pin_memory()

        return self


class DataLoader(data.DataLoader):
    """
    The Sampler object can be used to sample a dataset, shuffle it and beeing
    able to restart at a specific point.

    :param dataset: The dataset
    :param targets: Configuration of the targets (requiered DataType and Padding)
    :param framework: The desired framework
    :param batch_size: The batch size
    :param start_at: Start at index
    :param subset: An optional subset
    :param shuffle: Should shuffle the data
    :param num_workers: Number of cpu core used to load the data
    :param pin_memory: Should pin memory
    :param batch_class: The batch class
    """

    def __init__(
        self,
        dataset: Dataset,
        targets: List[Tuple[DataType, Padding]],
        framework: Framework,
        batch_size: int,
        start_at: int = 0,
        subset: data.Subset = None,
        shuffle: bool = True,
        num_workers: int = 4,
        pin_memory: bool = False,
        batch_class: type = Batch,
        drop_last: bool = False,
    ):
        targeted_types = [t for t, _ in targets]
        self.targets = targets

        self.framework = framework

        self.batch_class = batch_class

        self.preprocessing = Preprocessing(dataset.data_types, targeted_types)

        def collate_wrapper(batch):

            l = len(batch)

            if isinstance(batch[0], list):
                batch = sum(batch, [])

            return self.batch_class(
                batch,
                self.preprocessing,
                self.framework,
                targets=self.targets,
                dataset=dataset,
            )

        if subset is None:
            subset = data.Subset(dataset, np.arange(len(dataset)))

        sampler = Sampler(
            subset,
            dataset.split_config.seed,
            start_at=start_at,
            batch_size=batch_size,
            shuffle=shuffle,
            drop_last=drop_last,
        )

        super(DataLoader, self).__init__(
            subset,
            batch_size=1,
            sampler=sampler,
            num_workers=num_workers,
            pin_memory=pin_memory,
            collate_fn=collate_wrapper,
        )


class DataLoaderKeepInMemory(DataLoader):
    def __init__(
        self,
        dataset: Dataset,
        targets: List[Tuple[DataType, Padding]],
        framework: Framework,
        batch_size: int,
        start_at: int = 0,
        subset: data.Subset = None,
        shuffle: bool = True,
        num_workers: int = 4,
        pin_memory: bool = False,
        batch_class: type = Batch,
        drop_last: bool = False,
        device: str = "cuda",
    ):
        super(DataLoaderKeepInMemory, self).__init__(
            dataset=dataset,
            targets=targets,
            framework=framework,
            batch_size=batch_size,
            start_at=start_at,
            subset=subset,
            shuffle=shuffle,
            num_workers=num_workers,
            pin_memory=pin_memory,
            batch_class=batch_class,
            drop_last=drop_last,
        )

        self.device = device

        self.data = []

    def __iter__(self):
        if len(self.data) == 0:
            it = super(DataLoaderKeepInMemory, self).__iter__()

            for batch in it:
                batch = batch.to(self.device)
                self.data.append(batch)
                yield batch

        else:
            for batch in self.data:
                yield batch
