from __future__ import annotations
import torch
import torch.utils.data as data

import numpy as np

from typing import List, Union, Dict, Tuple
from abc import abstractmethod, ABCMeta, abstractproperty
import os
import json
from enum import Enum

from .type import DataType


class Set(Enum):
    """Subset configuration"""

    TRAINING = 1
    VALIDATION = 2
    TESTING = 3


class Dataset(metaclass=ABCMeta):
    """
    Abstract Dataset class. The **__len__** and **__getitem__** methods must
    be provided to implement this abstract class. All the datasets are meant to
    be stored in the same directory to be albe to locate them easily.

    The Dataset class also provide a decorator to register new dataset and a
    static method to get dataset by name.

    A short exemple with a demo custom dataset:

    .. code-block:: python

        class Index(DataType):
            def __init__(self, data, framework=Framework.NUMPY):
                super(Index, self).__init__(data, framework=framework, indexing=None)

        @Dataset.register
        class CustomDataset(Dataset):
            data_types: List[type] = [Index] # setup supplied data as a list of type (class derived from DataType)

            def __init__(self, path: str, split_config: Dataset.Split):
                super(CustomDataset, self).__init__(path, split_config)

            def __len__(self) -> int:
                return 100

            def __getitem__(self, idx: int) -> Dict[str, DataType]:
                return {Index.get_name(): Index(np.array([idx]))}

        dataset_class = Dataset.get_dataset("CustomDataset")
        dataset = dataset_class("/path/to/dataset", Dataset.Split(0, 1.0))

    :param path: the path containing dataset
    :param split_config: the split configuration of the dataset
    """

    class Split:
        """
        Split is a class to configure how to split a dataset. To make the
        split reproducible, an arbitrary seed can be provided but the dataset
        can't be resized.

        :param seed: Set the seed of the random generator to split the dataset
        :param training: The size of the training set, float type a pourcentage or int for a number of sample
        :param validation: The size of the validation set
        :param testing: The size of the testing set
        :param type: Splitting strategy
        """

        class Type(Enum):
            """Use random split or scaffold split to split the dataset"""

            RANDOM = 1
            SCAFFOLD = 2

        def __init__(
            self,
            seed: int,
            training: Union[int, float],
            validation: Union[int, float] = 0,
            testing: Union[int, float] = 0,
            type: Dataset.Split.Type = Type.RANDOM,
        ):
            assert isinstance(seed, int)
            assert isinstance(training, (int, float))
            assert isinstance(validation, (int, float))
            assert isinstance(testing, (int, float))
            assert isinstance(type, self.__class__.Type)

            self.seed = seed
            self.training = training
            self.validation = validation
            self.testing = testing
            self.type = type

            if self.type == self.__class__.Type.SCAFFOLD:
                raise NotImplementedError("Scaffold split not implemented")

        def check_split(self, dataset_size: int) -> Tuple[bool, List[int]]:
            """
            Check the split configuration. The sum of the subsets must be
            smaller than the dataset itself.

            :param dataset_size: The size of the dataset (in sample)
            """

            assert isinstance(dataset_size, int)

            sets = [self.training, self.validation, self.testing]
            sets = [(s if type(s) == int else int(s * dataset_size)) for s in sets]

            return (sum(sets) <= dataset_size), sets

        def split(self, dataset: Dataset) -> List[data.Subset]:
            """
            Split a dataset into :class:`torch.utils.data.Subset`

            :param dataset: input dataset
            """

            valid, sets_size = self.check_split(len(dataset))
            if not valid:
                raise Exception(
                    f"Spliting configuration of the dataset invalid {len(dataset)} sample can be splited in {sets_size}"
                )

            if self.type == self.__class__.Type.RANDOM:
                return data.dataset.random_split(
                    dataset,
                    sets_size,
                    generator=torch.Generator().manual_seed(self.seed),
                )
            else:
                raise Exception(f"unkown split type {self.type}")

        def save(self, file_name: str):
            """
            Save the current configuration in a json file

            :param file_name: name of the file
            """

            with open(file_name, "w") as fp:
                json.dump(
                    {
                        "seed": self.seed,
                        "training": self.training,
                        "validation": self.validation,
                        "testing": self.testing,
                        "type": self.type.name,
                    },
                    fp,
                )

        @staticmethod
        def load(file_name: str) -> Dataset.Split:
            """
            Load a configuration from a json file

            :param file_name: name of the file
            """

            with open(file_name, "r") as fp:
                config = json.load(fp)

            return Dataset.Split(
                config["seed"],
                config["training"],
                validation=config["validation"],
                testing=config["testing"],
                type=Dataset.Split.Type[config["type"]],
            )

    __datasets = {}
    data_types: List[DataType] = []

    def __init__(self, path: str, split_config: Dataset.Split):
        path = os.path.join(os.path.abspath(path), self._dir_name)

        try:
            os.makedirs(path)
        except OSError:
            pass

        self.path = path
        self.split_config = split_config

    @classmethod
    def get_name(cls) -> str:
        """A static method to get the Dataset's name"""

        return str(cls.__name__)

    @abstractmethod
    def __len__(self) -> int:
        """Return the number of samples in the dataset"""

        return 0

    def get_sample_size(self, _: int) -> int:
        return 1

    @abstractmethod
    def __getitem__(self, idx: int) -> Dict[type, DataType]:
        """
        Get a sample of the dataset.

        :param idx: The index of the sample in the dataset
        """

        return []

    def split(self) -> List[data.Subset]:
        """
        Split the current Dataset into :class:`torch.utils.data.Subset` by
        following the :class:`chemtest.core.data.Dataset.Split` configuration.
        """

        return self.split_config.split(self)

    @property
    def name(self) -> str:
        """A property to get the current Dataset name."""

        return self.get_name()

    @abstractproperty
    def elements(self) -> List[int]:
        """List of elements contained in the datas"""

        return []

    @staticmethod
    def register(dataset: Dataset) -> Dataset:
        """
        A decorator to register a dataset.

        :param dataset: The dataset
        """

        assert isinstance(dataset, type)

        Dataset.__datasets[dataset.get_name()] = dataset

        return dataset

    @staticmethod
    def get_dataset(name: str) -> type:
        """
        Get a dataset by name. The dataset must be registered with the
        register decorator.

        :param name: The name of the dataset
        """

        return Dataset.__datasets[name]

    @property
    def _dir_name(self):
        raise NotImplementedError


elements = {
    "H": 1,
    "He": 2,
    "Li": 3,
    "Be": 4,
    "B": 5,
    "C": 6,
    "N": 7,
    "O": 8,
    "F": 9,
    "Ne": 10,
    "Na": 11,
    "Mg": 12,
    "Al": 13,
    "Si": 14,
    "P": 15,
    "S": 16,
    "Cl": 17,
    "Ar": 18,
    "K": 19,
    "Ca": 20,
    "Sc": 21,
    "Ti": 22,
    "V": 23,
    "Cr": 24,
    "Mn": 25,
    "Fe": 26,
    "Co": 27,
    "Ni": 28,
    "Cu": 29,
    "Zn": 30,
    "Ga": 31,
    "Ge": 32,
    "As": 33,
    "Se": 34,
    "Br": 35,
    "Kr": 36,
    "Rb": 37,
    "Sr": 38,
    "Y": 39,
    "Zr": 40,
    "Nb": 41,
    "Mo": 42,
    "Tc": 43,
    "Ru": 44,
    "Rh": 45,
    "Pd": 46,
    "Ag": 47,
    "Cd": 48,
    "In": 49,
    "Sn": 50,
    "Sb": 51,
    "Te": 52,
    "I": 53,
    "Xe": 54,
    "Cs": 55,
    "Ba": 56,
    "La": 57,
    "Ce": 58,
    "Pr": 59,
    "Nd": 60,
    "Pm": 61,
    "Sm": 62,
    "Eu": 63,
    "Gd": 64,
    "Tb": 65,
    "Dy": 66,
    "Ho": 67,
    "Er": 68,
    "Tm": 69,
    "Yb": 70,
    "Lu": 71,
    "Hf": 72,
    "Ta": 73,
    "W": 74,
    "Re": 75,
    "Os": 76,
    "Ir": 77,
    "Pt": 78,
    "Au": 79,
    "Hg": 80,
    "Tl": 81,
    "Pb": 82,
    "Bi": 83,
    "Po": 84,
    "At": 85,
    "Rn": 86,
    "Fr": 87,
    "Ra": 88,
    "Ac": 89,
    "Th": 90,
    "Pa": 91,
    "U": 92,
    "Np": 93,
    "Pu": 94,
    "Am": 95,
    "Cm": 96,
    "Bk": 97,
    "Cf": 98,
    "Es": 99,
    "Fm": 100,
    "Md": 101,
    "No": 102,
    "Lr": 103,
    "Rf": 104,
    "Db": 105,
    "Sg": 106,
    "Bh": 107,
    "Hs": 108,
    "Mt": 109,
    "Ds": 110,
    "Rg": 111,
    "Cn": 112,
    "Nh": 113,
    "Fl": 114,
    "Mc": 115,
    "Lv": 116,
    "Ts": 117,
    "Og": 118,
}
