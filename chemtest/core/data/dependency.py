from .type import DataType
from .conversion import DataConversion
from ..hparam import HParams

from typing import List, Set, Tuple, Dict

import torch.utils.data as data


class Dependency:
    """
    The Dependency class is a static class used to register
    :class:`chemtest.core.data.DataType` and
    :class:`chemtest.core.data.DataConversion`. The goal of the Dependency
    class is also to provide the registered classes and initialize the data
    convertion with the configured hyperparameters.
    """

    # current hyperparameters
    __hyperparameters = None
    # DataTypes are stored by name
    __datatypes = {}
    # DataConversions are stored by output's name to make the dependancy tree easier to build
    __dataconversions_outputs = {}

    @staticmethod
    def register_datatype(to_register: type) -> type:
        """
        Register a custom DataType to build the dependency tree.

        :param to_register: a DataType object
        """

        Dependency.__datatypes[to_register.get_name()] = to_register

        return to_register

    @staticmethod
    def register_dataconversion(to_register: type) -> type:
        """
        Register a custom DataConversion to build the dependency tree.

        :param to_register: a DataConversion object
        """

        for output in to_register.outputs:
            lst = Dependency.__dataconversions_outputs.get(output.get_name(), [])
            lst.append(to_register)
            Dependency.__dataconversions_outputs[output.get_name()] = lst

        return to_register

    @staticmethod
    def get_dataconversions(output_type: type) -> List[type]:
        """
        Get all registered the DataConversion class that output a specific
        DataType.

        :param output_type: the targeted DataType
        """
        if Dependency.__hyperparameters is None:
            Dependency.__hyperparameters = HParams()

        lst_conv = Dependency.__dataconversions_outputs.get(output_type.get_name(), [])
        return [conv(Dependency.__hyperparameters) for conv in lst_conv]

    @staticmethod
    def init_convertion(conv_class: type) -> DataConversion:
        """
        Initialize DataConversion object with selected hyperparameters

        :param conv_class: type of the DataConversion
        """

        if Dependency.__hyperparameters is None:
            Dependency.__hyperparameters = HParams()

        return conv_class(Dependency.__hyperparameters)

    @staticmethod
    def get_dataconversions_type(output_type: type) -> List[type]:
        """
        Get a list of type of DataConversion able to output a given DataType

        :param output_type: type of the DataType
        """

        return Dependency.__dataconversions_outputs.get(output_type.get_name(), [])

    @staticmethod
    def get_datatype(type_name: str) -> DataType:
        """
        Get a registered DataType by name.

        :param type_name: name of the DataType
        """

        if type_name not in Dependency.__datatypes:
            raise Exception(f"The {type_name} DataType is not registered")

        return Dependency.__datatypes[type_name]

    @staticmethod
    def set_hyperparameters(hparams: HParams):
        """
        Set the used hyperparameters

        :param hparams: the desired hyperparameters
        """

        Dependency.__hyperparameters = hparams


class DependencyTypeNode:
    """
    DependencyTypeNode is a node of a DependencyTree representing a given
    DataType (stored in property **data_type**). The node contains DataConversion
    able to output it **data_type** as a dictionnary with DataConversion's type
    as key and DependencyConvNode as value. If the type can be built from the
    **sources** (e.g. known input DataType), the property **valid** is True. To
    be valid, the node must have at least one valid DataConversion able to build
    its DataType.

    :param data_type: The targeted DataType
    :param sources: The known DataType
    :param _already_visited: a reserved parameters representing the already visited DataType (to prevent infinit loop)
    """

    def __init__(
        self, data_type: type, sources: Set[type], _already_visited: Set[type] = set()
    ):
        self.data_type = data_type
        self.valid = False
        self.convs = {}

        if data_type in sources:
            self.valid = True
            return

        if data_type in _already_visited:
            return

        _already_visited = _already_visited.copy()
        _already_visited.add(data_type)

        convs_available = Dependency.get_dataconversions_type(data_type)
        for conv in convs_available:
            self.convs[conv] = DependencyConvNode(
                conv, sources.copy(), _already_visited=_already_visited
            )
            if self.convs[conv].valid:
                self.valid = True
                break

    def print(self, depth: int = 0):
        """
        Recursive print of the subtree.

        :param depth: the current depth of the node
        """

        print("    " * depth, self.data_type.get_name(), f"({self.valid})")
        for conv in self.convs.values():
            conv.print(depth=depth + 1)

    def recipe(self, depth: int = 0) -> List[Tuple[int, type]]:
        """
        Recursive generation of the recipe.

        :param depth: the current depth of the node
        """

        assert self.valid

        convs_recipe = []

        for conv in self.convs.values():
            if conv.valid:
                convs_recipe.extend(conv.recipe(depth=depth))
                break

        return convs_recipe


class DependencyConvNode:
    """
    DependencyTypeNode is a node of a DependencyTree representing a given
    DataConversion. The role of the node is the same as
    :class:`chemtest.core.data.DependencyTypeNode` except it represent a
    DataConversion instead of a DataType (stored in property **conv_type**).
    All the requiered inputs are stored. If all requiered inputs are valid the
    DataConversion node is also consitered as valid (accessible with the
    property **valid**).

    :param conv_type: The node's DataConversion
    :param sources: The known DataType
    :param _already_visited: a reserved parameters representing the already visited DataType (to prevent infinit loop)
    """

    def __init__(
        self, conv_type: type, sources: Set[type], _already_visited: Set[type] = set()
    ):
        self.conv_type = conv_type
        self.valid = True
        self.types = {}

        _already_visited = _already_visited.copy()

        for i in conv_type.inputs:
            self.types[i] = DependencyTypeNode(
                i, sources, _already_visited=_already_visited
            )
            if not self.types[i].valid:
                self.valid = False
                return

    def print(self, depth: int = 0):
        """
        Recursive print of the subtree.

        :param depth: the current depth of the node
        """

        print("    " * depth, self.conv_type.get_name(), f"({self.valid})")
        for t in self.types.values():
            t.print(depth=depth + 1)

    def recipe(self, depth: int = 0) -> List[Tuple[int, type]]:
        """
        Recursive generation of the recipe.

        :param depth: the current depth of the node
        """

        assert self.valid

        types_recipe = [(depth, self.conv_type)]

        for t in self.types.values():
            types_recipe.extend(t.recipe(depth=depth + 1))

        return types_recipe


class DependencyTree:
    """Build a dependencies tree. The goal is to build recipe to convert
    input DataType into the targeted DataType.

    :param sources: input DataType
    :param target: targeted DataType
    """

    def __init__(self, sources: List[type], target: type):
        self.target = target
        self.tree = DependencyTypeNode(target, set(sources))
        # self.tree.clean(sources)

    def recipe(self) -> List[DataConversion]:
        """Get the list of conversion functions with their order."""

        if not self.tree.valid:
            return []

        # get recipe (with useless steps)
        list_conv = self.tree.recipe()

        # sort the conversion by order
        list_conv.sort(key=lambda x: x[0], reverse=True)
        list_conv = [conv for _, conv in list_conv]

        # remove duplicated conversion
        inserted = set()
        convs_recipe = []
        for conv in list_conv:
            if conv not in inserted:
                convs_recipe.append(conv)
                inserted.add(conv)

        # build initialized conversion list
        convs_recipe = [Dependency.init_convertion(conv) for conv in convs_recipe]

        return convs_recipe

    @staticmethod
    def apply_recipe(
        data: Dict[type, DataType], recipe: List[DataConversion]
    ) -> Dict[type, DataType]:
        """
        Apply a recipe to the input data to get the targeted type. If
        intermediary DataType are already converted in data_dict, these
        DataType are not computed one more time.

        .. warning::
            The data parameters will be modified by border effect to prevent
            unnecessary data copy.

        :param data: input type
        :param recipe: the recipe of the targeted DataType
        """

        for conv in recipe:

            # if no need of data convertion, skip it
            for output in conv.outputs:
                if output.name not in data:
                    break
            else:
                continue

            if not conv.check_input_type(data):
                lst_input = ", ".join(
                    [data_type.get_name() for data_type in data.keys()]
                )
                raise Exception(
                    f"missing input type to apply the {conv.get_name()} convertion input: {{{lst_input}}}"
                )

            outputs = conv.convert(data)

            if not conv.check_output_type(outputs):
                obtained_types = ", ".join([o.get_name() for o in outputs])
                targeted_types = ", ".join([o.get_name() for o in conv.outputs])
                raise Exception(
                    f"broken convertion {conv.get_name()}, missing output type ({{{obtained_types}}} != {{{targeted_types}}})"
                )

            for o_type, o_data in outputs.items():
                if o_type not in data:
                    data[o_type] = o_data

        return data


class FailToBuildRecipe(Exception):
    pass


class Preprocessing(data.Dataset):
    """Create a dataset with a list of recipe to convert the source data to the
    targeted data.

    :raises FailToBuildRecipe: one of the targets cannot be build

    :param sources: DataType of the input data
    :param targets: targeted DataType
    """

    def __init__(
        self,
        sources: List[type],
        targets: List[type],
    ):
        # build all the recipes to generate targeted DataType
        recipes = []
        for t in targets:
            tree = DependencyTree(sources, t)
            recipe = tree.recipe()

            if (len(recipe) == 0) and (t not in sources):
                raise FailToBuildRecipe(
                    f"Failed to create targeted DataType {t.get_name()}"
                )

            recipes.append(recipe)

        # gather all the recipes and remove repeted operations
        already_used = set()
        self.recipe = []
        for step in sum(recipes, []):
            if step not in already_used:
                self.recipe.append(step)
                already_used.add(step)

        self.sources = sources
        self.targets = targets

    def preprocess(self, data: Dict[type, DataType]) -> Dict[type, DataType]:
        """Apply a recipe to the input data to get the targeted type.

        :param data: input data
        """

        data = DependencyTree.apply_recipe(data, self.recipe)

        return data
