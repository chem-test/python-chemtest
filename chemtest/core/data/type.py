from __future__ import annotations
from abc import ABCMeta, abstractmethod
from enum import Enum
from typing import Tuple, List
import string

import numpy as np
import torch

# import tensorflow as tf


class Framework(Enum):
    """Select the targeted framework"""

    NUMPY = 0
    PYTORCH = 1
    TENSORFLOW = 2


class Padding(Enum):
    """Diffrent padding configuration"""

    NONE = 1
    PADDING_WITH_MASK = 2
    PADDING_WITHOUT_MASK = 3
    CONCATENATE = 4


class Normalize(metaclass=ABCMeta):
    """
    Abstract normalization class meant to be used by DataType. Implementation
    of *normalize* and *unnormalize* requiered.
    """

    def __init__(self):
        pass

    @abstractmethod
    def normalize(self, data: np.ndarray) -> np.ndarray:
        pass

    @abstractmethod
    def unnormalize(self, data: np.ndarray) -> np.ndarray:
        pass


class Normal(Normalize):
    """
    Normalization with simple Normal law.

    :param mean: the mean of the normal law
    :param std: the standard deviation of the normal law
    """

    def __init__(self, mean: float = 0.0, std: float = 1.0):
        self.mean = mean
        self.std = std

    def normalize(self, data: np.ndarray) -> np.ndarray:
        return (data - self.mean) / self.std

    def unnormalize(self, data: np.ndarray) -> np.ndarray:
        return data * self.std + self.mean


class DataType:
    """DataType is a base class for data type implementation. This class define
    name, how to gather sample into a batch, convertion to a specific framework
    and reindexing (in case of indexed data e.g. edges indexing vertices).

    Create a new DataType object with numpy array as input.

    :param raw_data: the input data
    :param framework: the targeted framework
    :param indexing: some data type can store index of other data (for examle an edge can be seen as a pair of vertices e.g. localized by there index). indexing enable the automatic reindexing of the DataType when the sample are gathered into batches
    :param _data: reserved input

    A custom type can be implemeted with a class derived from DataType as follow:

    .. code-block:: python

        class CustomType(DataType):
            def __init__(self, data: np.ndarray, framework: Framework):
                super(CustomType, self).__init__(data, framework = framework, indexing = None)

        custom = CustomType(np.array([]), Framework.PYTORCH)
        custom.name # access the name of the datatype (normalized of needed)
        custom.raw_data # access the raw data (not normalized data)
        custom.data # access the data (numpy array or tensor from torch or tensorflow)
        custom.shape # access the shape of the data (as a tuple of int)
        custom.dtype # access the data type
        custom.mask # access the mask (None if there is no mask)
        custom.sample_index # access the sample index (None if data is not concatenated)
        custom.sample_size # access the sample size (None if data is not concatenated)

    """

    normalize: Normalize = None

    def __init__(
        self,
        data: np.ndarray,
        framework: Framework = Framework.NUMPY,
        indexing: type = None,
        _data: np.ndarray = None,
        normalised: bool = False,
    ):

        assert isinstance(data, (np.ndarray, torch.Tensor))
        assert isinstance(framework, Framework)
        assert isinstance(indexing, (type(None), type))
        assert (indexing is None) or (self.normalize is None)
        assert isinstance(_data, (type(None), np.ndarray))

        if normalised:  # if input data are noramlized
            self.data = data
            if self.normalize is not None:
                if _data is not None:  # bypass the normalization if already known
                    self.raw_data = _data
                else:
                    self.raw_data = self.normalize.unnormalize(data)
            else:
                self.raw_data = data
        else:  # if input data aren't noramlized
            self.raw_data = data
            if self.normalize is not None:
                if _data is not None:  # bypass the normalization if already known
                    self.data = _data
                else:
                    self.data = self.normalize.normalize(data)
            else:
                self.data = data

        self.mask = None
        self.sample_index = None
        self.sample_size = None
        self.framework = framework
        self.indexing = indexing

    @classmethod
    def get_name(cls) -> str:
        """a static method to get the DataType's name"""

        class_name = str(cls.__name__)
        name = ""
        for l_prev, l_next in zip("\0" + class_name, class_name):
            if l_next in string.ascii_uppercase and l_prev in string.ascii_lowercase:
                name += "_"
            name += l_next.lower()

        return name

    @property
    def name(self):
        """A property to get the current DataType name"""

        return self.get_name()

    @property
    def shape(self) -> Tuple[int]:
        """A property to get the shape of the array"""
        return tuple(int(d) for d in self.data.shape)

    @property
    def dtype(self) -> type:
        """A property to get the dtype of the array"""

        return self.data.dtype

    @staticmethod
    def gather(
        samples: List[DataType],
        padding: Padding = Padding.NONE,
        padding_value: float = 0,
    ) -> DataType:
        """Gather multiple sample (as a list of DataType) into a batch (as a
        single DataType).

        :param samples: A list of sample
        :param padding: Padding configuration
        :param padding_value: pad data with a custom paddinf value (0 by default)
        """

        assert isinstance(samples[0], DataType)

        n = len(samples)
        _class = samples[0].__class__
        normalize = samples[0].normalize is not None
        framework = samples[0].framework
        indexing = samples[0].indexing
        dtype = samples[0].data.dtype
        mask = None
        sample_index = None
        sample_size = None
        data = None

        shapes = np.array([list(d.shape) for d in samples])
        bouding_box = np.max(shapes, axis=0)

        if padding == Padding.NONE:
            assert (shapes == bouding_box[None]).all()

            new_data = np.concatenate([[d.raw_data] for d in samples])
            if normalize:
                data = np.concatenate([[d.data] for d in samples])

        elif padding == Padding.PADDING_WITH_MASK:
            new_shape = (n, *bouding_box)
            new_data = np.full(new_shape, padding_value, dtype=dtype)
            data = np.full(new_shape, padding_value, dtype=dtype)
            mask = np.zeros(new_shape, dtype=bool)

            for i in range(n):
                s = tuple([i] + [slice(dim) for dim in samples[i].shape])
                new_data[s] = samples[i].raw_data
                if normalize:
                    data[s] = samples[i].data
                mask[s] = True

        elif padding == Padding.PADDING_WITHOUT_MASK:
            new_shape = (n, *bouding_box)
            new_data = np.full(new_shape, padding_value, dtype=dtype)
            data = np.full(new_shape, padding_value, dtype=dtype)

            for i in range(n):
                s = tuple([i] + [slice(dim) for dim in samples[i].shape])
                new_data[s] = samples[i].raw_data
                if normalize:
                    data[s] = samples[i].data

        elif padding == Padding.CONCATENATE:
            assert (
                shapes[:, 1:] == bouding_box[None, 1:]
            ).all(), (
                f"padding error: {shapes[:, 1:]} don't match {bouding_box[None, 1:]}"
            )

            new_data = np.concatenate([d.raw_data for d in samples])
            if normalize:
                data = np.concatenate([d.data for d in samples])
            sample_index = np.concatenate(
                [np.full((samples[i].shape[0],), i) for i in range(n)]
            )
            sample_size = np.array([samples[i].shape[0] for i in range(n)])

        else:
            raise Exception(f"Unknown padding {padding}")

        rst_data = _class(new_data, framework, indexing=indexing, _data=data)
        rst_data.mask = mask
        rst_data.sample_index = sample_index
        rst_data.sample_size = sample_size
        return rst_data

    def reindex(self, reindex_by: DataType):
        """Reindex indexed data. In case of aggregation by concatenation to
        make batch of data, the indexed data need to be reindexed.

        :param reindex_by: the indexed datatype
        """

        if self.indexing is None:
            return

        assert isinstance(self.indexing, type)
        assert isinstance(reindex_by, self.indexing)

        offset = np.cumsum(np.concatenate(([0], reindex_by.sample_size)))
        self.data = np.transpose(
            np.transpose(self.data) + np.transpose(offset[self.sample_index])
        )

    def to_framework(self):
        """Convert the data into the configured framework."""

        if self.framework == Framework.PYTORCH:
            self.data = torch.from_numpy(self.data)

            if (self.data != self.raw_data) and (self.raw_data is not None):
                self.raw_data = torch.from_numpy(self.raw_data)

            if self.mask is not None:
                self.mask = torch.from_numpy(self.mask)

            if self.sample_index is not None:
                self.sample_index = torch.from_numpy(self.sample_index)

            if self.sample_size is not None:
                self.sample_size = torch.from_numpy(self.sample_size)

    def to(self, device: str):
        """
        Transfer the data to a device (aka cpu/gpu).

        :param device: the device
        """

        if self.framework == Framework.PYTORCH:

            data_is_row = self.data is self.raw_data

            self.data = self.data.to(device)

            if data_is_row:
                self.raw_data = self.data
            else:
                self.raw_data = self.raw_data.to(device)

            if self.mask is not None:
                self.mask = self.mask.to(device)

            if self.sample_index is not None:
                self.sample_index = self.sample_index.to(device)

            if self.sample_size is not None:
                self.sample_size = self.sample_size.to(device)

        elif self.framework == Framework.TENSORFLOW:
            raise NotImplemented()
