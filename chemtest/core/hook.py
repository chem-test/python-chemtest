from __future__ import annotations

import time
import re
from enum import Enum
from typing import Dict, Any, List, Tuple, Union, TYPE_CHECKING
from collections.abc import Callable

from .data import Set

if TYPE_CHECKING:
    from .training import Training


def current_time_if_none(t):
    if t is None:
        return time.time()
    return t


class Step:
    def __init__(self, epoch: int, batch: int, _time: float = None):
        self.epoch = epoch
        self.batch = batch
        self.time = current_time_if_none(_time)

    @staticmethod
    def from_batch(batch: int):
        return Step(epoch=None, batch=batch, _time=None)

    @staticmethod
    def from_epoch(epoch: int):
        return Step(epoch=epoch, batch=None, _time=None)

    @staticmethod
    def unlimited():
        return Step(epoch=None, batch=None, _time=None)

    def update_time(self, _time: float = None):
        self.time = current_time_if_none(_time)

    @staticmethod
    def from_str(arg: str):

        if re.match(r"\Ainf\Z", arg):
            return Step.unlimited()

        if re.match(r"\A\d+(e|epoch|E|Epoch)?\Z", arg):
            arg = int(re.match(r"\A\d+", arg)[0])
            return Step.from_epoch(arg)

        if re.match(r"\A\d+(b|batch|B|Batch)\Z", arg):
            arg = int(re.match(r"\A\d+", arg)[0])
            return Step.from_batch(arg)

        raise Exception(f"invalid input string {arg}")


class Hook:
    class Event(Enum):
        BEGIN_TRAINING = 0
        END_TRAINING = 1

        BEFORE_TRAINING_BATCH = 2
        AFTER_TRAINING_BATCH = 3

        BEFORE_TRAINING_EPOCH = 4
        AFTER_TRAINING_EPOCH = 5

        BEFORE_VALIDATION_BATCH = 6
        AFTER_VALIDATION_BATCH = 7

        BEGIN_VALIDATION = 8
        END_VALIDATION = 9

        BEFORE_TESTING_BATCH = 10
        AFTER_TESTING_BATCH = 11

        BEGIN_TESTING = 12
        END_TESTING = 13

        END = 14

        TIME = 15

    def __init__(
        self,
        event: Event,
        every_n: Union[int, float] = 1,
        callback: Callable[[Training, Step], None] = None,
        current_time: float = None,
        name: str = None,
    ):
        current_time = current_time_if_none(current_time)

        self.target = event
        self.callback = callback
        if name is None:
            self.suffix = ""
        else:
            self.suffix = "_" + name

        if self.target == Hook.Event.TIME:
            self.step = current_time
            self.every_n = float(every_n)
        else:
            self.step = 0
            self.every_n = int(every_n)

    @property
    def name(self):
        return str(self.__class__.__name__) + self.suffix

    def reset_time(self, current_time: float = None):
        current_time = current_time_if_none(current_time)

        assert self.target == Hook.Event.TIME

        self.step = current_time

    def __call__(self, training: Training, step: Step):
        assert self.callback is not None

        self.callback(training, step)

    def __repr__(self) -> str:
        return f"{self.name} on event {self.target.name}"

    def _hook_to_dict(self, current_time: float = None) -> Dict[str, Any]:
        current_time = current_time_if_none(current_time)

        if self.target == Hook.Event.TIME:
            step = current_time - self.step
        else:
            step = self.step

        return {
            "hook": self.name,
            "event": self.target.name,
            "step": step,
            "every_n": self.every_n,
        }


class Hooks:
    def __init__(self):
        self.hooks: Dict[Hook.Event, List[Hook]] = {}

    def add_hook(self, hook: Hook, current_time: float = None):
        current_time = current_time_if_none(current_time)

        ser_hook = hook._hook_to_dict()
        try:
            self._find_hook(ser_hook)
        except KeyError:
            pass
        else:
            raise Exception(
                f"A hook with the same name and event is already registered"
            )

        hooks = self.hooks.get(hook.target, [])

        if hook.target == Hook.Event.TIME:
            hook.reset_time(current_time=current_time)

        hooks.append(hook)
        self.hooks[hook.target] = hooks

    def handle(self, training: Training, event: Hook.Event, step: Step):
        if event not in self.hooks:
            return

        for i, hook in enumerate(self.hooks[event]):

            if event == Hook.Event.TIME:
                elapsed = step.time - hook.step
                if elapsed >= hook.every_n:
                    hook.step = step.time - (elapsed % hook.every_n)
                    hook(training, step)
            else:
                hook.step += 1
                if (hook.step % hook.every_n) == 0:
                    hook(training, step)

            self.hooks[event][i] = hook

    def to_dict(self, current_time: float = None) -> List[Dict[str, Any]]:
        current_time = current_time_if_none(current_time)

        hooks = []

        for hook in sum(self.hooks.values(), []):
            hooks.append(Hook._hook_to_dict(hook, current_time=current_time))

        return hooks

    def _find_hook(self, hook: Dict[str, Any]) -> Tuple[Hook.Event, int]:
        check = ["hook", "event"]

        event = Hook.Event[hook["event"]]
        hook_list = self.hooks[event]

        for i, h in enumerate(hook_list):

            h = Hook._hook_to_dict(h)

            for key in check:
                if h[key] != hook[key]:
                    break
            else:
                return event, i

        raise KeyError

    def from_dict(self, hooks: List[Dict[str, Any]], current_time: float = None):
        current_time = current_time_if_none(current_time)

        for hook in hooks:
            event, index = self._find_hook(hook)

            if event == Hook.Event.TIME:
                step = current_time - float(hook["step"])
                every_n = self.hooks[event][index].every_n
                self.hooks[event][index].step = step % every_n
            else:
                step = int(hook["step"])
                every_n = self.hooks[event][index].every_n
                self.hooks[event][index].step = step

        return hooks


class HookValidation(Hook):
    def __init__(
        self,
        event: Hook.Event,
        every_n: Union[int, float] = 1,
        current_time: float = None,
    ):
        super(HookValidation, self).__init__(
            event, every_n=every_n, current_time=current_time
        )

    def __call__(self, training: Training, step: Step):
        training.validation = True


class HookCheckpoint(Hook):
    def __init__(
        self,
        event: Hook.Event,
        every_n: Union[int, float] = 1,
        current_time: float = None,
    ):
        super(HookCheckpoint, self).__init__(
            event, every_n=every_n, current_time=current_time
        )

    def __call__(self, training: Training, step: Step):
        training.save_checkpoint()


class HookTensorboardScalar(Hook):
    def __init__(
        self,
        event: Hook.Event,
        on_set: Set,
        every_n: Union[int, float] = 1,
        current_time: float = None,
    ):
        super(HookTensorboardScalar, self).__init__(
            event, every_n=every_n, current_time=current_time, name=on_set.name
        )
        self.on_set = on_set

    def __call__(self, training: Training, step: Step):
        training.metrics.gather(self.on_set)
        training.metrics.to_tensorboard(
            self.on_set, training.tensorboard, step=training.current_step.batch
        )


class HookTensorboardHParam(Hook):
    def __init__(
        self,
        event: Hook.Event,
        every_n: Union[int, float] = 1,
        current_time: float = None,
    ):
        super(HookTensorboardHParam, self).__init__(
            event, every_n=every_n, current_time=current_time
        )

    def __call__(self, training: Training, step: Step):
        hparams = training.global_hparams.to_tensorboard_hparams()
        metrics = training.metrics.preview(Set.TESTING)
        training.tensorboard.add_hparams(hparams, metrics)


class HookLrScheduler(Hook):
    def __init__(
        self,
        event: Hook.Event,
        on_set: Set,
        every_n: Union[int, float] = 1,
        current_time: float = None,
    ):
        super(HookLrScheduler, self).__init__(
            event, every_n=every_n, current_time=current_time, name=on_set.name
        )
        self.on_set = on_set

    def __call__(self, training: Training, step: Step):
        training.scheduler.step()


class HookEmaAppend(Hook):
    def __init__(
        self,
        event: Hook.Event,
        on_set: Set,
        every_n: Union[int, float] = 1,
        current_time: float = None,
    ):
        super(HookEmaAppend, self).__init__(
            event, every_n=every_n, current_time=current_time, name=on_set.name
        )
        self.on_set = on_set

    def __call__(self, training: Training, step: Step):
        training.ema(training.model)
