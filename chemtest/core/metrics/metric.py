import torch
from torch.utils.tensorboard import SummaryWriter
import torch.nn.functional as F

# import tensorflow as tf
import numpy as np

from ..data import DataType, Padding, Framework, Batch

import os
from typing import Union, List, Dict, Tuple
from abc import ABCMeta, abstractmethod, abstractproperty


class Metric(metaclass=ABCMeta):
    """
    The Metric class is an abstract object. The goal of this object is to
    simplify the metric's implementation by providing method to handle data,
    generate preview, save and load existing data and export metric to
    tensoboard.

    To implement a valid metric, the **name** property, the
    **calculate_batch_torch** method and the **calculate_batch_tensorflow**
    must be provided. Here is a sort example of a metric define with the l1
    distance.

    .. code-block:: python

        class CustomMetric(Metric):
            @property
            def name(self):
                return "custom_metric"

            def calculate_batch_torch(self, batch, pred):
                return torch.abs(batch.get(self.target).raw_data - pred[self.target].raw_data)

            def calculate_batch_tensorflow(self, batch, pred):
                return tf.abs(batch.get(self.target).raw_data - pred[self.target].raw_data)

    Loss values can be monitored as a metric with the
    :class:`chemtest.core.metrics.Loss` class.

    :param target: The targeted data
    :param normalized: must be calculated on normalized (**True**) or on raw data (**False**)
    :param has_preview: display a preview a the metric's value during the training pocess in the progress bar
    """

    def __init__(
        self,
        target: DataType,
        normalized: bool = False,
        has_preview: bool = True,
    ):
        self.target: DataType = target
        self.normalized: bool = normalized
        self.has_preview: bool = has_preview
        self.batch: List[np.ndarray] = [None]

    @abstractproperty
    def name(self) -> str:
        """Get the name of the metric."""
        pass

    @property
    def loss(self) -> bool:
        """Is the current metric representing the loss?"""
        return False

    def calculate_batch(
        self,
        batch: Batch,
        pred: Dict[type, DataType],
    ) -> Union[torch.Tensor, np.ndarray]:
        """
        Calculate the metric from the current data and prediction of the model.

        :param batch: The given data (input and targeted data)
        :param pred: The prediction of the model
        """

        for data_type, data in pred.items():
            assert isinstance(data_type, type)
            assert isinstance(data, DataType)

        if batch.framework == Framework.PYTORCH:
            return self.calculate_batch_torch(batch, pred)
        elif batch.framework == Framework.TENSORFLOW:
            return self.calculate_batch_tensorflow(batch, pred)
        else:
            raise Exception(
                "metric is calculated from torch.Tensor or tensorflow.Tensor"
            )

    @abstractmethod
    def calculate_batch_torch(
        self,
        batch: Batch,
        pred: Dict[type, DataType],
    ) -> Union[torch.Tensor, np.ndarray]:
        """
        Calculate the metric from pytorch tensors

        :param batch: The given data (input and targeted data)
        :param pred: The prediction of the model
        """
        pass

    @abstractmethod
    def calculate_batch_tensorflow(
        self,
        batch: Batch,
        pred: Dict[type, DataType],
    ) -> np.ndarray:
        """
        Calculate the metric from tensorflow tensors

        :param batch: The given data (input and targeted data)
        :param pred: The prediction of the model
        """
        pass

    def _append_batch(self, data: np.ndarray):
        if self.batch[-1] is None:
            if len(data.shape) > 0:
                shape = (0, data.shape[-1])
            else:
                shape = (0, 1)
            self.batch[-1] = np.empty(shape)

        self.batch[-1] = np.concatenate((self.batch[-1], data), axis=0)

    @staticmethod
    def _tensor_to_numpy(data: Union[torch.Tensor, List[torch.Tensor]]):
        if isinstance(data, list):
            return [Metric._tensor_to_numpy(t) for t in data]

        elif isinstance(data, torch.Tensor):
            return data.detach().cpu().numpy()

        # elif isinstance(data, tf.Tensor):
        #    raise NotImplemented

        elif not isinstance(data, np.ndarray):
            raise Exception(
                "Metrics should be a torch.Tensor, tensorflow.Tensor or numpy.ndarray"
            )

    def handle_batch(
        self,
        batch: Batch,
        pred: Dict[type, DataType],
    ):
        """
        Handle batch's data (only if the metric is not a loss)

        :param batch: The given data (input and targeted data)
        :param pred: The prediction of the model
        """

        assert not self.loss

        results = self.calculate_batch(batch, pred)

        results = self._tensor_to_numpy(results)

        self._append_batch(results)

    def handle_loss(self, loss: torch.Tensor):
        """
        Record the value of the loss (only if the current metric is a loss)

        :param loss: the value of the loss function
        """

        assert self.loss

        loss = self._tensor_to_numpy(loss)

        self._append_batch(loss.reshape((1, 1)))

    def preview(self) -> Dict[str, float]:
        """
        Generate a preview of the metric. This function can be called at any
        time to display a preview in a gui (e.g. before and after the
        **gather** function has been used). This function return a dictionnary
        of values to be able to display multiple values if neede.
        """

        if not self.has_preview:
            return {}

        name = self.name
        if self.batch[-1] is not None:
            return {name: float(self.batch[-1][-1, :].mean())}
        elif len(self.batch) > 1:
            return {name: float(self.batch[-2].mean())}

        return {name: None}

    def gather(self):
        """
        Gather all the collected values together. For example, if the metric
        must be sumed-up and at the end of each epoch, the **gather** method
        should be called after each epoch. The resulting data is the average of
        the collected data.
        """

        assert self.batch[-1] is not None, "no data to gather"

        self.batch[-1] = self.batch[-1].mean(axis=0)
        self.batch.append(None)

    def unserialize(self, data: dict):
        """
        Load data from a dictionnary

        :param data: input data
        """

        assert (
            data["name"] == self.name
        ), "data doesn't match with the current metric name"

        assert (("target" not in data) and (self.target is None)) or (
            data["target"] == self.target.get_name()
        ), "data doesn't match with the current metric target"

        batch = [(np.array(b) if b is not None else None) for b in data["batch"]]
        self.batch = batch

    def serialize(self) -> Dict[str, Union[str, list]]:
        """
        Convert the data into a dictionnary
        """

        batch = [(b.tolist() if isinstance(b, np.ndarray) else b) for b in self.batch]

        data = {"name": self.name, "batch": batch}

        if self.target is not None:
            data["target"] = self.target.get_name()

        return data

    def to_tensorboard(
        self, writer: SummaryWriter, step: int = None, prefix: str = ""
    ) -> str:
        """
        Export the last gathered data to tensorboard.

        :param writer: the tensorboard
        :param step: the current step
        :param prefix: an optional prefix added to de default tag name
        """

        if self.batch[-1] is not None:
            data = self.batch[-1].mean(axis=0).item()
        elif (
            len(self.batch) >= 2
            and isinstance(self.batch[-2], np.ndarray)
            and self.batch[-2].shape[0] > 0
        ):
            data = self.batch[-2].mean().item()
        else:
            raise Exception("no data to add to tensorboard")

        tag = os.path.join(prefix, self.name)
        writer.add_scalar(tag, data, global_step=step)

        return tag

    def get_last_gathered(self) -> float:
        """
        Get the last gathered data.
        """

        if len(self.batch) < 2:
            return None

        return self.batch[-2].mean().item()


class Loss(Metric):
    """
    Record the values of the loss function as a metric.

    :param has_preview: display a preview durring the training
    """

    def __init__(
        self,
        name: str = "loss",
        has_preview: bool = True,
    ):
        super(Loss, self).__init__(
            target=None, normalized=False, has_preview=has_preview
        )
        self._name = name

    @property
    def name(self) -> str:
        """
        Return the name of the metric i.e. "loss"
        """

        return self._name

    @property
    def loss(self) -> bool:
        return True

    def calculate_batch_torch(
        self,
        batch: Batch,
        pred: Dict[type, DataType],
    ) -> Union[torch.Tensor, np.ndarray]:
        raise NotImplemented

    def calculate_batch_tensorflow(
        self,
        batch: Batch,
        pred: Dict[type, DataType],
    ) -> Union[torch.Tensor, np.ndarray]:
        raise NotImplemented
