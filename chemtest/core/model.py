from .data.type import DataType
from .data.dataloader import Padding
from .hparam.node import HParamsNode
from typing import Union
import torch
import torch.nn as nn

# import tensorflow as tf

import copy
import os
from typing import Union, List, Tuple, Dict
from abc import ABCMeta, abstractmethod

from .data import Batch, Framework
from .hparam import HParams, DefaultHParams
from .tools import mkdir


class Model(DefaultHParams, metaclass=ABCMeta):
    _hparams_filename = "hparams.json"
    _pytorch_model_filename = "model.pt"
    _tensorflow_model_filename = "model.ckpt"

    _hparams_namespace: str = "model"

    framework: Framework
    inputs_type: List[Tuple[type, Padding]]

    """
    @classmethod
    def get_name(cls) -> str:
        return str(cls.__name__)

    @property
    def scope_name(self) -> str:
        return ".".join([self._hparams_namespace, self.get_name().lower()])

    @property
    def hparams(self) -> HParamsNode:
        return self.global_hparams.get_node(self.scope_name)
    """

    def __init__(
        self, outputs_type: List[Tuple[type, Padding]], global_hparams: HParams = None
    ):
        super(Model, self).__init__(global_hparams=global_hparams)

        self.outputs_type = outputs_type

        self.model = None
        self.build(self.outputs_type, self.hparams)

        self._check_framework()

    def _check_framework(self) -> None:
        pytorch_model = isinstance(self.model, nn.Module)
        # tensorflow_model = isinstance(self.model, tf.Module)
        if pytorch_model and self.framework != Framework.PYTORCH:
            raise Exception(
                "the model of a module must be a torch.nn.Module for pytorch"
            )
        elif pytorch_model == False:
            raise Exception(
                "the model of a module must be a torch.nn.Module class or a tensorflow.Module"
            )

    @abstractmethod
    def forward(self, batch: Batch) -> Dict[type, DataType]:
        return []

    @abstractmethod
    def build(self, outputs_type: List[Tuple[type, Padding]], hparams: HParamsNode):
        self.model = None

    def save(self, dir: str):
        assert self.model is not None

        mkdir(dir)

        config = HParams(default=False)

        with config.scope(self.scope_name, expand=True) as hparams:
            hparams.merge(self.hparams)

        config.save(os.path.join(dir, self._hparams_filename))

        if self.framework == Framework.PYTORCH:
            torch.save(
                self.model.state_dict(), os.path.join(dir, self._pytorch_model_filename)
            )
        elif self.framework == Framework.TENSORFLOW:
            self.model.save_weights(os.path.join(dir, self._tensorflow_model_filename))
        else:
            raise Exception(f"unkown framework {self.framework}")

    def load(self, dir: str):
        hparam_file = os.path.join(dir, self._hparams_filename)
        if not os.path.exists(hparam_file):
            return False

        config = HParams(default=False)
        config.load(hparam_file, fill_default=False)

        with config.scope(self.scope_name, expand=False) as hparams:
            model_hparam = self.global_hparams.__getattr__(self._hparams_namespace)
            model_hparam.__setattr__(self.get_name().lower(), copy.deepcopy(hparams))

        self.build(self.outputs_type, self.hparams)

        if self.framework == Framework.PYTORCH:
            checkpoint = torch.load(
                os.path.join(dir, self._pytorch_model_filename),
                map_location=torch.device("cpu"),
            )
            self.model.load_state_dict(checkpoint)

            self.model.eval()
        elif self.framework == Framework.TENSORFLOW:
            self.model.load_weights(os.path.join(dir, self._tensorflow_model_filename))
        else:
            raise Exception(f"unkown framework {self.framework}")

    def parameters_count(self) -> int:
        if self.framework == Framework.PYTORCH:
            return sum(p.numel() for p in self.model.parameters() if p.requires_grad)
        elif self.framework == Framework.TENSORFLOW:
            raise NotImplemented
        elif self.framework == Framework.NUMPY:
            raise NotImplemented
        else:
            raise Exception(f"unkown framework {self.framework}")

    def to(self, device):
        if self.framework == Framework.PYTORCH:
            self.model = self.model.to(device)
        elif self.framework == Framework.TENSORFLOW:
            raise NotImplemented
        else:
            raise Exception(f"unkown framework {self.framework}")

    def train(self):
        if self.framework == Framework.PYTORCH:
            self.model.train()
        elif self.framework == Framework.TENSORFLOW:
            raise NotImplemented
        else:
            raise Exception(f"unkown framework {self.framework}")

    def eval(self):
        if self.framework == Framework.PYTORCH:
            self.model.eval()
        elif self.framework == Framework.TENSORFLOW:
            raise NotImplemented
        else:
            raise Exception(f"unkown framework {self.framework}")
