import torch
#import tensorflow as tf
import numpy as np
import signal

import os
from json import JSONEncoder


class EMA:
    def __init__(self, model, decay):
        self.decay = decay
        self.shadow = {}
        self.original = {}

        # Register model parameters
        for name, param in model.model.named_parameters():
            if param.requires_grad:
                self.shadow[name] = param.data.clone()

    def __call__(self, model, num_updates=99999):
        decay = min(self.decay, (1.0 + num_updates) / (10.0 + num_updates))
        for name, param in model.model.named_parameters():
            if param.requires_grad:
                assert name in self.shadow
                new_average = (1.0 - decay) * param.data + decay * self.shadow[name]
                self.shadow[name] = new_average.clone()

    def assign(self, model):
        for name, param in model.model.named_parameters():
            if param.requires_grad:
                assert name in self.shadow
                self.original[name] = param.data.clone()
                param.data = self.shadow[name]

    def resume(self, model):
        for name, param in model.model.named_parameters():
            if param.requires_grad:
                assert name in self.shadow
                param.data = self.original[name]

    def to(self, device):
        for k, v in self.shadow.items():
            self.shadow[k] = v.to(device)
        for k, v in self.original.items():
            self.original[k] = v.to(device)


class TensorArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        if isinstance(obj, torch.Tensor):
            return obj.tolist()
        #if isinstance(obj, tf.Tensor):
        #    return obj.numpy().tolist()
        return JSONEncoder.default(self, obj)


def mkdir(dir: str) -> bool:
    try:
        os.makedirs(dir)
    except OSError:
        return True
    except:
        return False

    return True


class TestTimeout(Exception):
    pass


class test_timeout:
    def __init__(self, seconds, error_message=None):
        if error_message is None:
            error_message = "test timed out after {}s.".format(seconds)
        self.seconds = seconds
        self.error_message = error_message

    def handle_timeout(self, signum, frame):
        raise TestTimeout(self.error_message)

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)

    def __exit__(self, exc_type, exc_val, exc_tb):
        signal.alarm(0)
