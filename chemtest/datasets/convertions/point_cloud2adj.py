from chemtest.core.hparam import HParams
from chemtest.core.data import DataConversion, Dependency

import numpy as np

from typing import List

from ..types import PointCloud, Adjacency


@HParams.register_default
@Dependency.register_dataconversion
class PointCloud2Adjacency(DataConversion):
    _hparams_default = {"cutoff": 5.0}

    inputs = [PointCloud]
    outputs = [Adjacency]

    def __init__(self, hparams: HParams = None):
        super(PointCloud2Adjacency, self).__init__(hparams)

    def convert(self, inputs: List[np.ndarray]) -> List[np.ndarray]:
        """Convert a point cloud in an adjacency matrix

        Args:
            inputs (List[np.ndarray]): a point cloud

        Returns:
            List[np.ndarray]: an adjacency matrix
        """

        assert self._check_type(inputs)

        point_cloud = inputs[0]

        adj = np.linalg.norm(point_cloud[:, None, :] - point_cloud[None, :, :], axis=-1)
        adj[adj > self.hparams.cutoff] = 0.0

        return [adj]
