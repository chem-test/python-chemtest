from chemtest.core.hparam import HParams
from chemtest.core.data import DataConversion, Dependency

import numpy as np

from typing import List

from ..types import PointCloud, VerticesCount


@Dependency.register_dataconversion
class PointCloud2VerticesCount(DataConversion):
    inputs = [PointCloud]
    outputs = [VerticesCount]

    def __init__(self, *args, **kwargs):
        super(PointCloud2VerticesCount, self).__init__()

    def convert(self, inputs: List[np.ndarray]) -> List[np.ndarray]:
        """Convert extract the number of point from a PointCloud

        Args:
            inputs (List[np.ndarray]): a PointCloud

        Returns:
            List[np.ndarray]: the number of point
        """

        assert self._check_type(inputs)

        points = inputs[0]
        count = np.array([len(points)])

        return [count]
