from chemtest.core.hparam import HParams
from chemtest.core.data import DataConversion, Dependency, DataType
from ..types import MatrixCell, PointCloud, AtomicNumber, VoxelCell, VoxelElements

import torch
from torch_scatter import scatter_add
import numpy as np
from ase import Atom, Atoms
from scipy.ndimage import maximum_filter, gaussian_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion

from typing import List, Dict
import time
import math


def mat_to_voxel(
    cell: np.ndarray,
    points: np.ndarray,
    nbins_cell: int = 32,
    nbins_atoms: int = 64,
    size: float = 15.0,
    sigma: float = 0.26,
    threshold: float = 0.02,
):
    # generate the voxel of the atoms

    # center the point cloud in a 15A cube
    mean = np.mean(points, 0)
    points = points - mean[None] + 0.5 * size

    # generate the coordinate of each voxel
    grid = np.linspace(
        0, size, nbins_atoms, endpoint=False
    )  # + 0.5 * size / nbins_atoms
    x, y, z = np.meshgrid(grid, grid, grid)
    pos = np.stack((y, x, z), 3)

    # calculate the intensity of each voxel (by distance)
    scale = 1.0 / (2 * sigma ** 2)
    dist = np.linalg.norm(points[None, None, None] - pos[:, :, :, None], axis=4)
    atoms_voxel = np.exp(-scale * dist ** 2)
    atoms_voxel = atoms_voxel * (atoms_voxel >= threshold)

    # Generate the voxel of the cell

    # define a grid: each voxel is represented by it relative position to the center
    grid = np.linspace(-0.5, 0.5, nbins_cell, endpoint=False)  # + 0.5 / nbins_cell
    x, y, z = np.meshgrid(grid, grid, grid)
    pos = np.stack((x, y, z), 3)

    # deforme the coordinate with the base vectors
    pos = (pos.reshape(-1, 3) @ cell).reshape(nbins_cell, nbins_cell, nbins_cell, 3)

    # calculate the new distance and the value of each voxel
    dist = np.linalg.norm(pos, axis=3)
    cell_voxel = np.exp(-scale * dist ** 2)
    cell_voxel = cell_voxel * (cell_voxel >= threshold)

    return atoms_voxel, cell_voxel


@HParams.register_default
@Dependency.register_dataconversion
class Mat2Vox(DataConversion):
    _hparams_default = {
        "voxel_size": 15.0,
        "nbins_cell": 32,
        "nbins_atoms": 64,
        "sigma": 0.26,
        "threshold": 0.02,
    }

    inputs = {MatrixCell, PointCloud, AtomicNumber}
    outputs = {VoxelCell, VoxelElements}

    def convert(self, inputs: Dict[type, DataType]) -> Dict[type, DataType]:
        """Convert a cell and point cloud in is voxel representation

        Args:
            inputs (List[np.ndarray]): a cell and a points cloud

        Returns:
            List[np.ndarray]: a list of two voxel, one for the cell and one for the atomic position
        """

        cell = inputs[MatrixCell].data
        points_clouds = inputs[PointCloud].data
        atomic_number = inputs[AtomicNumber].data

        atoms_voxel, cell_voxel = mat_to_voxel(
            cell,
            np.matmul(points_clouds.reshape(-1, 1, 3), cell).reshape(-1, 3),
            nbins_cell=self.hparams.nbins_cell,
            nbins_atoms=self.hparams.nbins_atoms,
            size=self.hparams.voxel_size,
            sigma=self.hparams.sigma,
            threshold=self.hparams.threshold,
        )

        z = np.unique(atomic_number)
        reverse = -np.ones(128, dtype=int)
        reverse[z] = np.arange(z.shape[0], dtype=int)
        idx = reverse[atomic_number]

        atoms_voxel = (
            scatter_add(torch.from_numpy(atoms_voxel), torch.from_numpy(idx), dim=3)
            .numpy()
            .transpose(3, 0, 1, 2)
        )

        return {
            VoxelCell: VoxelCell(cell_voxel),
            VoxelElements: VoxelElements(atoms_voxel),
        }


def compute_length(axis_val):
    non_zeros = axis_val[axis_val > 0]
    (a,) = np.where(axis_val == non_zeros.min())

    # distance from center in grid space
    N = np.abs(16 - a[0])

    # length of the unit vector
    r_fake = np.sqrt(-2 * 0.26 ** 2 * np.log(non_zeros.min()))  # r_fake = N*(r/32)
    r = r_fake * 32.0 / float(N)
    return r


def compute_angle(ri, rj, rij):
    cos_theta = (ri ** 2 + rj ** 2 - rij ** 2) / (2 * ri * rj)
    theta = math.acos(-cos_theta) * 180 / np.pi  # angle in deg.
    return theta


def define_cell(img):

    a_axis = img[:, 16, 16]
    ra = compute_length(a_axis)
    b_axis = img[16, :, 16]
    rb = compute_length(b_axis)
    c_axis = img[16, 16, :]
    rc = compute_length(c_axis)

    ab_axis = np.array([img[i, i, 16] for i in range(32)])
    rab = compute_length(ab_axis)
    bc_axis = np.array([img[16, i, i] for i in range(32)])
    rbc = compute_length(bc_axis)
    ca_axis = np.array([img[i, 16, i] for i in range(32)])
    rca = compute_length(ca_axis)

    alpha = compute_angle(rb, rc, rbc)
    beta = compute_angle(rc, ra, rca)
    gamma = compute_angle(ra, rb, rab)

    atoms = Atoms(cell=[ra, rb, rc, alpha, beta, gamma], pbc=True)
    atoms.append(Atom("Cu", [0.5] * 3))
    pos = atoms.get_positions()
    atoms.set_scaled_positions(pos)

    return atoms.cell.array


def detect_peaks(image):
    neighborhood = generate_binary_structure(3, 2)
    local_max = maximum_filter(image, footprint=neighborhood, mode="wrap") == image

    background = image < 0.02

    eroded_background = binary_erosion(
        background, structure=neighborhood, border_value=1
    )
    detected_peaks = np.logical_and(local_max, np.logical_not(eroded_background))
    return detected_peaks


def reconstruction(image, ele, nbins_atoms=64, size=15.0):
    # image should have dimension of (N,N,N)
    image0 = gaussian_filter(image, sigma=0.15)
    peaks = detect_peaks(image0)

    peaks_coords = np.where(peaks == 1.0)
    return np.stack(peaks_coords, axis=1)[:, [1, 0, 2]] * size / nbins_atoms - 7.5


if __name__ == "__main__":

    from ase.io import read
    from ase.visualize import view

    struct = read(
        "/media/arthur/1b88647d-044b-4439-a658-5171d37ccdfd/data/bi_se/database/geometries/mvc-15954_copy1_opt.vasp_subst_O-Se_V-Bi.vasp"
    )

    pos = struct.get_positions()
    z = struct.numbers
    # np.random.rand(16, 3) * 5

    cell = struct.cell.array
    """
    np.array(
        [
            [3.2286951083493136, -0.0000000000002218, 0.0000003532902048],
            [-0.0000000000002217, 3.2286951083493136, 0.0000003532902048],
            [-1.6143491984692107, -1.6143491984692107, 2.9878408438007549],
        ]
    )
    """

    print("test")

    view(struct)

    t0 = time.time()
    for _ in range(10):
        v_atoms, v_cell = mat_to_voxel(cell, pos)
    t1 = time.time()
    print(t1 - t0)

    import matplotlib
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt

    matplotlib.use("TkAgg")

    ax = plt.figure().add_subplot(projection="3d")

    mean = np.mean(pos, 0)
    pos = (pos - mean[None] + 7.5) / 15 * 64

    z_list = np.unique(z)
    for c_z in z_list:
        mask = c_z == z
        ax.scatter(pos[mask, 0], pos[mask, 1], pos[mask, 2], zorder=1)
        ax.voxels(v_atoms[:, :, :, mask].sum(axis=3) > 0.2, zorder=-1)

    plt.show()

    new_pos = reconstruction(v_atoms.sum(axis=-1), "V")
    mean = np.mean(pos, 0)
    new_pos = new_pos + mean

    cell_prime = define_cell(v_cell)

    print(np.linalg.norm(cell - cell_prime))

    from scipy.spatial import distance_matrix

    dist = distance_matrix(pos, new_pos).argmin(axis=1)

    print(np.abs(pos - new_pos[dist]).mean())
    print(15 / 64)

    exit(0)

    import matplotlib.pyplot as plt

    ax = plt.figure().add_subplot(projection="3d")
    ax.voxels(v_atoms.sum(axis=3) > 0.2)

    ax = plt.figure().add_subplot(projection="3d")
    image0 = gaussian_filter(v_atoms.sum(axis=3), sigma=0.15)
    peaks = detect_peaks(image0)
    ax.voxels(peaks)

    ax = plt.figure().add_subplot(projection="3d")
    grid = np.linspace(0, 15.0, 64, endpoint=False)  # + 0.5 * size / nbins_atoms
    x, y, z = np.meshgrid(grid, grid, grid)
    pos = np.stack((y, x, z), 3)
    scale = 1.0 / (2 * 0.26 ** 2)
    dist = np.linalg.norm(new_pos[None, None, None] - pos[:, :, :, None], axis=4)
    atoms_voxel = np.exp(-scale * dist ** 2)
    atoms_voxel = atoms_voxel * (atoms_voxel >= 0.02)
    ax.voxels(atoms_voxel.sum(axis=3) > 0.2)
    plt.show()
