from chemtest.core.data import DataType, Dataset, elements

import numpy as np
import torch_geometric.datasets as datasets
import tqdm

from .types import *

import warnings
import json
import os
from typing import Dict, List


@Dataset.register
class QM9(Dataset):
    _dir_name = "qm9"
    __preprocess_file_name = "preprocess.json"

    properties = {
        PropertyMu: 0,
        PropertyAlpha: 1,
        PropertyHomo: 2,
        PropertyLumo: 3,
        PropertyDeltaEpsilon: 4,
        PropertyR2: 5,
        PropertyZPVE: 6,
        PropertyU0: 7,
        PropertyU: 8,
        PropertyH: 9,
        PropertyG: 10,
        PropertyCv: 11,
        PropertyU0_atom: 12,
        PropertyU_atom: 13,
        PropertyH_atom: 14,
        PropertyG_atom: 15,
        PropertyA: 16,
        PropertyB: 17,
        PropertyC: 18,
    }

    data_types = [
        Index,
        PointCloud,
        AtomicNumber,
        AtomicIndex,
        MatrixCell,
        Bounds,
        *list(properties.keys()),
    ]

    def __init__(
        self,
        path: str,
        split_config: Dataset.Split = Dataset.Split(0, 1.0),
    ):
        super(QM9, self).__init__(path, split_config)

        self.__qm9 = datasets.QM9(self.path)

        self.labels = None

        self.preprocess()

    def preprocess(self) -> None:
        normalize_file_name = os.path.join(self.path, self.__preprocess_file_name)

        self.elements_index = np.zeros((10,), dtype=np.int)
        for idx, z in enumerate(self.elements):
            self.elements_index[z] = idx

        if not os.path.exists(normalize_file_name):
            labels = {data_type: [] for data_type in self.properties.keys()}

            for data in tqdm.tqdm(
                self,
                unit="mol",
                total=len(self),
                postfix="calculing normalisation parameters",
            ):
                for data_type in self.properties.keys():
                    labels[data_type].append(data[data_type].raw_data)

            for data_type in self.properties.keys():
                labels[data_type] = np.stack(labels[data_type])

            self.labels = {
                data_type: {"mean": float(np.mean(data)), "std": float(np.std(data))}
                for data_type, data in labels.items()
            }
            stats = {
                data_type.get_name(): data for data_type, data in self.labels.items()
            }

            with open(normalize_file_name, "w") as fp:
                json.dump({"statistics": {"data": stats}}, fp, indent=4)

        if self.labels is None:
            with open(normalize_file_name, "r") as fp:
                statistics = json.load(fp)
                param = statistics["statistics"]["data"]

                self.labels = {
                    data_type: param[data_type.get_name()]
                    for data_type in self.properties.keys()
                }

        for label in self.properties.keys():
            mean = self.labels[label]["mean"]
            std = self.labels[label]["std"]
            label.set_normalization_param(mean, std)

    @property
    def elements(self) -> List[int]:
        """List of elements contained in the dataset."""

        return [
            elements["C"],
            elements["H"],
            elements["O"],
            elements["N"],
            elements["F"],
        ]

    def __len__(self) -> int:
        return len(self.__qm9)

    def __getitem__(self, idx: int) -> Dict[type, DataType]:

        if isinstance(idx, (list, np.ndarray)):
            return [self.__getitem__(int(i)) for i in idx]

        if idx >= len(self):
            raise StopIteration

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")

            data = self.__qm9[idx]

        sample = {}

        for data_type, index in self.properties.items():
            d = data.y[0, index].numpy().astype(np.float32)
            sample[data_type] = data_type(d)

        index = np.array([idx], dtype=np.long)
        points_clouds = data.pos.numpy().astype(np.float32)
        atomic_number = data.z.numpy().astype(np.long)
        atomic_index = self.elements_index[atomic_number]
        edge_index = data.edge_index.t().numpy().astype(np.long)

        sample[Index] = Index(index)
        sample[PointCloud] = PointCloud(points_clouds)
        sample[AtomicNumber] = AtomicNumber(atomic_number)
        sample[AtomicIndex] = AtomicIndex(atomic_index)
        sample[MatrixCell] = MatrixCell(np.zeros((3, 3)))
        sample[Bounds] = Bounds(edge_index)

        return sample
