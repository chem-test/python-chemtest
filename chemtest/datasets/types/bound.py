from chemtest.core.data import DataType, Dependency, Framework
from .atomic_number import AtomicNumber


@Dependency.register_datatype
class Bounds(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Bounds, self).__init__(data, framework=framework, indexing=AtomicNumber)
