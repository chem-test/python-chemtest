from chemtest.core.data import DataType, Dependency, Framework
from .atomic_number import AtomicNumber


@Dependency.register_datatype
class Edge(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Edge, self).__init__(data, framework=framework, indexing=AtomicNumber)


@Dependency.register_datatype
class EdgeTriplets(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(EdgeTriplets, self).__init__(
            data, framework=framework, indexing=AtomicNumber
        )


@Dependency.register_datatype
class ReduceExpand(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(ReduceExpand, self).__init__(data, framework=framework, indexing=Edge)
