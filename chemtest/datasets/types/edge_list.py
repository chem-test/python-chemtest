from chemtest.core.data import DataType, Dependency

from .points_clouds import PointCloud


@Dependency.register_datatype
class EdgeList(DataType):
    def __init__(self):
        super(EdgeList, self).__init__((-1, 2), dtype=int, reindex=PointCloud)
