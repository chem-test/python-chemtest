from chemtest.core.data import DataType, Dependency, Framework


@Dependency.register_datatype
class Index(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Index, self).__init__(data, framework=framework, indexing=None)
