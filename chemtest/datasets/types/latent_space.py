from chemtest.core.data import DataType, Dependency, Framework


@Dependency.register_datatype
class LatentVector(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(LatentVector, self).__init__(data, framework=framework)


@Dependency.register_datatype
class PriorMean(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(PriorMean, self).__init__(data, framework=framework)


@Dependency.register_datatype
class PriorLogStd(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(PriorLogStd, self).__init__(data, framework=framework)
