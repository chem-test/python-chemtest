from chemtest.core.data import DataType, Dataset, elements
from chemtest.datasets.types import *

from ase.io import read
from torch_geometric.data import download_url, extract_tar
import numpy as np
import tqdm

import json
import glob
import os
from typing import Dict, List


@Dataset.register
class VODataset(Dataset):
    _dir_name = "v_o"
    _data_dir = "iMatGen-VO_dataset_generated_strctures/VO_dataset"
    _geometrie_dir = "geometries"
    _propertie_dir = "properties"

    properties = {PropertyFormationEnergyPerAtom: "formation_energy_per_atom"}

    data_types = [
        Index,
        PointCloud,
        AtomicNumber,
        AtomicIndex,
        MatrixCell,
        *list(properties.keys()),
    ]

    def __init__(
        self,
        path: str,
        split_config: Dataset.Split = Dataset.Split(0, 1.0),
    ):
        super(VODataset, self).__init__(path, split_config)

        self.samples = []

        self.load(self.path)

    def load(
        self,
        dir: str,
    ):
        vasp = glob.glob(
            os.path.join(dir, self._data_dir, self._geometrie_dir, "*.vasp")
        )

        if len(vasp) == 0:
            print("download V O dataset")
            file_path = download_url(
                "https://github.com/kaist-amsg/imatgen/raw/master/iMatGen-VO_dataset_generated_strctures.tar",
                dir,
            )
            extract_tar(file_path, dir, mode="r")
            os.unlink(file_path)

            vasp = glob.glob(
                os.path.join(dir, self._data_dir, self._geometrie_dir, "*.vasp")
            )

        ids = [os.path.splitext(os.path.split(file)[1])[0] for file in vasp]

        for i in tqdm.tqdm(ids, desc="loading VO dataset"):
            file_geom = os.path.join(
                dir, self._data_dir, self._geometrie_dir, f"{i}.vasp"
            )
            file_prop = os.path.join(
                dir, self._data_dir, self._propertie_dir, f"{i}_pack_Ef.npy"
            )
            mat = read(file_geom)

            energy = np.load(file_prop, allow_pickle=True, encoding="bytes")

            self.samples.append(
                {
                    "cell": mat.get_cell().array,
                    "pos": mat.get_scaled_positions(),
                    "z": mat.numbers,
                    "energy": float(energy),
                }
            )

    def __len__(self) -> int:
        return len(self.samples)

    @property
    def elements(self) -> List[int]:
        """List of elements contained in the dataset."""

        return sorted([elements["V"], elements["O"]])

    def get_sample_size(self, idx: int) -> int:
        return len(self.samples[idx]["z"])

    def __getitem__(self, idx: int) -> Dict[type, DataType]:

        if isinstance(idx, (list, np.ndarray)):
            return [self.__getitem__(int(i)) for i in idx]

        if idx >= len(self):
            raise StopIteration

        data = self.samples[idx]

        sample = {}

        index = np.array([idx], dtype=np.long)
        matrix_cell = np.array(data["cell"], dtype=np.float32)
        points_clouds = np.array(data["pos"], dtype=np.float32)
        atomic_number = np.array(data["z"], dtype=np.long)
        atomic_index = np.where(atomic_number == self.elements[0], 0, 1).astype(int)
        energy = np.array(data["energy"], dtype=np.float32)

        sample[Index] = Index(index)
        sample[MatrixCell] = MatrixCell(matrix_cell)
        sample[PointCloud] = PointCloud(points_clouds, cell=matrix_cell)
        sample[AtomicNumber] = AtomicNumber(atomic_number)
        sample[AtomicIndex] = AtomicIndex(atomic_index)
        sample[PropertyFormationEnergyPerAtom] = PropertyFormationEnergyPerAtom(energy)

        return sample
