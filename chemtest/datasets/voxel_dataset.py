from chemtest.datasets.v_o_dataset import VODataset
from chemtest.datasets.material_projectv2 import MaterialProjectV2
from chemtest.core.data.type import Framework, Padding
from chemtest.datasets import BiSeDataset
from chemtest.core.data import DataType, Dataset, elements, DataLoader
from chemtest.core.hparam import HParams, DefaultHParams
from chemtest.datasets.types import *
from chemtest.datasets.convertions import Mat2Vox

from ase.io import read
from torch_geometric.data import download_url, extract_zip
import numpy as np
import tqdm

import json
import glob
import os
from typing import Dict, List


import h5py


class VoxelDataset:
    _voxel_file = "voxel.hdf5"

    def __init__(self, *args, **kwargs):
        super(VoxelDataset, self).__init__(*args, **kwargs)

        self.voxel_file = None

    def preprocessing(self, num_workers: int = 0):
        file_name = os.path.join(self.path, self._voxel_file)

        elem = np.array(self.elements)
        n_elem = len(elem)
        size = len(self)

        f = h5py.File(file_name, "w")
        elements = f.create_dataset("elements", (size, n_elem), dtype="b")
        sites = f.create_dataset(
            "sites",
            (
                size,
                n_elem,
                self.hparams.nbins_atoms,
                self.hparams.nbins_atoms,
                self.hparams.nbins_atoms,
            ),
            dtype="f",
        )
        cells = f.create_dataset(
            "cells",
            (
                size,
                self.hparams.nbins_cell,
                self.hparams.nbins_cell,
                self.hparams.nbins_cell,
            ),
            dtype="f",
        )

        parent_dataset = None
        for cls in self.__class__.__bases__:
            if Dataset in cls.__mro__:
                parent_dataset = cls
                break

        dataloader = DataLoader(
            parent_dataset(os.path.split(self.path)[0]),
            [
                (VoxelCell, Padding.NONE),
                (VoxelElements, Padding.CONCATENATE),
                (AtomicNumber, Padding.CONCATENATE),
                (Index, Padding.NONE),
            ],
            framework=Framework.NUMPY,
            batch_size=256,
            num_workers=num_workers,
        )

        for data in tqdm.tqdm(dataloader, desc="Generating voxel representation"):
            # print(idx, len(dataloader))
            # print(idx)
            # print(data.index.data)
            # print(data.voxel_cell.shape)
            # print(data.voxel_elements.shape)
            # print(data.atomic_number.data)

            idx = data.index.data.flatten()

            atomic_index = data.atomic_number.sample_index
            sites_index = data.voxel_elements.sample_index
            # print(atomic_index)
            # print(sites_index)

            mask = np.zeros((idx.shape[0], 128), dtype=bool)
            mask[atomic_index, data.atomic_number.data] = True
            mask = mask[:, elem]
            # print(mask)

            for i in range(data.index.shape[0]):
                elements[idx[i]] = mask[i]
                c_data = data.voxel_elements.data[sites_index == i]
                for j,m in enumerate(mask[i]):
                    if m:
                        sites[idx[i], j] = c_data[j]
                cells[idx[i]] = data.voxel_cell.data[i]

    @property
    def data_types(self):
        return super(VoxelDataset, self).data_types

    def get_sample_size(self, idx: int) -> int:
        return 1

    def __getitem__(self, idx: int) -> Dict[type, DataType]:
        if self.voxel_file is None:
            self.voxel_file = h5py.File(os.path.join(self.path, self._voxel_file), "r")

        if isinstance(idx, (list, np.ndarray)):
            return [self.__getitem__(int(i)) for i in idx]

        data = super(VoxelDataset, self).__getitem__(idx)

        cell = self.voxel_file["cells"][idx]
        sites = self.voxel_file["sites"][idx]

        data[VoxelCell] = VoxelCell(cell)
        data[VoxelElements] = VoxelElements(sites)

        return data


@Dataset.register
@HParams.register_default
class VoxelBiSeDataset(VoxelDataset, DefaultHParams, BiSeDataset):
    _hparams_namespace = "bi_se_voxel"
    _hparams_default = {
        "nbins_cell": 32,
        "nbins_atoms": 64,
    }

    data_types = [
        Index,
        VoxelCell,
        VoxelElements,
        PointCloud,
        AtomicNumber,
        AtomicIndex,
        MatrixCell,
        PropertyFormationEnergyPerAtom,
    ]


@Dataset.register
@HParams.register_default
class VoxelVODataset(VoxelDataset, DefaultHParams, VODataset):
    _hparams_namespace = "vo_voxel"
    _hparams_default = {
        "nbins_cell": 32,
        "nbins_atoms": 64,
    }

    data_types = [
        Index,
        VoxelCell,
        VoxelElements,
        PointCloud,
        AtomicNumber,
        AtomicIndex,
        MatrixCell,
        PropertyFormationEnergyPerAtom,
    ]


@Dataset.register
@HParams.register_default
class VoxelMaterialProjectDataset(VoxelDataset, DefaultHParams, MaterialProjectV2):
    _hparams_namespace = "mp_voxel"
    _hparams_default = {
        "nbins_cell": 32,
        "nbins_atoms": 64,
    }

    data_types = [
        Index,
        VoxelCell,
        VoxelElements,
        PointCloud,
        AtomicNumber,
        AtomicIndex,
        MatrixCell,
        PropertyFormationEnergyPerAtom,
        PropertyBandGap,
    ]
