from abc import abstractmethod, ABCMeta
import torch
from torch.utils.tensorboard import SummaryWriter
#import tensorflow as tf
import numpy as np

import matplotlib

matplotlib.rcParams["text.usetex"]
import matplotlib.pyplot as plt

plt.switch_backend("agg")

from chemtest.core.data import DataType, Batch
from chemtest.core.metrics import Metric

from typing import Union, Dict
import os


class RegressionMetric(Metric, metaclass=ABCMeta):
    """
    A generic regression metric. The **to_tensorboard** method also record
    figures showing the relationship between the outcome and the target values.

    .. note::

        No need to implement **calculate_batch_torch** and
        **calculate_batch_tensorflow**. Define **function_torch** and
        **function_tensorflow** is enough.

    :param target: The targeted data
    :param target_name: The displayed name of the target
    :param normalized: must be calculated on normalized (**True**) or on raw data (**False**)
    :param has_preview: display a preview a the metric's value during the training pocess in the progress bar
    """

    def __init__(
        self,
        target: DataType,
        target_name: str = "",
        normalized: bool = False,
        has_preview: bool = True,
    ):
        super(RegressionMetric, self).__init__(
            target=target,
            normalized=normalized,
            has_preview=has_preview,
        )

        self.target_name = target_name
        self.last_pred = []
        self.last_target = []

    @abstractmethod
    def function_torch(self, pred: torch.Tensor, target: torch.Tensor):
        """
        The metric's function for pytorch. The data is already normalize or
        unnormalized if needed.

        :param pred: The predicted data
        :param target: The targeted data
        """

        pass

    @abstractmethod
    def function_tensorflow(self, pred: torch.Tensor, target: torch.Tensor):
        """
        The metric's function for tensorflow. The data is already normalize or
        unnormalized if needed.

        :param pred: The predicted data
        :param target: The targeted data
        """

        pass

    def calculate_batch_torch(
        self,
        batch: Batch,
        pred: Dict[type, DataType],
    ) -> Union[torch.Tensor, np.ndarray]:

        if self.normalized:
            target = batch.get(self.target).data
            pred = pred[self.target].data
        else:
            target = batch.get(self.target).raw_data
            pred = pred[self.target].raw_data

        self.last_pred.append(pred.clone().cpu())
        self.last_target.append(target.clone().cpu())

        return self.function_torch(pred, target)

    def calculate_batch_tensorflow(
        self,
        batch: Batch,
        pred: Dict[type, DataType],
    ) -> Union[torch.Tensor, np.ndarray]:
        raise NotImplemented

    def to_tensorboard(self, writer: SummaryWriter, step: int = None, prefix: str = ""):
        tag = super(RegressionMetric, self).to_tensorboard(
            writer, step=step, prefix=prefix
        )

        last_pred = np.concatenate(self.last_pred)
        last_target = np.concatenate(self.last_target)

        x_min, x_max = last_target.min(), last_target.max()

        plt.plot([x_min, x_max], [x_min, x_max], c="r")
        plt.scatter(last_target, last_pred)

        if self.target_name == "":
            plt.xlabel(f"values (dataset)")
            plt.ylabel(f"values (prediction)")
        else:
            plt.xlabel(f"${self.target_name}$ (dataset)")
            plt.ylabel(f"${self.target_name}$ (prediction)")

        writer.add_figure(os.path.join(tag, "fig"), plt.gcf(), global_step=step)
        plt.close()

        self.last_pred = []
        self.last_target = []
