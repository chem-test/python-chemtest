from typing import List, Tuple, Dict

from ..core.data import Framework, Padding, Batch, DataType
from ..core.hparam import HParams, HParamsNode
from ..core import Model
from ..datasets.types import *

from torch_geometric.nn import models


@HParams.register_default
class DimeNet(Model):
    _hparams_default = {
        "emb_size": 128,
        "num_blocks": 6,
        "num_bilinear": 8,
        "num_spherical": 7,
        "num_radial": 6,
        "cutoff": 5.0,
        "envelope_exponent": 5,
        "num_before_skip": 1,
        "num_after_skip": 2,
        "num_dense_output": 3,
    }
    framework = Framework.PYTORCH
    inputs_type = [
        (PointCloud, Padding.CONCATENATE),
        (AtomicIndex, Padding.CONCATENATE),
    ]

    def build(self, outputs_type: List[Tuple[type, Padding]], hparams: HParamsNode):
        self.model = models.DimeNet(
            hidden_channels=hparams.emb_size,
            out_channels=len(outputs_type),
            num_blocks=hparams.num_blocks,
            num_bilinear=hparams.num_bilinear,
            num_spherical=hparams.num_spherical,
            num_radial=hparams.num_radial,
            cutoff=hparams.cutoff,
            envelope_exponent=hparams.envelope_exponent,
            num_before_skip=hparams.num_before_skip,
            num_after_skip=hparams.num_after_skip,
            num_output_layers=hparams.num_dense_output,
        )

    def forward(self, batch: Batch) -> Dict[type, DataType]:
        out = self.model(
            batch.atomic_index.data,
            batch.point_cloud.data,
            batch.atomic_index.sample_index,
        )

        data_type, _ = self.outputs_type[0]

        return {data_type: data_type(out, framework=self.framework, normalized=True)}
