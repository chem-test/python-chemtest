from .dep.physnet.NeuralNetwork import NeuralNetwork

from ..core.hparam import HParams
from ..core import Module

# WIP


@HParams.register_default
class PhysNet(Module):
    _hparams_default = {}

    def __init__(self, targets: int):
        super(PhysNet, self).__init__()

        self.model = NeuralNetwork(
            F,  # dimensionality of feature vector
            K,  # number of radial basis functions
            sr_cut,  # cutoff distance for short range interactions
            lr_cut=None,  # cutoff distance for long range interactions (default: no cutoff)
            num_blocks=3,  # number of building blocks to be stacked
            num_residual_atomic=2,  # number of residual layers for atomic refinements of feature vector
            num_residual_interaction=2,  # number of residual layers for refinement of message vector
            num_residual_output=1,  # number of residual layers for the output blocks
            use_electrostatic=True,  # adds electrostatic contributions to atomic energy
            use_dispersion=True,  # adds dispersion contributions to atomic energy
            s6=None,  # s6 coefficient for d3 dispersion, by default is learned
            s8=None,  # s8 coefficient for d3 dispersion, by default is learned
            a1=None,  # a1 coefficient for d3 dispersion, by default is learned
            a2=None,  # a2 coefficient for d3 dispersion, by default is learned
            Eshift=0.0,  # initial value for output energy shift (makes convergence faster)
            Escale=1.0,  # initial value for output energy scale (makes convergence faster)
            Qshift=0.0,  # initial value for output charge shift
            Qscale=1.0,  # initial value for output charge scale
            kehalf=7.199822675975274,  # half (else double counting) of the Coulomb constant (default is in units e=1, eV=1, A=1)
            activation_fn=shifted_softplus,  # activation function
        )
