from ..core.hparam import HParams, HParamsNode
from ..core.data import Framework, Padding, Batch, DataType
from ..core import Model
from ..datasets.types import *

from typing import List, Dict, Tuple

from torch_geometric.nn import models


@HParams.register_default
class SchNet(Model):
    _hparams_default = {
        "n_atom_basis": 128,
        "n_filters": 128,
        "n_interactions": 6,
        "cutoff": 10.0,
        "n_gaussians": 50,
    }
    framework = Framework.PYTORCH
    inputs_type = [
        (PointCloud, Padding.CONCATENATE),
        (AtomicIndex, Padding.CONCATENATE),
    ]

    def build(self, outputs_type: List[Tuple[type, Padding]], hparams: HParamsNode):
        assert len(outputs_type) == 1

        self.model = models.SchNet(
            hidden_channels=hparams.n_atom_basis,
            num_filters=hparams.n_filters,
            num_interactions=hparams.n_interactions,
            num_gaussians=hparams.n_gaussians,
            cutoff=hparams.cutoff,
        )

    def forward(self, batch: Batch) -> Dict[type, DataType]:
        out = self.model(
            batch.atomic_index.data,
            batch.point_cloud.data,
            batch.atomic_index.sample_index,
        )

        data_type, _ = self.outputs_type[0]

        return {data_type: data_type(out, framework=self.framework, normalised=True)}

