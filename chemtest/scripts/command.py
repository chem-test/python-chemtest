from argparse import ArgumentParser, Namespace
from abc import ABCMeta, abstractstaticmethod, abstractproperty


class Command(metaclass=ABCMeta):
    @abstractstaticmethod
    def name() -> str:
        return ""

    @abstractstaticmethod
    def init_subparser(subparser: ArgumentParser) -> None:
        pass

    @abstractstaticmethod
    def run(args: Namespace) -> int:
        pass
