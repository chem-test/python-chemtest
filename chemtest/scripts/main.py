def main():
    import argparse

    from chemtest import __version__

    from .default import Default
    from .train import Train
    from .test import Test

    parser = argparse.ArgumentParser(description="Chem-test command line tools")

    parser.add_argument(
        "--version", "-V", action="store_true", help="get current version"
    )

    subparsers = parser.add_subparsers()

    commands = [Default, Train, Test]

    commands = {c.name(): c for c in commands}
    for name, command in commands.items():
        c_parser = subparsers.add_parser(name)
        c_parser.set_defaults(cmd=name)
        command.init_subparser(c_parser)

    args = parser.parse_args()

    if args.version:
        print(f"chem-test version {__version__}")
    else:
        assert args.cmd in commands

        success = commands[args.cmd].run(args)

        exit(success)


if __name__ == "__main__":
    main()