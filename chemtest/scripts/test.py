from .command import Command

from argparse import ArgumentParser, Namespace

import unittest
import importlib


class Test(Command):
    @staticmethod
    def name() -> str:
        return "test"

    @staticmethod
    def init_subparser(subparser: ArgumentParser) -> None:
        subparser.add_argument("modules", nargs="*", default=["all"])

    @staticmethod
    def run(args: Namespace) -> int:
        import os

        os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"

        existing_modules = {
            "all": {"chemtest.scripts.tests"},
            "data": {"chemtest.scripts.tests.data"},
            "examples": {"chemtest.scripts.tests.examples"},
            "hook": {"chemtest.scripts.tests.hook"},
            "hparam": {"chemtest.scripts.tests.hparam"},
            "metrics": {"chemtest.scripts.tests.metrics"},
            "training": {"chemtest.scripts.tests.training"},
        }

        paths = set()
        for m in args.modules:
            paths.update(existing_modules[m])

        modules = (importlib.import_module(i) for i in paths)

        suite = unittest.TestSuite()
        loader = unittest.TestLoader()
        for module in modules:
            tests = loader.loadTestsFromModule(module)
            suite.addTests(tests)

        runner = unittest.TextTestRunner(verbosity=3)

        success = runner.run(suite)

        if len(success.errors) == 0 and len(success.failures) == 0:
            return 0

        return 1
