import unittest

from chemtest.core.data import (
    Dataset,
    Dependency,
    DataType,
    DataConversion,
    Framework,
)
import numpy as np


@Dependency.register_datatype
class Index(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Index, self).__init__(data, framework=framework, indexing=None)


@Dependency.register_datatype
class Type1(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Type1, self).__init__(data, framework=framework, indexing=None)


@Dependency.register_datatype
class Type2(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Type2, self).__init__(data, framework=framework, indexing=None)


@Dependency.register_datatype
class Type3(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Type3, self).__init__(data, framework=framework, indexing=None)


@Dependency.register_dataconversion
class Conv1(DataConversion):
    inputs = {Type1}
    outputs = {Type2}

    def convert(self, inputs):
        assert inputs[Type1].data == 1
        return {Type2: Type2(np.array([2]))}


class TestingDataset(Dataset):
    _dir_name = "testing_dataset"
    data_types = [Index, Type1, Type3]

    def __len__(self) -> int:
        return 100

    def __getitem__(self, idx: int):
        return {
            Index: Index(np.array([idx])),
            Type1: Type1(np.array([1])),
            Type3: Type3(np.full((idx % 3) + 1, 3)),
        }

    @property
    def elements(self):
        return []


class TestTypes(unittest.TestCase):
    def test_name(self):
        from chemtest.core.data import DataType
        import numpy as np

        class Test1(DataType):
            pass

        class TestName2(DataType):
            pass

        class TestNameSec3(DataType):
            pass

        class test_name4(DataType):
            pass

        class test_ABC5(DataType):
            pass

        test1 = Test1(np.zeros((0,)))
        test_name2 = TestName2(np.zeros((0,)))
        test_name_sec3 = TestNameSec3(np.zeros((0,)))
        _test_name4 = test_name4(np.zeros((0,)))
        test_abc5 = test_ABC5(np.zeros((0,)))

        self.assertEqual(Test1.get_name(), test1.name)
        self.assertEqual(test1.name, "test1")
        self.assertEqual(test_name2.name, "test_name2")
        self.assertEqual(test_name_sec3.name, "test_name_sec3")
        self.assertEqual(_test_name4.name, "test_name4")
        self.assertEqual(test_abc5.name, "test_abc5")

    def test_gather_padding_none(self):
        from chemtest.core.data import DataType, Padding
        import numpy as np

        a = DataType(np.random.randn(5, 5))
        b = DataType(np.random.randn(5, 5))
        c = DataType(np.random.randn(5, 5))

        out = DataType.gather([a, b, c], Padding.NONE)

        self.assertEqual(out.shape, (3, 5, 5))
        self.assertTrue(
            (out.data == np.concatenate([[a.data], [b.data], [c.data]])).all()
        )
        self.assertEqual(out.mask, None)
        self.assertEqual(out.sample_index, None)
        self.assertEqual(out.sample_size, None)

    def test_gather_padding_without_mask(self):
        from chemtest.core.data import DataType, Padding
        import numpy as np

        a = DataType(np.random.randn(5, 5))
        b = DataType(np.random.randn(6, 6))
        c = DataType(np.random.randn(9, 9))

        out = DataType.gather([a, b, c], Padding.PADDING_WITHOUT_MASK)

        self.assertEqual(out.shape, (3, 9, 9))
        self.assertTrue((out.data[0, :5, :5] == a.data).all())
        self.assertTrue((out.data[1, :6, :6] == b.data).all())
        self.assertTrue((out.data[2, :9, :9] == c.data).all())
        self.assertEqual(out.mask, None)
        self.assertEqual(out.sample_index, None)
        self.assertEqual(out.sample_size, None)

    def test_gather_padding_with_mask(self):
        from chemtest.core.data import DataType, Padding
        import numpy as np

        a = DataType(np.random.randn(5, 5))
        b = DataType(np.random.randn(6, 6))
        c = DataType(np.random.randn(9, 9))

        out = DataType.gather([a, b, c], Padding.PADDING_WITH_MASK)

        self.assertEqual(out.shape, (3, 9, 9))
        self.assertTrue((out.data[0, :5, :5] == a.data).all())
        self.assertTrue((out.data[1, :6, :6] == b.data).all())
        self.assertTrue((out.data[2, :9, :9] == c.data).all())
        self.assertTrue(
            (out.mask[0, :5, :5] == True).all() and np.sum(out.mask[0]) == 25
        )
        self.assertTrue(
            (out.mask[1, :6, :6] == True).all() and np.sum(out.mask[1]) == 36
        )
        self.assertTrue(
            (out.mask[2, :9, :9] == True).all() and np.sum(out.mask[2]) == 81
        )
        self.assertEqual(out.sample_index, None)
        self.assertEqual(out.sample_size, None)

    def test_gather_concatenate(self):
        from chemtest.core.data import DataType, Padding
        import numpy as np

        a = DataType(np.random.randn(5, 4))
        b = DataType(np.random.randn(6, 4))
        c = DataType(np.random.randn(9, 4))
        index = np.array([0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2])

        out = DataType.gather([a, b, c], Padding.CONCATENATE)

        self.assertEqual(out.shape, (20, 4))
        self.assertTrue((out.data[:5] == a.data).all())
        self.assertTrue((out.data[5:11] == b.data).all())
        self.assertTrue((out.data[11:] == c.data).all())
        self.assertEqual(out.mask, None)
        self.assertTrue((out.sample_index == index).all())
        self.assertTrue((out.sample_size == (5, 6, 9)).all())

    def test_reindex(self):
        from chemtest.core.data import DataType, Padding
        import numpy as np

        a = DataType(np.random.randn(9))
        b = DataType(np.random.randn(4))
        c = DataType(np.random.randn(6))
        e_a = DataType(
            np.array([[0, 1], [2, 3], [4, 5], [6, 7], [8, 0]]), indexing=DataType
        )
        e_b = DataType(np.array([[0, 1], [0, 2], [0, 3]]), indexing=DataType)
        e_c = DataType(np.array([[0, 1], [2, 3], [4, 5], [4, 5]]), indexing=DataType)
        out = DataType.gather([a, b, c], Padding.CONCATENATE)
        e_out = DataType.gather([e_a, e_b, e_c], Padding.CONCATENATE)
        e_out.reindex(out)
        ground_truth = np.array(
            [
                [0, 1],
                [2, 3],
                [4, 5],
                [6, 7],
                [8, 0],
                [9, 10],
                [9, 11],
                [9, 12],
                [13, 14],
                [15, 16],
                [17, 18],
                [17, 18],
            ]
        )
        self.assertTrue((e_out.data == ground_truth).all())
        ground_truth = np.array([0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 2])
        self.assertTrue((e_out.sample_index == ground_truth).all())

    def test_to_framework(self):
        from chemtest.core.data import DataType, Framework
        import numpy as np
        import torch

        # import tensorflow as tf

        data = np.random.randn(4, 4)
        data_np = DataType(data, framework=Framework.NUMPY)
        data_torch = DataType(data, framework=Framework.PYTORCH)
        # data_tf = DataType(data, framework=Framework.TENSORFLOW)

        data_np.to_framework()
        data_torch.to_framework()
        # data_tf.to_framework()

        self.assertIsInstance(data_np.data, np.ndarray)
        self.assertIsInstance(data_torch.data, torch.Tensor)
        # self.assertIsInstance(data_tf.data, tf.Tensor)

        self.assertTrue((data == data_np.data).all())
        self.assertTrue((data == data_torch.data.numpy()).all())
        # self.assertTrue((data == data_tf.data.numpy()).all())

    def test_normalize_padding_none(self):
        from chemtest.core.data import DataType, Padding, Normal
        import numpy as np

        class NormalizedType(DataType):
            normalize = Normal(mean=1, std=2)

        raw_a = np.random.randn(5, 5)
        raw_b = np.random.randn(5, 5)
        raw_c = np.random.randn(5, 5)
        a = NormalizedType(raw_a)
        b = NormalizedType(raw_b)
        c = NormalizedType(raw_c)

        self.assertTrue((a.raw_data == raw_a).all())
        self.assertTrue((b.raw_data == raw_b).all())
        self.assertTrue((c.raw_data == raw_c).all())
        self.assertTrue((a.data == (raw_a - 1) / 2).all())
        self.assertTrue((b.data == (raw_b - 1) / 2).all())
        self.assertTrue((c.data == (raw_c - 1) / 2).all())

        out = NormalizedType.gather([a, b, c], Padding.NONE)

        self.assertEqual(out.shape, (3, 5, 5))
        self.assertTrue((out.data[0] == a.data).all())
        self.assertTrue((out.data[1] == b.data).all())
        self.assertTrue((out.data[2] == c.data).all())
        self.assertTrue((out.raw_data[0] == raw_a).all())
        self.assertTrue((out.raw_data[1] == raw_b).all())
        self.assertTrue((out.raw_data[2] == raw_c).all())

    def test_normalize_padding_without_mask(self):
        from chemtest.core.data import DataType, Normal, Padding
        import numpy as np

        class NormalizedType(DataType):
            normalize = Normal(mean=1, std=2)

        raw_a = np.random.randn(5, 5)
        raw_b = np.random.randn(6, 6)
        raw_c = np.random.randn(9, 9)
        a = NormalizedType(raw_a)
        b = NormalizedType(raw_b)
        c = NormalizedType(raw_c)

        self.assertTrue((a.raw_data == raw_a).all())
        self.assertTrue((b.raw_data == raw_b).all())
        self.assertTrue((c.raw_data == raw_c).all())
        self.assertTrue((a.data == (raw_a - 1) / 2).all())
        self.assertTrue((b.data == (raw_b - 1) / 2).all())
        self.assertTrue((c.data == (raw_c - 1) / 2).all())

        out = NormalizedType.gather([a, b, c], Padding.PADDING_WITHOUT_MASK)

        self.assertEqual(out.shape, (3, 9, 9))
        self.assertTrue((out.data[0, :5, :5] == a.data).all())
        self.assertTrue((out.data[1, :6, :6] == b.data).all())
        self.assertTrue((out.data[2, :9, :9] == c.data).all())
        self.assertTrue((out.raw_data[0, :5, :5] == raw_a).all())
        self.assertTrue((out.raw_data[1, :6, :6] == raw_b).all())
        self.assertTrue((out.raw_data[2, :9, :9] == raw_c).all())

    def test_normalize_padding_with_mask(self):
        from chemtest.core.data import DataType, Normal, Padding
        import numpy as np

        class NormalizedType(DataType):
            normalize = Normal(mean=1, std=2)

        raw_a = np.random.randn(5, 5)
        raw_b = np.random.randn(6, 6)
        raw_c = np.random.randn(9, 9)
        a = NormalizedType(raw_a)
        b = NormalizedType(raw_b)
        c = NormalizedType(raw_c)

        self.assertTrue((a.raw_data == raw_a).all())
        self.assertTrue((b.raw_data == raw_b).all())
        self.assertTrue((c.raw_data == raw_c).all())
        self.assertTrue((a.data == (raw_a - 1) / 2).all())
        self.assertTrue((b.data == (raw_b - 1) / 2).all())
        self.assertTrue((c.data == (raw_c - 1) / 2).all())

        out = NormalizedType.gather([a, b, c], Padding.PADDING_WITH_MASK)

        self.assertEqual(out.shape, (3, 9, 9))
        self.assertTrue((out.data[0, :5, :5] == a.data).all())
        self.assertTrue((out.data[1, :6, :6] == b.data).all())
        self.assertTrue((out.data[2, :9, :9] == c.data).all())
        self.assertTrue((out.raw_data[0, :5, :5] == raw_a).all())
        self.assertTrue((out.raw_data[1, :6, :6] == raw_b).all())
        self.assertTrue((out.raw_data[2, :9, :9] == raw_c).all())

    def test_gather_concatenate(self):
        from chemtest.core.data import DataType, Normal, Padding
        import numpy as np

        class NormalizedType(DataType):
            normalize = Normal(mean=1, std=2)

        raw_a = np.random.randn(5, 4)
        raw_b = np.random.randn(6, 4)
        raw_c = np.random.randn(9, 4)

        a = NormalizedType(raw_a)
        b = NormalizedType(raw_b)
        c = NormalizedType(raw_c)

        self.assertTrue((a.raw_data == raw_a).all())
        self.assertTrue((b.raw_data == raw_b).all())
        self.assertTrue((c.raw_data == raw_c).all())
        self.assertTrue((a.data == (raw_a - 1) / 2).all())
        self.assertTrue((b.data == (raw_b - 1) / 2).all())
        self.assertTrue((c.data == (raw_c - 1) / 2).all())

        out = NormalizedType.gather([a, b, c], Padding.CONCATENATE)

        self.assertEqual(out.shape, (20, 4))
        self.assertTrue((out.data[:5] == a.data).all())
        self.assertTrue((out.data[5:11] == b.data).all())
        self.assertTrue((out.data[11:] == c.data).all())
        self.assertTrue((out.raw_data[:5] == raw_a).all())
        self.assertTrue((out.raw_data[5:11] == raw_b).all())
        self.assertTrue((out.raw_data[11:] == raw_c).all())


class TestDataset(unittest.TestCase):
    def test_register_dataset(self):
        from chemtest.core.data import Dataset, DataType
        import numpy as np

        class Zeros(DataType):
            pass

        class Ones(DataType):
            pass

        @Dataset.register
        class TestingDataset0(Dataset):
            _dir_name = "testing_dataset"
            data_types = [Zeros]
            size = 100

            def __len__(self):
                return self.size

            def __getitem__(self, idx: int):
                return {Zeros: Zeros(np.zeros(1))}

            @property
            def elements(self):
                return []

        @Dataset.register
        class TestingDataset1(Dataset):
            _dir_name = "testing_dataset"
            data_types = [Ones]
            size = 100

            def __len__(self):
                return self.size

            def __getitem__(self, idx: int):
                return {Ones: Ones(np.ones(1))}

            @property
            def elements(self):
                return []

        @Dataset.register
        class TestingDataset01(Dataset):
            _dir_name = "testing_dataset"
            data_types = [Zeros, Ones]
            size = 100

            def __len__(self):
                return self.size

            def __getitem__(self, idx: int):
                return {
                    Zeros: Zeros(np.zeros(1)),
                    Ones: Ones(np.ones(1)),
                }

            @property
            def elements(self):
                return []

        self.assertEqual(Dataset.get_dataset("TestingDataset0"), TestingDataset0)
        self.assertEqual(Dataset.get_dataset("TestingDataset1"), TestingDataset1)
        self.assertEqual(Dataset.get_dataset("TestingDataset01"), TestingDataset01)

    def test_derived_dataset(self):
        import tempfile, os
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import Dataset

        split = Dataset.Split(0, 1.0)

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            class TestingDataset(Dataset):
                _dir_name = "testing_dataset"
                data_types = []

                def __len__(self):
                    return 0

                def __getitem__(self, idx: int):
                    return {}

                @property
                def elements(self):
                    return []

            dataset = TestingDataset(os.path.join(dir_name, "data"), split)
            self.assertEqual(dataset[0], {})

    def test_split_load_save(self):
        import tempfile, os, random
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import Dataset

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            for i in range(100):
                file_name = os.path.join(dir_name, f"split_{i}.json")

                seed = random.randint(0, 1000)
                sets = [random.random() for _ in range(3)]
                sets_sum = sum(sets)
                sets = [s / sets_sum for s in sets]
                split_type = Dataset.Split.Type.RANDOM

                original = Dataset.Split(seed, sets[0], sets[1], sets[2], split_type)

                original.save(file_name)
                loaded = Dataset.Split.load(file_name)

                self.assertEqual(original.seed, loaded.seed)
                self.assertEqual(original.training, loaded.training)
                self.assertEqual(original.validation, loaded.validation)
                self.assertEqual(original.testing, loaded.testing)
                self.assertEqual(original.type, loaded.type)

    def test_split_consistency(self):
        import tempfile, os, random
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import DataType, Dataset, Framework
        import numpy as np

        class TestingDataset(Dataset):
            _dir_name = "testing_dataset"
            data_types = [Index]

            def __len__(self) -> int:
                return 100

            def __getitem__(self, idx: int):
                return {Index: Index(np.array([idx]))}

            @property
            def elements(self):
                return []

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:
            for _ in range(100):
                seed = random.randint(0, 1000)

                o_split = Dataset.Split(seed, 0.8, 0.1, 0.1)
                o_dataset = TestingDataset(os.path.join(dir_name, "data"), o_split)
                [o_train, o_valid, o_test] = o_dataset.split()

                n_split = Dataset.Split(seed, 0.8, 0.1, 0.1)
                n_dataset = TestingDataset(os.path.join(dir_name, "data"), n_split)
                [n_train, n_valid, n_test] = n_dataset.split()

                o_train = np.array([sample[Index].data for sample in o_train])
                o_valid = np.array([sample[Index].data for sample in o_valid])
                o_test = np.array([sample[Index].data for sample in o_test])

                n_train = np.array([sample[Index].data for sample in n_train])
                n_valid = np.array([sample[Index].data for sample in n_valid])
                n_test = np.array([sample[Index].data for sample in n_test])

                self.assertTrue((o_train == n_train).all())
                self.assertTrue((o_valid == n_valid).all())
                self.assertTrue((o_test == n_test).all())

    def test_split_check_size(self):
        from chemtest.core.data import Dataset

        scenarii = [
            # by number of sample
            {"size": 150, "split": [100, 20, 30], "valid": True},
            {"size": 150, "split": [80, 20, 30], "valid": True},
            {"size": 150, "split": [120, 20, 30], "valid": False},
            {"size": 150, "split": [120, 0, 0], "valid": True},
            {"size": 150, "split": [200, 0, 0], "valid": False},
            {"size": 150, "split": [0, 120, 0], "valid": True},
            {"size": 150, "split": [0, 200, 0], "valid": False},
            {"size": 150, "split": [0, 0, 120], "valid": True},
            {"size": 150, "split": [0, 0, 200], "valid": False},
            {"size": 150, "split": [60, 60, 0], "valid": True},
            {"size": 150, "split": [100, 100, 0], "valid": False},
            {"size": 150, "split": [0, 60, 60], "valid": True},
            {"size": 150, "split": [0, 100, 100], "valid": False},
            {"size": 150, "split": [60, 0, 60], "valid": True},
            {"size": 150, "split": [100, 0, 100], "valid": False},
            # by percentage
            {"size": 150, "split": [0.8, 0.1, 0.1], "valid": True},
            {"size": 150, "split": [0.7, 0.1, 0.1], "valid": True},
            {"size": 150, "split": [0.9, 0.1, 0.1], "valid": False},
            # mite
            {"size": 150, "split": [100, 0.1, 0.1], "valid": True},
            {"size": 150, "split": [0.7, 20, 0.1], "valid": True},
            {"size": 150, "split": [0.9, 10, 0.1], "valid": False},
        ]

        for config in scenarii:
            split = Dataset.Split(
                0,
                config["split"][0],
                validation=config["split"][1],
                testing=config["split"][2],
            )
            is_valid, sets_size = split.check_split(config["size"])

            split_size = [
                int(s * config["size"]) if type(s) == float else s
                for s in config["split"]
            ]

            self.assertEqual(is_valid, config["valid"])
            self.assertEqual(sets_size, split_size)


class TestPreprocessing(unittest.TestCase):
    def test_build(self):
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import (
            Preprocessing,
            Dependency,
            DataType,
            DataConversion,
            FailToBuildRecipe,
        )
        import numpy as np

        @Dependency.register_datatype
        class Type4(DataType):
            pass

        @Dependency.register_datatype
        class Type5(DataType):
            pass

        @Dependency.register_datatype
        class Type6(DataType):
            pass

        @Dependency.register_datatype
        class Type7(DataType):
            pass

        @Dependency.register_datatype
        class Type8(DataType):
            pass

        @Dependency.register_datatype
        class Type9(DataType):
            pass

        @Dependency.register_datatype
        class Type10(DataType):
            pass

        @Dependency.register_dataconversion
        class Conv7(DataConversion):
            inputs = {Type6}
            outputs = {Type5}

            def convert(self, inputs):
                assert inputs[Type6].data == 6
                return {Type5: Type5(np.array([5]))}

        @Dependency.register_dataconversion
        class Conv8(DataConversion):
            inputs = {Type9}
            outputs = {Type6}

            def convert(self, inputs):
                assert inputs[Type9].data == 9
                return {Type6: Type6(np.array([6]))}

        @Dependency.register_dataconversion
        class Conv9(DataConversion):
            inputs = {Type10}
            outputs = {Type9}

            def convert(self, inputs):
                assert inputs[Type10].data == 10
                return {Type9: Type9(np.array([9]))}

        @Dependency.register_dataconversion
        class Conv10(DataConversion):
            inputs = {Type8}
            outputs = {Type10}

            def convert(self, inputs):
                assert inputs[Type8].data == 8
                return {Type10: Type10(np.array([10]))}

        @Dependency.register_dataconversion
        class Conv2(DataConversion):
            inputs = {Type2, Type3}
            outputs = {Type4}

            def convert(self, inputs):
                assert inputs[Type2].data == 2
                assert inputs[Type3].data == 3
                return {Type4: Type4(np.array([4]))}

        @Dependency.register_dataconversion
        class Conv3(DataConversion):
            inputs = {Type2}
            outputs = {Type1}

            def convert(self, inputs):
                assert inputs[Type2].data == 2
                return {Type1: Type1(np.array([1]))}

        @Dependency.register_dataconversion
        class Conv4(DataConversion):
            inputs = {Type2, Type4, Type5}
            outputs = {Type6}

            def convert(self, inputs):
                assert inputs[Type2].data == 2
                assert inputs[Type4].data == 4
                assert inputs[Type5].data == 5
                return {Type6: Type6(np.array([6]))}

        @Dependency.register_dataconversion
        class Conv5(DataConversion):
            inputs = {Type6}
            outputs = {Type7}

            def convert(self, inputs):
                assert inputs[Type6].data == 6
                return {Type7: Type7(np.array([7]))}

        @Dependency.register_dataconversion
        class Conv6(DataConversion):
            inputs = {Type4, Type7}
            outputs = {Type8}

            def convert(self, inputs):
                assert inputs[Type4].data == 4
                assert inputs[Type7].data == 7
                return {Type8: Type8(np.array([8]))}

        with test_timeout(1):

            # failed cases
            self.assertRaises(
                FailToBuildRecipe,
                lambda: Preprocessing(sources=[Type1, Type5], targets=[Type3]),
            )
            self.assertRaises(
                FailToBuildRecipe,
                lambda: Preprocessing(sources=[Type1, Type2, Type6], targets=[Type4]),
            )
            self.assertRaises(
                FailToBuildRecipe,
                lambda: Preprocessing(sources=[Type1, Type2, Type6], targets=[Type3]),
            )
            self.assertRaises(
                FailToBuildRecipe,
                lambda: Preprocessing(sources=[Type2], targets=[Type6]),
            )

            # success cases
            prep1 = Preprocessing(sources=[Type1, Type3, Type5], targets=[Type6])
            prep2 = Preprocessing(sources=[Type1], targets=[Type2])
            prep3 = Preprocessing(sources=[Type1, Type3, Type6], targets=[Type2])
            prep4 = Preprocessing(sources=[Type1, Type3, Type5], targets=[Type4])
            prep5 = Preprocessing(sources=[Type2, Type5], targets=[Type1])
            prep6 = Preprocessing(sources=[Type2, Type3, Type5], targets=[Type1, Type4])
            prep7 = Preprocessing(sources=[Type2, Type3, Type5], targets=[Type8])

            # apply preprocessing
            pro1 = prep1.preprocess(
                {
                    Type1: Type1(np.array([1])),
                    Type3: Type3(np.array([3])),
                    Type5: Type5(np.array([5])),
                }
            )
            pro2 = prep2.preprocess({Type1: Type1(np.array([1]))})
            pro3 = prep3.preprocess(
                {
                    Type1: Type1(np.array([1])),
                    Type3: Type3(np.array([3])),
                    Type6: Type6(np.array([6])),
                }
            )
            pro4 = prep4.preprocess(
                {
                    Type1: Type1(np.array([1])),
                    Type3: Type3(np.array([3])),
                    Type5: Type6(np.array([5])),
                }
            )
            pro5 = prep5.preprocess(
                {Type2: Type2(np.array([2])), Type5: Type5(np.array([5]))}
            )
            pro6 = prep6.preprocess(
                {
                    Type2: Type2(np.array([2])),
                    Type3: Type3(np.array([3])),
                    Type5: Type5(np.array([5])),
                }
            )
            pro7 = prep7.preprocess(
                {
                    Type2: Type2(np.array([2])),
                    Type3: Type3(np.array([3])),
                    Type5: Type5(np.array([5])),
                }
            )

            # test final result
            self.assertEqual(pro1[Type6].data, 6)
            self.assertEqual(pro2[Type2].data, 2)
            self.assertEqual(pro3[Type2].data, 2)
            self.assertEqual(pro4[Type4].data, 4)
            self.assertEqual(pro5[Type1].data, 1)
            self.assertEqual(pro6[Type1].data, 1)
            self.assertEqual(pro6[Type4].data, 4)
            self.assertEqual(pro7[Type8].data, 8)


class TestDataLoader(unittest.TestCase):
    def test_dataloader_init(self):
        import tempfile, os
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import DataLoader, Dataset, Framework, Padding

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            dataloader = DataLoader(
                TestingDataset(
                    os.path.join(dir_name, "testing"), Dataset.Split(1, 1.0)
                ),
                targets=[
                    (Index, Padding.CONCATENATE),
                    (Type1, Padding.CONCATENATE),
                    (Type2, Padding.CONCATENATE),
                    (Type3, Padding.PADDING_WITH_MASK),
                ],
                framework=Framework.NUMPY,
                batch_size=4,
                num_workers=1,
            )

            index = []
            for batch in dataloader:
                batch.to("cpu")

                index.append(batch.index.data.reshape(-1))

                self.assertTrue((batch.type1.data == 1).all())
                self.assertIsInstance(batch.type1.data, np.ndarray)
                self.assertIsInstance(batch.type1.raw_data, np.ndarray)
                self.assertIs(batch.type1.mask, None)
                self.assertIsInstance(batch.type1.sample_index, np.ndarray)
                self.assertIsInstance(batch.type1.sample_size, np.ndarray)

                self.assertTrue((batch.type2.data == 2).all())
                self.assertIsInstance(batch.type2.data, np.ndarray)
                self.assertIsInstance(batch.type2.raw_data, np.ndarray)
                self.assertIs(batch.type2.mask, None)
                self.assertIsInstance(batch.type2.sample_index, np.ndarray)
                self.assertIsInstance(batch.type2.sample_size, np.ndarray)

                self.assertTrue((batch.type3.data[batch.type3.mask] == 3).all())
                self.assertIsInstance(batch.type3.data, np.ndarray)
                self.assertIsInstance(batch.type3.raw_data, np.ndarray)
                self.assertIsInstance(batch.type3.mask, np.ndarray)
                self.assertIs(batch.type3.sample_index, None)
                self.assertIs(batch.type3.sample_size, None)

            index = np.concatenate(index)
            index = np.sort(index)
            self.assertTrue((index == np.arange(0, 100, dtype=int)).all())

    def test_pytorch(self):
        import tempfile, os
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import DataLoader, Dataset, Framework, Padding
        import torch

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            dataloader = DataLoader(
                TestingDataset(
                    os.path.join(dir_name, "testing"), Dataset.Split(1, 1.0)
                ),
                targets=[
                    (Index, Padding.CONCATENATE),
                    (Type1, Padding.CONCATENATE),
                    (Type2, Padding.CONCATENATE),
                    (Type3, Padding.PADDING_WITH_MASK),
                ],
                framework=Framework.PYTORCH,
                batch_size=4,
                num_workers=1,
            )
            index = []
            for batch in dataloader:
                batch.to("cpu")

                index.append(batch.index.data.view(-1).numpy())

                self.assertTrue((batch.type1.data == 1).all())
                self.assertTrue(isinstance(batch.type1.data, torch.Tensor))
                self.assertTrue(isinstance(batch.type1.raw_data, torch.Tensor))
                self.assertIs(batch.type1.mask, None)
                self.assertTrue(isinstance(batch.type1.sample_index, torch.Tensor))
                self.assertTrue(isinstance(batch.type1.sample_size, torch.Tensor))

                self.assertTrue((batch.type2.data == 2).all())
                self.assertTrue(isinstance(batch.type2.data, torch.Tensor))
                self.assertTrue(isinstance(batch.type2.raw_data, torch.Tensor))
                self.assertIs(batch.type2.mask, None)
                self.assertTrue(isinstance(batch.type2.sample_index, torch.Tensor))
                self.assertTrue(isinstance(batch.type2.sample_size, torch.Tensor))

                self.assertTrue((batch.type3.data[batch.type3.mask] == 3).all())
                self.assertTrue(isinstance(batch.type3.data, torch.Tensor))
                self.assertTrue(isinstance(batch.type3.raw_data, torch.Tensor))
                self.assertTrue(isinstance(batch.type3.mask, torch.Tensor))
                self.assertIs(batch.type3.sample_index, None)
                self.assertIs(batch.type3.sample_size, None)

            index = np.concatenate(index)
            index = np.sort(index)
            self.assertTrue((index == np.arange(0, 100, dtype=int)).all())
