import unittest
import os
import tempfile

import chemtest
from chemtest.core.tools import test_timeout

targeted_hparams = """{
    "module": {
        "schnet": {
            "atomwise": {
                "n_layers": 2
            },
            "schnet": {
                "n_atom_basis": 64,
                "n_filters": 128,
                "n_interactions": 6
            }
        }
    },
    "training": {
        "trainschnet": {
            "batch_size": 32,
            "optimizer": {
                "lr": 0.0001
            }
        }
    }
}"""


class TestGettingStarted(unittest.TestCase):
    def test_default(self):
        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:
            tmp_hparam = os.path.join(dir_name, "hparam.json")
            abs_chemtest = os.path.abspath(os.path.split(chemtest.__file__)[0])
            getting_started_file = os.path.join(
                abs_chemtest, "examples/getting_started.py"
            )
            os.system(f"chem-test default {getting_started_file} -o {tmp_hparam}")
            with open(tmp_hparam, "r") as fp:
                content = fp.read()
                self.assertEqual(content, targeted_hparams)
