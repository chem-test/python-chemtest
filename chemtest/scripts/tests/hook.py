import unittest


class TestHook(unittest.TestCase):
    flag = False

    def test_time(self):
        from chemtest.core import Hook, Step, Hooks

        import random

        TestHook.flag = False

        def rise_flag(*kargs):
            assert not TestHook.flag
            TestHook.flag = True

        period = 3.14159
        time = 0.0
        max_dt = 1.0
        hooks = Hooks()
        hook = Hook(
            Hook.Event.TIME, every_n=period, callback=rise_flag, current_time=time
        )
        hooks.add_hook(hook, current_time=time)

        for _ in range(10000):
            prev_step = int(time // period)
            time += random.random() * max_dt
            current_step = int(time // period)

            hooks.handle(None, Hook.Event.TIME, Step(0, 0, _time=time))

            if prev_step == current_step:
                self.assertFalse(TestHook.flag)
            else:
                self.assertTrue(TestHook.flag)
                TestHook.flag = False

    def test_event(self):
        from chemtest.core import Hook, Step, Hooks

        TestHook.flag = False

        def rise_flag(*kargs):
            assert not TestHook.flag
            TestHook.flag = True

        hooks = Hooks()
        hook = Hook(Hook.Event.AFTER_TRAINING_BATCH, callback=rise_flag)
        hooks.add_hook(hook)

        self.assertFalse(TestHook.flag)

        hooks.handle(None, Hook.Event.AFTER_TRAINING_BATCH, Step(0, 0))
        self.assertTrue(TestHook.flag)
        TestHook.flag = False

        hooks.handle(None, Hook.Event.BEFORE_TESTING_BATCH, Step(0, 0))
        self.assertFalse(TestHook.flag)

        hooks.handle(None, Hook.Event.BEFORE_VALIDATION_BATCH, Step(0, 0))
        self.assertFalse(TestHook.flag)

        hooks.handle(None, Hook.Event.AFTER_TRAINING_BATCH, Step(0, 0))
        self.assertTrue(TestHook.flag)
        TestHook.flag = False

        hooks.handle(None, Hook.Event.AFTER_TRAINING_BATCH, Step(0, 0))
        self.assertTrue(TestHook.flag)
        TestHook.flag = False

        hooks.handle(None, Hook.Event.AFTER_TRAINING_BATCH, Step(0, 0))
        self.assertTrue(TestHook.flag)
        TestHook.flag = False

    def test_event_n(self):
        from chemtest.core import Hook, Step, Hooks

        import random

        hooks = Hooks()

        TestHook.flag = False

        def rise_flag(*kargs):
            assert not TestHook.flag
            TestHook.flag = True

        choices = [
            Hook.Event.AFTER_TRAINING_BATCH,
            Hook.Event.AFTER_TESTING_BATCH,
            Hook.Event.AFTER_TRAINING_EPOCH,
        ]

        every_n = 3
        step = 0
        hook = Hook(
            Hook.Event.AFTER_TRAINING_BATCH, every_n=every_n, callback=rise_flag
        )
        hooks.add_hook(hook)

        for _ in range(10000):
            prev = step
            event = random.choice(choices)

            hooks.handle(None, event, Step(0, 0))
            if event == Hook.Event.AFTER_TRAINING_BATCH:
                step += 1

            if ((step % 3) == 0) and (prev != step):
                self.assertTrue(TestHook.flag)
                TestHook.flag = False
            else:
                self.assertFalse(TestHook.flag)

    def test_dict(self):
        from chemtest.core import Hook, Step, Hooks

        class Hook1(Hook):
            def __init__(self, event, every_n=1, name=None):
                super(Hook1, self).__init__(
                    event, every_n=every_n, name=name, current_time=0
                )

            def __call__(self, *kargs):
                return

        class Hook2(Hook):
            def __init__(self, event, every_n=1, name=None):
                super(Hook2, self).__init__(
                    event, every_n=every_n, name=name, current_time=0
                )

            def __call__(self, *kargs):
                return

        class Hook3(Hook):
            def __init__(self, event, every_n=1, name=None):
                super(Hook3, self).__init__(
                    event, every_n=every_n, name=name, current_time=0
                )

            def __call__(self, *kargs):
                return

        class Hook4(Hook):
            def __init__(self, event, every_n=1, name=None):
                super(Hook4, self).__init__(
                    event, every_n=every_n, name=name, current_time=0
                )

            def __call__(self, *kargs):
                return

        hooks = Hooks()

        hooks.add_hook(Hook1(Hook.Event.AFTER_TRAINING_BATCH), current_time=0)
        hooks.add_hook(Hook1(Hook.Event.BEFORE_TRAINING_BATCH), current_time=0)
        hooks.add_hook(Hook1(Hook.Event.TIME, every_n=60), current_time=0)
        hooks.add_hook(Hook1(Hook.Event.BEGIN_TRAINING), current_time=0)
        hooks.add_hook(
            Hook2(Hook.Event.AFTER_TRAINING_BATCH, every_n=5), current_time=0
        )
        hooks.add_hook(
            Hook2(Hook.Event.AFTER_TRAINING_BATCH, name="test"), current_time=0
        )
        hooks.add_hook(
            Hook2(Hook.Event.AFTER_TRAINING_BATCH, name="test2"), current_time=0
        )
        hooks.add_hook(
            Hook3(Hook.Event.AFTER_TRAINING_BATCH, every_n=5), current_time=0
        )
        hooks.add_hook(
            Hook4(Hook.Event.AFTER_TRAINING_BATCH, every_n=3), current_time=0
        )
        hooks.add_hook(Hook4(Hook.Event.END_TRAINING), current_time=0)
        hooks.add_hook(
            Hook4(Hook.Event.BEFORE_TRAINING_BATCH, every_n=2), current_time=0
        )
        hooks.add_hook(Hook4(Hook.Event.TIME, every_n=10), current_time=0)
        hooks.add_hook(
            Hook4(Hook.Event.AFTER_TESTING_BATCH, every_n=10), current_time=0
        )
        hooks.add_hook(
            Hook4(Hook.Event.BEFORE_VALIDATION_BATCH, every_n=16), current_time=0
        )
        hooks.add_hook(Hook4(Hook.Event.BEGIN_TRAINING), current_time=0)

        hooks.handle(None, Hook.Event.BEGIN_TRAINING, Step(1, 1, _time=1.0))
        hooks.handle(None, Hook.Event.BEFORE_TRAINING_EPOCH, Step(1, 1, _time=2.0))
        hooks.handle(None, Hook.Event.BEFORE_TRAINING_BATCH, Step(1, 1, _time=3.0))
        hooks.handle(None, Hook.Event.AFTER_TRAINING_BATCH, Step(1, 1, _time=4.0))
        hooks.handle(None, Hook.Event.BEFORE_TRAINING_BATCH, Step(1, 2, _time=5.0))
        hooks.handle(None, Hook.Event.AFTER_TRAINING_BATCH, Step(1, 2, _time=6.0))
        hooks.handle(None, Hook.Event.BEFORE_TRAINING_BATCH, Step(1, 3, _time=7.0))
        hooks.handle(None, Hook.Event.AFTER_TRAINING_BATCH, Step(1, 3, _time=8.0))
        hooks.handle(None, Hook.Event.BEFORE_VALIDATION_BATCH, Step(1, 3, _time=9.0))
        hooks.handle(None, Hook.Event.AFTER_VALIDATION_BATCH, Step(1, 3, _time=10.0))
        hooks.handle(None, Hook.Event.AFTER_TRAINING_EPOCH, Step(1, 3, _time=11.0))
        hooks.handle(None, Hook.Event.BEFORE_TRAINING_EPOCH, Step(2, 4, _time=12.0))
        hooks.handle(None, Hook.Event.BEFORE_TRAINING_BATCH, Step(2, 4, _time=13.0))
        hooks.handle(None, Hook.Event.AFTER_TRAINING_BATCH, Step(2, 4, _time=14.0))

        data = hooks.to_dict(current_time=15)

        hooks2 = Hooks()

        hooks2.add_hook(Hook1(Hook.Event.AFTER_TRAINING_BATCH), current_time=0)
        hooks2.add_hook(Hook1(Hook.Event.BEFORE_TRAINING_BATCH), current_time=0)
        hooks2.add_hook(Hook1(Hook.Event.TIME, every_n=60), current_time=0)
        hooks2.add_hook(Hook1(Hook.Event.BEGIN_TRAINING), current_time=0)
        hooks2.add_hook(
            Hook2(Hook.Event.AFTER_TRAINING_BATCH, every_n=5), current_time=0
        )
        hooks2.add_hook(
            Hook2(Hook.Event.AFTER_TRAINING_BATCH, name="test"), current_time=0
        )
        hooks2.add_hook(
            Hook2(Hook.Event.AFTER_TRAINING_BATCH, name="test2"), current_time=0
        )
        hooks2.add_hook(
            Hook3(Hook.Event.AFTER_TRAINING_BATCH, every_n=5), current_time=0
        )
        hooks2.add_hook(
            Hook4(Hook.Event.AFTER_TRAINING_BATCH, every_n=3), current_time=0
        )
        hooks2.add_hook(Hook4(Hook.Event.END_TRAINING), current_time=0)
        hooks2.add_hook(
            Hook4(Hook.Event.BEFORE_TRAINING_BATCH, every_n=2), current_time=0
        )
        hooks2.add_hook(Hook4(Hook.Event.TIME, every_n=10), current_time=0)
        hooks2.add_hook(
            Hook4(Hook.Event.AFTER_TESTING_BATCH, every_n=10), current_time=0
        )
        hooks2.add_hook(
            Hook4(Hook.Event.BEFORE_VALIDATION_BATCH, every_n=16), current_time=0
        )
        hooks2.add_hook(Hook4(Hook.Event.BEGIN_TRAINING), current_time=0)

        hooks2.from_dict(data, current_time=15)

        data_cpy = hooks2.to_dict(current_time=15)

        self.assertEqual(data, data_cpy)
