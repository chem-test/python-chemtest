import unittest
import random
import string
import copy
from typing import Union

from chemtest.core.hparam import HParamsNode
from chemtest.core.tools import test_timeout


def random_string(max_length_name: int) -> str:
    length = random.randint(1, max_length_name)
    return "".join([random.choice(string.ascii_letters) for _ in range(length)])


def random_value(targeted_type=[int, float, bool, str]) -> Union[int, float, bool, str]:
    v_type = random.choice(targeted_type)

    if v_type == int:
        return random.randint(-1000, 1000)
    elif v_type == float:
        return random.random() * 1000.0
    elif v_type == bool:
        return random.choice([False, True])
    elif v_type == str:
        return random_string(10)

    raise Exception(f"unexpected type {v_type}")


def random_tree(
    max_depth: int = 4,
    min_depth: int = 2,
    max_node: int = 10,
    max_length_name: int = 5,
    density_subtree: float = 0.5,
) -> dict:
    assert min_depth < max_depth

    tree = {}

    elems = random.randint(1, max_node)
    for _ in range(elems):
        name = random_string(max_length_name)

        if random.random() < density_subtree and max_depth > 0:
            tree[name] = random_tree(
                max_depth - 1, min_depth - 1, max_node, max_length_name, density_subtree
            )
        else:
            tree[name] = random_value()

    if min_depth >= 1:
        name = random_string(max_length_name)

        if max_depth > 0:
            tree[name] = random_tree(
                max_depth - 1, min_depth - 1, max_node, max_length_name, density_subtree
            )

    return tree


def random_path(tree: dict, go_deeper_prob: float = 0.8) -> str:
    key = random.choice(list(tree.keys()))
    value = tree[key]

    path = key

    if type(value) == dict and random.random() <= go_deeper_prob:
        path += "." + random_path(value, go_deeper_prob=go_deeper_prob)

    return path


def valid_path(tree: dict, path: str) -> bool:
    pos = path.find(".")

    if pos == -1:
        elem_name, new_path = path, ""
    else:
        elem_name, new_path = path[:pos], path[pos + 1 :]

    if (type(tree) != dict) or (elem_name not in tree):
        return False

    if new_path == "":
        return path == elem_name
    else:
        return valid_path(tree[elem_name], new_path)


def random_error_path(tree: dict, path: str) -> str:
    while valid_path(tree, path):
        index = random.randint(0, len(path))

        path = path[:index] + random.choice(string.ascii_letters) + path[index + 1 :]

    return path


def is_leaf(tree: dict, path: str) -> bool:
    pos = path.find(".")

    if pos == -1:
        return not isinstance(tree[path], HParamsNode)

    return is_leaf(tree[path[:pos]], path[pos + 1 :])


def random_insertion_path(tree: dict, go_deeper_prob: float = 0.8) -> str:
    path = []

    while len(path) == 0 or random.random() <= go_deeper_prob:
        subtrees = []
        for key, node in tree.items():
            if isinstance(node, dict):
                subtrees.append(key)

        if len(subtrees) == 0:
            break

        key = random.choice(subtrees)

        path.append(key)
        tree = tree[key]

    assert len(path) > 0

    new_node = random_string(5)
    while valid_path(tree, new_node):
        new_node = random_string(5)

    path.append(new_node)

    while random.random() <= go_deeper_prob:
        path.append(random_string(5))

    return path


import json


def tree_merge(src: dict, dst: dict) -> dict:
    for key, value in src.items():
        if isinstance(value, dict):
            if key in dst:
                origin_dst = dst[key]
            else:
                origin_dst = {}

            dst[key] = tree_merge(value, origin_dst)
        else:
            dst[key] = copy.deepcopy(value)

    return dst


def tree_random_modif(tree: dict) -> dict:
    for _ in range(100):
        path = random_path(tree).split(".")
        value = random_value()

        node = tree
        for key in path[:-1]:
            node = node[key]

        node[path[-1]] = value

    return tree


def tree_merge_check(test_case, tree_src, tree_dst):
    node_src = HParamsNode.__from_dict__(tree_src)
    node_dst = HParamsNode.__from_dict__(tree_dst)

    tree_merged = tree_merge(tree_src, tree_dst.copy())

    node_dst.merge(node_src)

    test_case.assertDictEqual(node_dst.__to_dict__(), tree_merged)


class TestNode(unittest.TestCase):
    def test_dict(self):
        for _ in range(10):

            src = random_tree()

            node = HParamsNode.__from_dict__(src)
            dst = node.__to_dict__()

            self.assertEqual(src, dst)

    def test_scope(self):
        for _ in range(300):
            tree = random_tree()
            node = HParamsNode.__from_dict__(tree)

            path = random_path(tree)
            self.assertTrue(node.has_scope(path))

            error_path = random_error_path(tree, path)
            self.assertFalse(node.has_scope(error_path))

    def test_getattr(self):
        for _ in range(10):
            tree = random_tree()
            node = HParamsNode.__from_dict__(tree)
            node.expand(True)

            for _ in range(30):

                path = random_path(tree, go_deeper_prob=1.0).split(".")

                value_tree = tree
                for e_name in path:
                    value_tree = value_tree[e_name]

                value_node = node
                for e_name in path:
                    value_node = value_node.__getattr__(e_name)

                self.assertEqual(value_tree, value_node)

    def test_setattr(self):
        for _ in range(300):
            tree = random_tree()
            node = HParamsNode.__from_dict__(tree)
            node.expand(True)

            tree = node.__to_dict__()

            # set existing element
            path = random_path(tree, go_deeper_prob=1.0).split(".")
            new_value = random_value()

            elem_node = node
            for e_name in path[:-1]:
                elem_node = elem_node.__getattr__(e_name)

            elem_node.__setattr__(path[-1], new_value)

            value_node = node
            for e_name in path:
                value_node = value_node.__getattr__(e_name)

            self.assertEqual(new_value, value_node)

        for _ in range(300):
            tree = random_tree()
            node = HParamsNode.__from_dict__(tree)
            node.expand(True)

            tree = node.__to_dict__()

            # set new element
            path = random_insertion_path(tree)
            new_value = random_value()

            elem_node = node
            for e_name in path[:-1]:
                elem_node = elem_node.__getattr__(e_name)

            elem_node.__setattr__(path[-1], new_value)

            value_node = node
            for e_name in path:
                try:
                    value_node = value_node.__getattr__(e_name)
                except:
                    print(".".join(path[:-1]), new_value)
                    print(valid_path(tree, ".".join(path[:-1])))
                    import json

                    with open("error.json", "w") as fp:
                        json.dump(tree, fp)
                    raise Exception("error")

            self.assertEqual(new_value, value_node)

    def test_expand(self):
        for i in range(300):
            tree = random_tree()
            node = HParamsNode.__from_dict__(tree)

            expand = (i & 1) == 0
            node.expand(expand)

            # set new element
            path = random_insertion_path(tree)
            new_value = random_value()

            try:

                elem_node = node
                for e_name in path[:-1]:
                    elem_node = elem_node.__getattr__(e_name)

                elem_node.__setattr__(path[-1], new_value)

                value_node = node
                for e_name in path:
                    value_node = value_node.__getattr__(e_name)

            except KeyError:
                self.assertFalse(expand)
            else:
                self.assertEqual(new_value, value_node)
                self.assertTrue(expand)

    def test_enter_scope(self):
        index = 0
        for _ in range(10):

            with test_timeout(3):
                tree = random_tree()
                node = HParamsNode.__from_dict__(tree)
                node.expand(False)

                path = random_path(tree)
                while "." not in path:
                    path = random_path(tree)

                path = path[: path.rfind(".")]

                subtree = tree
                for p in path.split("."):
                    subtree = subtree[p]

                with node.scope(path) as s:
                    for _ in range(10):
                        index += 1
                        sub_path = random_path(subtree)
                        self.assertTrue(s.has_scope(sub_path))

    def test_merge(self):
        tree_merge_check(self, {}, {})
        tree_merge_check(self, {"a": 1}, {"b": 2})
        tree_merge_check(self, {"a": 1}, {"a": 2})
        tree_merge_check(self, {"a": 2}, {"a": 1})
        tree_merge_check(self, {"a": {"b": 3, "c": 1}}, {"a": {"d": 1}})
        tree_merge_check(self, {"a": {"d": 1}}, {"a": {"b": 3, "c": 1}})
        tree_merge_check(self, {"a": {"a": 3, "c": 1}}, {"a": {"a": 1}})
        tree_merge_check(self, {"a": {"a": 1}}, {"a": {"a": 3, "c": 1}})
        tree_merge_check(self, {"a": {"a": 3, "e": {"f": 0}, "c": 1}}, {"a": {"a": 1}})
        tree_merge_check(self, {"a": {"a": 1}}, {"a": {"a": 3, "e": {"f": 0}, "c": 1}})


class TestHParams(unittest.TestCase):
    def test_from_dict(self):
        from chemtest.core.hparam import HParams

        for _ in range(100):

            src = random_tree()

            node = HParamsNode.__from_dict__(src)
            dst = node.__to_dict__()

            self.assertDictEqual(src, dst)

        for _ in range(100):

            HParams.reset_default()

            tree = random_tree()
            hparams = HParams(tree, default=True)

            self.assertDictEqual(tree, hparams.to_dict())

        for _ in range(100):

            HParams.reset_default()

            tree1 = random_tree()
            tree2 = random_tree()
            tree3 = random_tree()

            tree = {
                "namespace1": {"testclass1": tree1, "testclass2": tree2},
                "namespace2": {"testclass3": tree3},
            }

            @HParams.register_default
            class TestClass1:
                _hparams_namespace = "namespace1"
                _hparams_default = tree1

            @HParams.register_default
            class TestClass2:
                _hparams_namespace = "namespace1"
                _hparams_default = tree2

            @HParams.register_default
            class TestClass3:
                _hparams_namespace = "namespace2"
                _hparams_default = tree3

            hparams = HParams({}, default=True)

            self.assertDictEqual(tree, hparams.to_dict())

            tree1_edited = tree1.copy()

            for _ in range(10):
                original_value = {}

                while isinstance(original_value, dict):

                    path = random_path(tree1_edited).split(".")

                    node = tree1_edited
                    for p in path[:-1]:
                        node = node[p]

                    original_value = node[path[-1]]

                node[path[-1]] = random_value(targeted_type=[type(original_value)])

            new_tree = {"namespace1": {"testclass1": tree1_edited}}

            hparams = HParams(new_tree, default=True)

            self.assertDictEqual(tree_merge(new_tree, tree.copy()), hparams.to_dict())

    def test_save_load(self):
        import tempfile
        import os

        from chemtest.core.hparam import HParams

        with tempfile.TemporaryDirectory() as dir_name:

            for i in range(100):

                file_name = os.path.join(dir_name, f"{i}_1.json")

                tree = random_tree()
                hparams = HParams(tree, default=False)

                hparams.save(file_name)

                loaded = HParams(default=False)
                loaded.load(file_name, fill_default=False)

                self.assertDictEqual(hparams.to_dict(), loaded.to_dict())

                HParams.reset_default()

                default_class = random_tree()

                @HParams.register_default
                class TestClass1:
                    _hparams_namespace = "namespace1"
                    _hparams_default = default_class

                default = {"namespace1": {"testclass1": default_class}}

                file_name = os.path.join(dir_name, f"{i}_2.json")

                tree = random_tree()
                hparams = HParams(tree, default=False)

                hparams.save(file_name)

                loaded = HParams(default=False)
                loaded.expand(True)
                loaded.load(file_name, fill_default=True)

                self.assertDictEqual(tree_merge(tree, default.copy()), loaded.to_dict())

    def test_getter_setter(self):
        from chemtest.core.hparam import HParams

        # test __getattr__
        for _ in range(100):
            tree = random_tree()

            hparams = HParams(tree, default=False)

            value_tree = {}

            while isinstance(value_tree, dict):
                path = random_path(tree).split(".")

                value_tree = tree
                for p in path:
                    value_tree = value_tree[p]

            value_hparam = hparams
            for p in path:
                value_hparam = value_hparam.__getattr__(p)

            self.assertEqual(value_tree, value_hparam)

        # test get_node
        for _ in range(100):
            tree = random_tree()

            hparams = HParams(tree, default=False)

            value_tree = {}

            while isinstance(value_tree, dict):
                path = random_path(tree)

                value_tree = tree
                for p in path.split("."):
                    value_tree = value_tree[p]

            value_hparam = hparams.get_node(path)

            self.assertEqual(value_tree, value_hparam)

        # test __setattr__
        hparams = HParams({"a": 2}, default=False)
        hparams.expand(True)
        self.assertEqual(hparams.a, 2)
        hparams.a = 3
        self.assertEqual(hparams.a, 3)
        hparams.b = 4
        self.assertEqual(hparams.b, 4)
        hparams.c.a = 5
        hparams.c.b = 6
        hparams.c.c = 7
        hparams.c.d.a = 10
        self.assertEqual(hparams.c.a, 5)
        self.assertEqual(hparams.c.b, 6)
        self.assertEqual(hparams.c.c, 7)
        self.assertEqual(hparams.c.d.a, 10)

        # test scope
        with hparams.scope("c", expand=False) as p:
            self.assertFalse(p.__expand__)
            self.assertEqual(p.a, 5)
            self.assertEqual(p.b, 6)
            self.assertEqual(p.c, 7)
        self.assertTrue(hparams.c.__expand__)

        with hparams.scope("c", expand=True) as p:
            self.assertTrue(p.__expand__)
            self.assertEqual(p.a, 5)
            self.assertEqual(p.b, 6)
            self.assertEqual(p.c, 7)

            p.d.a = 8
            p.e = 9

        self.assertTrue(hparams.c.__expand__)

        self.assertEqual(hparams.c.d.a, 8)
        self.assertEqual(hparams.c.e, 9)

        # test has_scope
        self.assertTrue(hparams.has_scope("a"))
        self.assertTrue(hparams.has_scope("b"))
        self.assertTrue(hparams.has_scope("c.a"))
        self.assertTrue(hparams.has_scope("c.b"))
        self.assertTrue(hparams.has_scope("c.c"))
        self.assertTrue(hparams.has_scope("c.d.a"))
        self.assertTrue(hparams.has_scope("c.e"))

        self.assertFalse(hparams.has_scope("d"))
        self.assertFalse(hparams.has_scope("d.a"))
        self.assertFalse(hparams.has_scope("a.d"))
        self.assertFalse(hparams.has_scope("c.d.b"))
        self.assertFalse(hparams.has_scope("c.d.a,b"))
