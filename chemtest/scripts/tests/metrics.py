from torch.utils.tensorboard.writer import SummaryWriter
from chemtest.core.metrics.metric import Loss
from chemtest.metrics.mse import MSEMetric
import unittest

from chemtest.core.data import DataType, Framework

from chemtest.core.metrics import Metric, Metrics
from chemtest.core.data import (
    Dataset,
    DataLoader,
    Padding,
    Framework,
)
import numpy as np
import torch


class Index(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(Index, self).__init__(data, framework=framework, indexing=None)


class CustomType(DataType):
    def __init__(self, data, framework=Framework.NUMPY, **kwargs):
        super(CustomType, self).__init__(data, framework=framework, indexing=None)


class TestingDataset(Dataset):
    _dir_name = "testing_dataset"
    data_types = [Index, CustomType]

    def __len__(self) -> int:
        return 100

    def __getitem__(self, idx: int):

        if isinstance(idx, (list, np.ndarray)):
            data = [self.__getitem__(int(i)) for i in idx]
            return data

        return {
            Index: Index(np.array([idx])),
            CustomType: CustomType(np.ones(1)),
        }

    @property
    def elements(self):
        return []


class CustomMetric(Metric):
    @property
    def name(self):
        return "custom_metric"

    def calculate_batch_torch(self, batch, pred):
        res = torch.abs(batch.get(self.target).raw_data - pred[self.target].raw_data)
        return res

    def calculate_batch_tensorflow(self, batch, pred):
        raise NotImplemented


class TestMetric(unittest.TestCase):
    def test_metric_custom(self):
        import tempfile
        import os
        from chemtest.core.tools import test_timeout

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            dataloader = DataLoader(
                TestingDataset(
                    os.path.join(dir_name, "testing"), Dataset.Split(1, 1.0)
                ),
                targets=[(Index, Padding.NONE), (CustomType, Padding.NONE)],
                framework=Framework.PYTORCH,
                batch_size=4,
            )

            metric = CustomMetric(CustomType)

            for batch in dataloader:
                metric.handle_batch(
                    batch,
                    {
                        CustomType: CustomType(
                            torch.tensor([[2.0], [2.0], [2.0], [2.0]])
                        )
                    },
                )

            serialized = metric.serialize()
            self.assertEqual(serialized["name"], "custom_metric")
            self.assertTrue((serialized["batch"] == np.ones((100, 1, 1))).all())
            self.assertEqual(metric.preview(), {"custom_metric": 1.0})
            metric.gather()
            serialized = metric.serialize()
            self.assertTrue(serialized["batch"], [[1.0], None])

    def run_metric(self, metric, values, ground_truth, name, loss=False):
        import tempfile
        import os
        from chemtest.core.tools import test_timeout

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            dataloader = DataLoader(
                TestingDataset(
                    os.path.join(dir_name, "testing"), Dataset.Split(1, 1.0)
                ),
                targets=[(Index, Padding.NONE), (CustomType, Padding.NONE)],
                framework=Framework.PYTORCH,
                batch_size=4,
                drop_last=True,
            )

            for value, error in zip(values, ground_truth):

                for batch in dataloader:
                    if loss:
                        metric.handle_loss(torch.tensor([value]))
                    else:
                        metric.handle_batch(
                            batch,
                            {
                                CustomType: CustomType(
                                    torch.tensor([[value], [value], [value], [value]])
                                )
                            },
                        )

                serialized = metric.serialize()
                self.assertEqual(serialized["name"], name)
                self.assertTrue(
                    (
                        np.linalg.norm(
                            (np.ones((100, 1, 1)) * error) - serialized["batch"][-1]
                        )
                        < 1e-5
                    ).all()
                )
                self.assertTrue(abs(metric.preview()[name] - error) < 1e-5)
                metric.gather()
                serialized = metric.serialize()
                self.assertTrue(len(serialized["batch"][-2]) == 1)
                self.assertTrue(abs(serialized["batch"][-2][0] - error) < 1e-5)
                self.assertTrue(serialized["batch"][-1] is None)

    def test_metric_loss(self):
        from chemtest.core.metrics import Loss

        values = [2.0, 0.0, 1.0, 0.1]
        ground_truth = [v for v in values]

        self.run_metric(Loss(), values, ground_truth, "loss", loss=True)

    def test_metric_loss_named(self):
        from chemtest.core.metrics import Loss

        values = [2.0, 0.0, 1.0, 0.1]
        ground_truth = [v for v in values]

        self.run_metric(Loss("custom_loss"), values, ground_truth, "custom_loss", loss=True)

    def test_tensorboard(self):
        import tempfile
        import os
        import shutil
        from chemtest.core.tools import test_timeout

        from torch.utils.tensorboard import SummaryWriter
        from tensorboard.backend.event_processing import event_accumulator
        import pandas as pd
        from chemtest.metrics import MAEMetric

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            dataset_path = os.path.join(dir_name, "testing")
            tensorboard_path = os.path.join(dir_name, "runs")

            dataloader = DataLoader(
                TestingDataset(dataset_path, Dataset.Split(1, 1.0)),
                targets=[(Index, Padding.NONE), (CustomType, Padding.NONE)],
                framework=Framework.PYTORCH,
                batch_size=4,
            )

            writer = SummaryWriter(log_dir=tensorboard_path)

            metric = CustomMetric(CustomType)

            for i in range(10):
                for batch in dataloader:
                    metric.handle_batch(
                        batch,
                        {
                            CustomType: CustomType(
                                torch.tensor(
                                    [[10.0 - i], [10.0 - i], [10.0 - i], [10.0 - i]]
                                )
                            )
                        },
                    )
                metric.gather()
                metric.to_tensorboard(writer, step=i)

            writer.flush()
            writer.close()

            # manualy check the recorded data
            ea = event_accumulator.EventAccumulator(writer.log_dir)

            ea.Reload()
            self.assertEqual(
                ea.Tags(),
                {
                    "images": [],
                    "audio": [],
                    "histograms": [],
                    "scalars": ["custom_metric"],
                    "distributions": [],
                    "tensors": [],
                    "graph": False,
                    "meta_graph": False,
                    "run_metadata": [],
                },
            )

            df = pd.DataFrame(ea.Scalars("custom_metric"))
            for i in range(10):
                self.assertEqual(int(df.iloc[[i]]["step"]), i)
                self.assertEqual(float(df.iloc[[i]]["value"]), 9.0 - i)
            self.assertEqual(len(df), 10)

            shutil.rmtree(tensorboard_path)

            writer = SummaryWriter(log_dir=tensorboard_path)

            metric = MAEMetric(CustomType, target_name="U_0")

            for i in range(10):
                for batch in dataloader:
                    metric.handle_batch(
                        batch,
                        {CustomType: CustomType(torch.randn((4, 1)))},
                    )
                metric.gather()
                metric.to_tensorboard(writer, step=i)

            writer.flush()
            writer.close()

            # manualy check the recorded data
            ea = event_accumulator.EventAccumulator(
                writer.log_dir, size_guidance={"images": 16}
            )

            ea.Reload()
            self.assertEqual(
                ea.Tags(),
                {
                    "images": ["mae(U_0)/fig"],
                    "audio": [],
                    "histograms": [],
                    "scalars": ["mae(U_0)"],
                    "distributions": [],
                    "tensors": [],
                    "graph": False,
                    "meta_graph": False,
                    "run_metadata": [],
                },
            )

            self.assertEqual(len(ea.Images("mae(U_0)/fig")), 10)

    def test_metric_mae(self):
        from chemtest.metrics import MAEMetric

        values = [2.0, 0.0, 1.0, 0.1]
        ground_truth = [abs(1.0 - v) for v in values]

        self.run_metric(
            MAEMetric(CustomType, target_name="custom type"),
            values,
            ground_truth,
            "mae(custom type)",
        )

    def test_metric_mse(self):
        from chemtest.metrics import MSEMetric

        values = [2.0, 0.0, 1.0, 0.1]
        ground_truth = [(1.0 - v) ** 2 for v in values]

        self.run_metric(
            MSEMetric(CustomType, target_name="custom type"),
            values,
            ground_truth,
            "mse(custom type)",
        )

    def test_serialize(self):
        import tempfile
        import os
        from chemtest.core.tools import test_timeout

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            dataloader = DataLoader(
                TestingDataset(
                    os.path.join(dir_name, "testing"), Dataset.Split(1, 1.0)
                ),
                targets=[(Index, Padding.NONE), (CustomType, Padding.NONE)],
                framework=Framework.PYTORCH,
                batch_size=4,
            )

            metric = CustomMetric(CustomType)

            for _ in range(10):
                for batch in dataloader:
                    metric.handle_batch(
                        batch,
                        {CustomType: CustomType(torch.randn((4, 1)))},
                    )
                metric.gather()

            serialized = metric.serialize()

            metric_cpy = CustomMetric(CustomType)
            metric_cpy.unserialize(serialized)

            self.assertEqual(metric.batch, metric_cpy.batch)


class TestMetrics(unittest.TestCase):
    def training_process(
        self, dataloader: DataLoader, metrics: Metrics, epoch: int = 10
    ):
        from chemtest.core.data import Set

        for epoch in range(10):
            for i, batch in enumerate(dataloader):
                loss = torch.randn(1)
                pred = {
                    CustomType: CustomType(torch.ones((4, 1)) * epoch),
                    Index: CustomType(torch.randint(0, 99, (4, 1))),
                }
                metrics.handle_batch(
                    Set.TRAINING,
                    batch,
                    pred,
                    loss=loss,
                )
                if i == len(dataloader) - 1:
                    metrics.handle_batch(
                        Set.VALIDATION,
                        batch,
                        pred,
                        loss=loss,
                    )
            metrics.gather(Set.TRAINING)
            metrics.gather(Set.VALIDATION)

        for i, batch in enumerate(dataloader):
            loss = torch.randn(1)
            pred = {
                CustomType: CustomType(torch.ones((4, 1)) * 1.5),
                Index: CustomType(torch.randint(0, 99, (4, 1))),
            }
            metrics.handle_batch(
                Set.TESTING,
                batch,
                pred,
                loss=loss,
            )

        metrics.gather(Set.TESTING)

        return metrics

    def test_metrics_running(self):
        import tempfile
        import os
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import Set

        class CustomMetric2(Metric):
            @property
            def name(self):
                return "custom_metric2"

            def calculate_batch_torch(self, batch, pred):
                res = (
                    batch.get(self.target).raw_data - pred[self.target].raw_data
                ) ** 2
                return res

            def calculate_batch_tensorflow(self, batch, pred):
                raise NotImplemented

        class CustomMetric3(Metric):
            @property
            def name(self):
                return "custom_metric3"

            def calculate_batch_torch(self, batch, pred):
                res = (
                    batch.get(self.target).raw_data - pred[self.target].raw_data
                ) ** 3
                return res

            def calculate_batch_tensorflow(self, batch, pred):
                raise NotImplemented

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            dataloader = DataLoader(
                TestingDataset(
                    os.path.join(dir_name, "testing"), Dataset.Split(1, 1.0)
                ),
                targets=[(Index, Padding.NONE), (CustomType, Padding.NONE)],
                framework=Framework.PYTORCH,
                batch_size=4,
            )

            metrics = Metrics()
            metrics.add_metric(Loss(), [Set.TRAINING, Set.VALIDATION, Set.TESTING])
            metrics.add_metric(
                CustomMetric(Index), [Set.TRAINING, Set.VALIDATION, Set.TESTING]
            )
            metrics.add_metric(
                CustomMetric(CustomType), [Set.TRAINING, Set.VALIDATION, Set.TESTING]
            )
            metrics.add_metric(CustomMetric2(CustomType), [Set.VALIDATION, Set.TESTING])
            metrics.add_metric(CustomMetric3(CustomType), [Set.TESTING])

            metrics = self.training_process(
                dataloader=dataloader, metrics=metrics, epoch=10
            )

            error = np.array(
                [
                    [1.0],
                    [0.0],
                    [1.0],
                    [2.0],
                    [3.0],
                    [4.0],
                    [5.0],
                    [6.0],
                    [7.0],
                    [8.0],
                ]
            )

            # test training metrics

            self.assertIn("loss", metrics.metrics[Set.TRAINING])

            loss = metrics.metrics[Set.TRAINING]["loss"]
            self.assertEqual(len(loss), 1)
            self.assertEqual(len(loss[0].batch), 11)
            self.assertEqual(np.array(loss[0].batch[:-1]).shape, (10, 1))

            self.assertIn("custom_metric", metrics.metrics[Set.TRAINING])

            custom_metric = metrics.metrics[Set.TRAINING]["custom_metric"]
            self.assertEqual(len(custom_metric), 2)
            self.assertEqual(custom_metric[0].target, Index)
            self.assertEqual(np.array(custom_metric[0].batch[:-1]).shape, (10, 1))
            self.assertIs(custom_metric[0].batch[-1], None)
            self.assertEqual(custom_metric[1].target, CustomType)
            self.assertTrue(
                np.linalg.norm(np.array(custom_metric[1].batch[:-1]) - error) < 1e-5
            )
            self.assertIs(custom_metric[1].batch[-1], None)

            # test validation metrics

            self.assertIn("loss", metrics.metrics[Set.VALIDATION])

            loss = metrics.metrics[Set.VALIDATION]["loss"]
            self.assertEqual(len(loss), 1)
            self.assertEqual(len(loss[0].batch), 11)
            self.assertEqual(np.array(loss[0].batch[:-1]).shape, (10, 1))
            self.assertIs(loss[0].batch[-1], None)

            self.assertIn("custom_metric", metrics.metrics[Set.VALIDATION])

            custom_metric = metrics.metrics[Set.VALIDATION]["custom_metric"]
            self.assertEqual(len(custom_metric), 2)
            self.assertEqual(custom_metric[0].target, Index)
            self.assertEqual(np.array(custom_metric[0].batch[:-1]).shape, (10, 1))
            self.assertEqual(custom_metric[1].target, CustomType)
            self.assertTrue(
                np.linalg.norm(np.array(custom_metric[1].batch[:-1]) - error) < 1e-5,
            )
            self.assertIs(custom_metric[0].batch[-1], None)

            self.assertIn("custom_metric2", metrics.metrics[Set.VALIDATION])

            custom_metric2 = metrics.metrics[Set.VALIDATION]["custom_metric2"]
            self.assertEqual(len(custom_metric2), 1)
            self.assertEqual(custom_metric2[0].target, CustomType)
            self.assertTrue(
                np.linalg.norm(np.array(custom_metric2[0].batch[:-1]) - error ** 2)
                < 1e-5
            )
            self.assertIs(custom_metric2[0].batch[-1], None)

            # test testing metrics

            self.assertIn("loss", metrics.metrics[Set.TESTING])

            loss = metrics.metrics[Set.TESTING]["loss"]
            self.assertEqual(len(loss), 1)
            self.assertEqual(len(loss[0].batch), 2)
            self.assertEqual(np.array(loss[0].batch[:-1]).shape, (1, 1))
            self.assertIs(loss[0].batch[-1], None)

            self.assertIn("custom_metric", metrics.metrics[Set.TESTING])

            custom_metric = metrics.metrics[Set.TESTING]["custom_metric"]
            self.assertEqual(len(custom_metric), 2)
            self.assertEqual(custom_metric[0].target, Index)
            self.assertEqual(np.array(custom_metric[0].batch[:-1]).shape, (1, 1))
            self.assertEqual(custom_metric[1].target, CustomType)
            self.assertAlmostEqual(custom_metric[1].batch[0][0], 0.5)
            self.assertIs(custom_metric[1].batch[-1], None)

            self.assertIn("custom_metric2", metrics.metrics[Set.TESTING])

            custom_metric2 = metrics.metrics[Set.TESTING]["custom_metric2"]
            self.assertEqual(len(custom_metric2), 1)
            self.assertEqual(custom_metric2[0].target, CustomType)
            self.assertAlmostEqual(custom_metric2[0].batch[0][0], 0.5 ** 2)
            self.assertIs(custom_metric2[0].batch[-1], None)

            self.assertIn("custom_metric3", metrics.metrics[Set.TESTING])

            custom_metric3 = metrics.metrics[Set.TESTING]["custom_metric3"]
            self.assertEqual(len(custom_metric3), 1)
            self.assertEqual(custom_metric3[0].target, CustomType)
            self.assertAlmostEqual(custom_metric3[0].batch[0][0], -(0.5 ** 3))
            self.assertIs(custom_metric3[0].batch[-1], None)

    def test_metrics_serialize(self):
        import tempfile
        import os
        import json
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import Set

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            dataloader = DataLoader(
                TestingDataset(
                    os.path.join(dir_name, "testing"), Dataset.Split(1, 1.0)
                ),
                targets=[(Index, Padding.NONE), (CustomType, Padding.NONE)],
                framework=Framework.PYTORCH,
                batch_size=4,
            )

            metrics = Metrics()

            metrics.add_metric(Loss(), [Set.TRAINING, Set.VALIDATION, Set.TESTING])
            metrics.add_metric(CustomMetric(Index), [Set.TRAINING, Set.TESTING])
            metrics.add_metric(CustomMetric(CustomType), [Set.TRAINING, Set.TESTING])

            metrics = self.training_process(
                dataloader=dataloader, metrics=metrics, epoch=10
            )

            file_name = os.path.join(dir_name, "metrics.json")

            metrics.save(file_name)

            metrics2 = Metrics()

            metrics2.add_metric(CustomMetric(Index), [Set.TRAINING, Set.TESTING])
            metrics2.add_metric(CustomMetric(CustomType), [Set.TESTING])
            metrics2.add_metric(CustomMetric(CustomType), [Set.TRAINING])
            metrics2.add_metric(Loss(), [Set.TRAINING, Set.VALIDATION])
            metrics2.add_metric(Loss(), [Set.TESTING])

            metrics2.load(file_name)

            def serialize_metric(metrics, set_name, metric_name, target_name=None):
                metrics_type = metrics.metrics[set_name]
                metrics_target = metrics_type[metric_name]

                for m_target in metrics_target:
                    if (target_name is None) or (
                        m_target.target.get_name() == target_name
                    ):
                        return json.dumps(m_target.serialize())

            self.assertEqual(
                serialize_metric(metrics, Set.TRAINING, "loss"),
                serialize_metric(metrics2, Set.TRAINING, "loss"),
            )
            self.assertEqual(
                serialize_metric(metrics, Set.VALIDATION, "loss"),
                serialize_metric(metrics2, Set.VALIDATION, "loss"),
            )
            self.assertEqual(
                serialize_metric(metrics, Set.TESTING, "loss"),
                serialize_metric(metrics2, Set.TESTING, "loss"),
            )

            self.assertEqual(
                serialize_metric(metrics, Set.TRAINING, "custom_metric", "index"),
                serialize_metric(metrics2, Set.TRAINING, "custom_metric", "index"),
            )
            self.assertEqual(
                serialize_metric(metrics, Set.TESTING, "custom_metric", "index"),
                serialize_metric(metrics2, Set.TESTING, "custom_metric", "index"),
            )

            self.assertEqual(
                serialize_metric(metrics, Set.TRAINING, "custom_metric", "custom_type"),
                serialize_metric(
                    metrics2, Set.TRAINING, "custom_metric", "custom_type"
                ),
            )
            self.assertEqual(
                serialize_metric(metrics, Set.TESTING, "custom_metric", "custom_type"),
                serialize_metric(metrics2, Set.TESTING, "custom_metric", "custom_type"),
            )

    def test_metrics_export(self):
        import tempfile
        import os
        import json
        from chemtest.core.tools import test_timeout

        from chemtest.core.data import Set
        from tensorboard.backend.event_processing import event_accumulator

        import pandas as pd

        with test_timeout(10) and tempfile.TemporaryDirectory() as dir_name:

            writer = SummaryWriter(os.path.join(dir_name, "runs"))
            batch_size = 4

            dataloader = DataLoader(
                TestingDataset(
                    os.path.join(dir_name, "testing"), Dataset.Split(1, 1.0)
                ),
                targets=[(Index, Padding.NONE), (CustomType, Padding.NONE)],
                framework=Framework.PYTORCH,
                batch_size=batch_size,
            )

            metrics = Metrics()

            metrics.add_metric(Loss(), [Set.TRAINING, Set.VALIDATION])
            metrics.add_metric(
                CustomMetric(CustomType), [Set.TRAINING, Set.VALIDATION, Set.TESTING]
            )

            metrics = self.training_process(
                dataloader=dataloader, metrics=metrics, epoch=10
            )

            for epoch in range(10):
                for i, batch in enumerate(dataloader):
                    loss = torch.ones((1,)) / (i + 1)
                    pred = {CustomType: CustomType(torch.ones((batch_size, 1)) * epoch)}
                    metrics.handle_batch(
                        Set.TRAINING,
                        batch,
                        pred,
                        loss=loss,
                    )

                    preview = metrics.preview(Set.TRAINING)
                    self.assertAlmostEqual(preview["loss"], 1.0 / (i + 1))
                    self.assertAlmostEqual(preview["custom_metric"], abs(1.0 - epoch))

                    metrics.gather(Set.TRAINING)

                    preview = metrics.preview(Set.TRAINING)
                    self.assertAlmostEqual(preview["loss"], 1.0 / (i + 1))
                    self.assertAlmostEqual(preview["custom_metric"], abs(1.0 - epoch))

                    flatten_metrics = metrics.export(Set.TRAINING)
                    self.assertAlmostEqual(flatten_metrics["loss"], 1.0 / (i + 1))
                    self.assertAlmostEqual(
                        flatten_metrics["custom_metric(custom_type)"], abs(1.0 - epoch)
                    )

                    metrics.to_tensorboard(
                        Set.TRAINING, writer, step=i + epoch * len(dataloader)
                    )

                    if i == len(dataloader) - 1:
                        metrics.handle_batch(
                            Set.VALIDATION,
                            batch,
                            pred,
                            loss=loss,
                        )
                        metrics.gather(Set.VALIDATION)
                        metrics.to_tensorboard(
                            Set.VALIDATION, writer, step=i + epoch * len(dataloader)
                        )

            for i, batch in enumerate(dataloader):
                loss = torch.ones((1,)) / 10
                pred = {CustomType: CustomType(torch.ones((batch_size, 1)) * 1.5)}
                metrics.handle_batch(
                    Set.TESTING,
                    batch,
                    pred,
                    loss=loss,
                )

            metrics.gather(Set.TESTING)
            metrics.to_tensorboard(Set.TESTING, writer, step=25 * len(dataloader) - 1)
            writer.flush()
            writer.close()

            # manualy check the recorded data
            ea = event_accumulator.EventAccumulator(writer.log_dir)

            ea.Reload()
            self.assertEqual(
                ea.Tags(),
                {
                    "images": [],
                    "audio": [],
                    "histograms": [],
                    "scalars": [
                        "train/loss",
                        "train/custom_metric",
                        "valid/loss",
                        "valid/custom_metric",
                        "test/custom_metric",
                    ],
                    "distributions": [],
                    "tensors": [],
                    "graph": False,
                    "meta_graph": False,
                    "run_metadata": [],
                },
            )

            df = pd.DataFrame(ea.Scalars("train/loss"))
            self.assertEqual(len(df), 250)
            for i in range(250):
                self.assertEqual(int(df.iloc[[i]]["step"]), i)
                self.assertAlmostEqual(
                    float(df.iloc[[i]]["value"]), 1.0 / ((i % 25) + 1.0)
                )

            df = pd.DataFrame(ea.Scalars("train/custom_metric"))
            self.assertEqual(len(df), 250)
            for i in range(250):
                self.assertEqual(int(df.iloc[[i]]["step"]), i)
                self.assertAlmostEqual(
                    float(df.iloc[[i]]["value"]), abs(1.0 - (i // 25))
                )

            df = pd.DataFrame(ea.Scalars("valid/loss"))
            self.assertEqual(len(df), 10)
            for i in range(10):
                self.assertEqual(int(df.iloc[[i]]["step"]), i * 25 + 24)
                self.assertAlmostEqual(float(df.iloc[[i]]["value"]), 1.0 / 25.0)

            df = pd.DataFrame(ea.Scalars("valid/custom_metric"))
            self.assertEqual(len(df), 10)
            for i in range(10):
                self.assertEqual(int(df.iloc[[i]]["step"]), i * 25 + 24)
                self.assertAlmostEqual(float(df.iloc[[i]]["value"]), abs(1.0 - i))

            df = pd.DataFrame(ea.Scalars("test/custom_metric"))
            self.assertEqual(len(df), 1)
            self.assertEqual(int(df.iloc[[0]]["step"]), 25 * len(dataloader) - 1)
            self.assertAlmostEqual(float(df.iloc[[0]]["value"]), 0.5)
