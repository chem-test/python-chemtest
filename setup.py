import setuptools

from typing import List
import json
import sys, os

sys.path.append(".")
from chemtest import __version__


def tree(path: str) -> List[str]:
    lst = []

    for root, dirs, _ in os.walk(path):
        for name in dirs:
            if "__pycache__" in name:
                continue
            lst.append(os.path.join(root, name).replace("/", "."))

    return lst


with open("README.md", "r") as fh:
    long_description = fh.read()

install_requires = [
        "torch>=1.9",
        "tqdm>=4.59",
        "pandas",
        "matplotlib",
        "torch-scatter",
        "torch-sparse",
        "torch-cluster",
        "torch-spline-conv",
        "torch-geometric",
        "torch_optimizer",
        "timm",
    ]

dependency_links = ["https://github.com/ildoonet/pytorch-gradual-warmup-lr.git"]

try:
    with open("./install.json", "r") as fp:
        config = json.load(fp)
        if not config["install_dep"]:
            install_requires = []
            dependency_links = []
except:
    pass  # default setup

setuptools.setup(
    name="chem-test",
    version=__version__,
    author="A. Klipfel",
    description="Framework to benchmark deep models for chemistry",
    author_email="arthur.klipfel@univ-artois.fr",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/chem-test/python-chemtest",
    packages=[
        "chemtest",
        "chemtest.core",
        "chemtest.core.data",
        "chemtest.core.hparam",
        "chemtest.core.metrics",
        "chemtest.datasets",
        "chemtest.datasets.types",
        "chemtest.datasets.convertions",
        "chemtest.metrics",
        "chemtest.models",
        "chemtest.training",
        "chemtest.scripts",
        "chemtest.scripts.tests",
        "chemtest.examples",
    ]
    + tree("chemtest/dep"),
    install_requires=install_requires,
    dependency_links=dependency_links,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    entry_points={
        "console_scripts": [
            "chem-test=chemtest.scripts.main:main",
        ]
    },
    python_requires=">=3.7",
)
